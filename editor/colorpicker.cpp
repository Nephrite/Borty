#include "colorpicker.h"
#include "ui_colorpicker.h"

#include <QColorDialog>
#include <QPalette>

#include <QDebug>

ColorPicker::ColorPicker(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ColorPicker)
{
    ui->setupUi(this);
    setColor(Qt::black);
}

ColorPicker::~ColorPicker()
{
    delete ui;
}

void ColorPicker::setColor(QColor c)
{
    m_color = c;
    QPalette palette = ui->pickButton->palette();
    palette.setColor(ui->pickButton->backgroundRole(), m_color);
    ui->pickButton->setAutoFillBackground(true);
    ui->pickButton->setPalette(palette);

    ui->label->setText(QString::number(m_color.rgb(),16).insert(2, 'x')); // AAxRRGGBB
    emit picked(c);
}

void ColorPicker::on_pickButton_clicked()
{
    QColor color = QColorDialog::getColor( m_color
                                         , this
                                         , "Pick a color"
                                         , QColorDialog::DontUseNativeDialog
                                         );
    setColor(color);
}


