#ifndef GRIBBO_H
#define GRIBBO_H

#include <QObject>
#include "game/gameobjects/actors/actor.h"

class Gribbo : public Actor
{
    Q_OBJECT


public:
    Gribbo();   
    Gribbo(QJsonObject& properties, b2World*world);
    virtual ~Gribbo();

    enum GribboType{simple, withBag, withFlute};

    Q_PROPERTY_WITH_ACCESSORS(int, gribboType, setGribboType)

    void advance(int timeDelta);
    void glDraw(GlUtils* glutils);
    void onScreenEnter();
    void onScreenLeave();
    void setGameData(GameData* data);
    void preSolve(PhysicsObject *, b2Contact *contact);

    enum state{idleWalking, attackWaiting, pointing , attack, leaving, friendlyGoAway};
protected:
    void emitBullet(type::bullets bulletType);

    void idleAdvance();
    void waitingAdvance();
    void pointingAdvance();
    void leavingAdvance();
    void attackAdvance();
    void goAwayAdvance();
    int m_attackInterval;
    int m_lastAttackTime;
    void setupShadowSprite();
    void setupSprite();
    state m_state;
    int m_timer;
    int m_timedelta;
    bool m_isAnger;

    double m_pointingDist;
    double m_attackDist;
};

#endif // GRIBBO_H
