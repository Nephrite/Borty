#include "gribbo.h"

#include "game/glesgameview/glutils.h"
#include "common/objecttypes.h"

#include "common/config.h"
#include "game/gameobjects/player.h"

#include "game/gameobjects/tooltipobject.h"

Gribbo::Gribbo()
{

}


Gribbo::Gribbo(QJsonObject &properties, b2World *world) :
    Actor(properties, world, false)
{

    m_attackInterval = 800;
    m_lastAttackTime = 0;

    setup(properties);
    createPhysics(world);

    m_inJump = false;
    m_falling = false;
    m_jumpHeight = 0;

    m_state = idleWalking;
    m_active = false;
    m_isOnScreen = false;
    m_direction = Direction::directions(rand()%7);
    m_timer = rand()%1000;
    m_health = 2;

    setupSprite();
    setupShadowSprite();

    m_maxVelocity = 0.5;

    collisionList.append(type::Bullet);
    collisionList.append(type::Player);


    switch (m_gribboType) {
    case simple:
        m_propertyModifier = new PropertyModifier("health", -1);
        m_pointingDist = 5;
        m_attackDist =1.5;
        break;
    case withBag:
        m_propertyModifier = nullptr;
        m_pointingDist = 4;
        m_attackDist = 1.5;
        break;

    case withFlute:
        m_propertyModifier = new PropertyModifier("health", -1);
        m_pointingDist = 6;
        m_attackDist = 5;
          break;
    default:
        break;
    }
}

Gribbo::~Gribbo()
{

}

void Gribbo::advance(int timeDelta)
{
    BaseGameObject::advance(timeDelta);
    if (!m_active || !m_isOnScreen) return;
    m_timer += timeDelta;
    m_timedelta = timeDelta;

    body->SetLinearDamping(m_damping);
    moveAdvance();
    updateJump();
    if (m_health < 1 && m_state != leaving)
    {
        m_direction = Direction::stand;
        m_gamedata->deCountAttack();
        m_state = leaving;
        m_shadowSprite.setOpacity(0);
        m_sprite.setCurrentFrame(4);
        disableCollisions();

        m_jumpNow = false;
        m_falling = false;
        m_inJump = false;
        m_jumpHeight = 0;
        m_maxJumpHeight = 0;
    }

    switch (m_state) {
    case idleWalking:
        idleAdvance();

        break;

    case attackWaiting:
        waitingAdvance();

        break;

    case pointing:
        pointingAdvance();

        break;

    case attack:
        attackAdvance();
        break;

    case leaving:
        leavingAdvance();

        break;
    case friendlyGoAway:
        goAwayAdvance();

        break;

    default:
        break;
    }

    if (m_timer % 500 < timeDelta)
    {
        m_shadowSprite.setNextFrame(Drawable::Loop);
    }
}

void Gribbo::idleAdvance()
{
    if (m_timer % 1000 < m_timedelta)
    {
        m_direction = Direction::next(m_direction);
        if (m_gamedata->isPlayerAngry())
        {
            m_sprite.setCurrentFrame(attackWaiting);
            m_state = attackWaiting;

        } else if (m_gamedata->isPlayerFriendly())
        {
            m_state = friendlyGoAway;
        }
    }
}

void Gribbo::waitingAdvance()
{
    if (m_gamedata->canAttack())
    {
        m_gamedata->countAttack();
        m_sprite.setCurrentFrame(pointing);
        m_state = pointing;
    }
    if ((m_gamedata->playerPos() - body->GetPosition()).Length()  > m_pointingDist)
        m_direction = Direction::vecToDir(m_gamedata->playerPos() - body->GetPosition());
    else
        m_direction = Direction::stand;

}

void Gribbo::pointingAdvance()
{
    b2Vec2 distToPlayer = m_gamedata->playerPos() - body->GetPosition();

    if ((m_gamedata->playerPos() - body->GetPosition()).Length() > m_attackDist)
    {
        m_direction = Direction::vecToDir(m_gamedata->playerPos() - body->GetPosition());
    }
    else
    {
        if (distToPlayer.y > 0)
            m_direction = Direction::down;
        else
            m_direction = Direction::up;

        bool ready;
        if (m_gribboType == withFlute) ready = fabs(distToPlayer.y) < m_attackDist;
        else ready = distToPlayer.Length() < m_attackDist;

        if (ready)
        {
            m_direction = Direction::stand;
            m_sprite.setCurrentFrame(attack);
            if (m_timer - m_lastAttackTime > 200)
            {
                 m_state = attack;
                 m_lastAttackTime = m_timer;
            }
            return;
        }
    }
}

void Gribbo::attackAdvance()
{
    if (m_timer - m_lastAttackTime < m_attackInterval)
    {
         m_direction = Direction::vecToDir(m_gamedata->playerPos() - body->GetPosition());
         return;
    }
    m_lastAttackTime = m_timer;
    switch (m_gribboType) {
    case simple:       
    case withBag:
            m_direction = Direction::vecToDir(m_gamedata->playerPos() - body->GetPosition());       
        break;
    case withFlute:
        m_direction = Direction::stand;
        emitBullet(type::Note);
        m_state = pointing;
        break;
    default:
        break;
    }
    m_sprite.setCurrentFrame(pointing);
}

void Gribbo::leavingAdvance()
{
    if (m_timer > 200)
    {
        m_sprite.setNextFrame(Drawable::Direct);
        m_timer = 0;
    }
    if (m_sprite.currentFrame() == 0)
    {
        m_sprite.setOpacity(0);
        m_active = false;
        m_visible = false;
        deleteMe(this);
        return;
    }
}

void Gribbo::goAwayAdvance()
{
    m_direction = Direction::invert(
            Direction::vecToDir(m_gamedata->playerPos() - body->GetPosition())
                );
}

void Gribbo::glDraw(GlUtils *glutils)
{
    if (!visible()) return;
    m_sprite.setPos(body->GetPosition());

    if (m_inJump || m_falling)
    {
        m_sprite.setOffset(0, -m_ySpriteOffset - m_jumpHeight);
    }else
    {
        m_sprite.setOffset(0, -m_ySpriteOffset);
    }
    m_shadowSprite.setPos(body->GetPosition());
    m_shadowSprite.turn(sin(m_timer/500));
    m_shadowSprite.draw(glutils);    

    if (Direction::isLeft(m_direction))
    {
        m_sprite.setFlipping(1.0, 1.0);
    }
    else if (Direction::isRight(m_direction))
    {
        m_sprite.setFlipping(-1.0, 1.0);
    }

    m_sprite.draw(glutils);   
}

void Gribbo::onScreenEnter()
{
    m_isOnScreen=true;
    m_active = true;
}

void Gribbo::onScreenLeave()
{    
    m_state = idleWalking;
}

void Gribbo::setGameData(GameData *data)
{
    m_gamedata = data;
    m_gamedata->registerEnemy();
}

void Gribbo::preSolve(PhysicsObject *o, b2Contact *contact)
{
    if (o->objectType() == type::Bullet)
    {
        contact->SetEnabled(false);
        Bullet* b = nullptr;
        b = dynamic_cast<Bullet*>(o);
        if (b != nullptr && (b->target() == Bullet::EnemyTarget || b->target() == Bullet::AllTarget))
        {
            setProperty(         b->affectProperty(),
                        property(b->affectProperty().toLatin1()).toInt() + b->value());
            b->disable();

        }
    }
    if (m_gribboType == withBag && o->objectType() == type::Player)
    {
        contact->SetEnabled(false);
        Player* p = nullptr;
        p = dynamic_cast<Player*>(o);
        if (p != nullptr )
        {
           p->gribboCatch();
           m_sprite.setCurrentFrame(4);
        }

    }
    m_state = idleWalking;  
}

void Gribbo::emitBullet(type::bullets bulletType)
{
    Bullet* b = new Bullet(bulletType, body->GetWorld());
    if (body->GetPosition().x < m_gamedata->playerPos().x)
    {
        b->setDirection(Direction::right);
        b->body->SetTransform(body->GetPosition()-b2Vec2(-0.5, m_ySpriteOffset+0.5), 0);

    }
    else
    {
        b->setDirection(Direction::left);
        b->body->SetTransform(body->GetPosition()-b2Vec2(0.5, m_ySpriteOffset+0.5), 0);

    }
    b->setTarget(Bullet::PlayerTarget);
    m_gamedata->addBullet(b);
}


void Gribbo::setupShadowSprite()
{
    m_shadowSprite.setAsBox(m_width-0.5, m_width-0.5);
    m_shadowSprite.setPos(body->GetPosition());
    m_shadowSprite.addTexture("./resources/textures/enemy/gribbo_shadow.png",
                              QOpenGLTexture::Linear);
    m_shadowSprite.setFrameCount(4);
    m_shadowSprite.setColumnCount(4);
    m_shadowSprite.setRowCount(1);
    m_shadowSprite.setCurrentFrame(1);
    m_shadowSprite.setTiling(1.0,1.0);
    m_shadowSprite.setShaderName("animatedSprite");
}

void Gribbo::setupSprite()
{
    m_sprite.setShaderName("animatedSprite");
    m_sprite.setOffset(0, -m_ySpriteOffset);
    m_sprite.setFrameCount(10);
    m_sprite.setColumnCount(5);
    m_sprite.setRowCount(2);
    m_sprite.setCurrentFrame(idleWalking);
}
