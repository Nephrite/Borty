#ifndef GAMEDATA_H
#define GAMEDATA_H

#include "common/qpropertyaccessmacro.h"

#include "Box2D/Box2D.h"
#include <QList>
#include <QRect>

class BaseGameObject;

class GameData
{    
    Q_MEMBER_WITH_ACCESSORS(b2Vec2, playerPos, setPlayerPos)
    Q_MEMBER_WITH_ACCESSORS(bool, isPlayerFriendly, setPlayerFriendly)
    Q_MEMBER_WITH_ACCESSORS(bool, isPlayerAngry, setPlayerAngry)
    Q_MEMBER_WITH_ACCESSORS(int, attackCount, setAttackCount)
    Q_MEMBER_WITH_ACCESSORS(int, maxAttackCount, setMaxAttackCount)
    Q_MEMBER_WITH_ACCESSORS(int, enemyCount, setEnemyCount)
    Q_MEMBER_WITH_ACCESSORS(QRectF, viewport, setViewport)

public:
    GameData();
    void registerEnemy();
    void reset();
    bool canAttack(){return m_attackCount < m_maxAttackCount;}
    void countAttack(){m_attackCount++;}
    void deCountAttack(){m_attackCount--;}
    void addBullet(BaseGameObject* b);
    void bulletsRegisteredNotify();
    QList<BaseGameObject *> &newBullets();

protected:
    QList<BaseGameObject*> m_newBullets;

};

#endif // GAMEDATA_H
