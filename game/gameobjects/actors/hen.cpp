#include "hen.h"
#include "common/objecttypes.h"
Hen::Hen() : m_hungry(true)
{
    m_objectType = type::Hen;
    collisionList.append(type::Bullet);
}

Hen::Hen(QJsonObject& properties, b2World *world) :
    Actor(properties, world),
    m_state(idleWalking),
    m_hungry(true)
{
    setup(properties);
    setupSprite();
    setupShadowSprite();
    m_shadowSprite.setShaderName("animatedSprite");
    m_timer = 0;
    m_damping = 2.5;
    m_inJump = false;
    m_falling = false;
    m_canJump = true;
    m_objectType = type::Hen;

    m_maxVelocity = 0.2;
    m_maxJumpHeight = 1;
    collisionList.append(type::Bullet);
    m_direction = Direction::random();
    m_ySpriteOffset+=0.3;
}

Hen::~Hen()
{

}

void Hen::onScreenEnter()
{
    m_active = true;
}

void Hen::onScreenLeave()
{
    m_active = false;
}

void Hen::preSolve(PhysicsObject *object, b2Contact *contact)
{
    if (object->objectType() == type::Bullet)
    {
        contact->SetEnabled(false);
        Bullet* b = dynamic_cast<Bullet*>(object);
        if (b)
        {
            m_hungry = !b->isTasty();
            if (m_hungry)
                m_state = idleWalking;
            else
                m_state = Panic;
            b->disable();
        }
    }
}



void Hen::setupSprite()
{   
    m_sprite.setShaderName("animatedSprite");
    m_sprite.setOffset(0, -m_ySpriteOffset);
    m_sprite.setFrameCount(8);
    m_sprite.setColumnCount(4);
    m_sprite.setRowCount(2);
    m_sprite.setCurrentFrame(0);
}

void Hen::glDraw(GlUtils *glutils)
{
    if (!visible()) return;
    m_sprite.setPos(body->GetPosition());

    if (m_inJump || m_falling)
    {
        m_shadowSprite.setOpacity(m_jumpHeight/m_maxJumpHeight);
        m_sprite.setOffset(0, -m_ySpriteOffset - m_jumpHeight);
    }else
    {
        m_shadowSprite.setOpacity(0);
        m_sprite.setOffset(0, -m_ySpriteOffset);
    }
    m_shadowSprite.setPos(body->GetPosition());
    m_shadowSprite.draw(glutils);

    if (Direction::isLeft(m_direction) && body->GetLinearVelocity().x < 0)
    {
        m_sprite.setFlipping(1.0, 1.0);
    }
    else if (Direction::isRight(m_direction)  && body->GetLinearVelocity().x > 0)
    {
        m_sprite.setFlipping(-1.0, 1.0);
    }

    m_sprite.draw(glutils);
}

void Hen::advance(int timedelta)
{
    if (!m_active) return;
    BaseGameObject::advance(timedelta);
    body->SetLinearDamping(m_damping);
    m_timer += timedelta;
    if ((body->GetPosition() - m_gamedata->playerPos()).Length() < 3 && m_hungry)
    {
        m_state = Panic;
        m_maxVelocity = 3;
    }
    else
    {
        m_state = idleWalking;     
        m_maxVelocity = 0.4;
    }

    switch (m_state) {
    case idleWalking:
        if (m_timer % 500 < timedelta)
            m_sprite.setNextFrame(Drawable::Direct);
        if (m_sprite.currentFrame() > 3) m_sprite.setCurrentFrame(0);

        if (m_timer % 1000 < timedelta )
        {
            m_direction = Direction::next(m_direction);
            m_maxVelocity = 0.4;
        }
        if (m_timer > 2500)
        {
            m_maxVelocity = 0;
            m_timer = 0;
        }

        break;

    case Panic:

        if (m_timer % 100 < timedelta)
        {
            m_sprite.setNextFrame(Drawable::Direct);
            if (m_sprite.currentFrame() < 4) m_sprite.setCurrentFrame(4);           
        }
        if (m_timer > 1000)
        {
            if (!m_inJump && !m_falling) m_jumpNow = true;
            m_direction = Direction::random();
            m_timer = 0;
        }
        break;
    default:

        break;
    }

    updateJump();
    moveAdvance();
}














