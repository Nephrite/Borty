#ifndef STATICBACKROUND_H
#define STATICBACKROUND_H

#include "game/gameobjects/basegameobject.h"

class StaticBackground : public BaseGameObject
{
    Q_OBJECT

public:
    StaticBackground();
    StaticBackground(QJsonObject& parameters);
    ~StaticBackground();
};

#endif // STATICBACKROUND_H
