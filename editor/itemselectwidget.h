#ifndef ITEMSELECTWIDGET_H
#define ITEMSELECTWIDGET_H

#include <QWidget>
#include <QMap>

#include "editor/itembutton.h"
#include "thumbnailer.h"
#include <QTreeWidget>
class QVBoxLayout;

namespace Ui {
class ItemSelectWidget;
}

class ItemSelectWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ItemSelectWidget(QWidget *parent = 0);
    ~ItemSelectWidget();
    QJsonObject* newProperties;
public slots:
    void selectedObject(QJsonObject*);
signals:
    void sendNewProperties(QJsonObject*);
private slots:
    void on_displaySelectedStyleButton_clicked();

private:
    void setupButtons(QString file);
    void addTreeItem(QTreeWidgetItem* parent, QWidget* widget);

    void addButton(QString tab, ItemButton* button);
    QMap<QString, QTreeWidgetItem*> objectListTabs;
    QTreeWidgetItem* m_TreeRoot;
    QList<ItemButton*> allItemButtons;
    Ui::ItemSelectWidget *ui;
    Thumbnailer m_thumbnailer;
};

#endif // ITEMSELECTWIDGET_H
