#ifndef SAVE_H
#define SAVE_H


#include <QDebug>
#include <QFile>

#include <QStringList>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>

#include "common/object.h"

class Save
{
    public:
        static Save* instance();
        void registerSavable(Object* obj);
        void unregisterSavable(Object* obj);
        void restoreState(Object* obj);
        void clearSavables();
        void selectSave(const QString& saveName);
        void load();
        void saveCurrentGame();
        QStringList& saves();
        bool levelIsFinished(const QString& fileName);
        void addToFinished(const QJsonObject& data);
        QJsonObject value(QString name);
        void setValue(QString name, QJsonObject value);
        void fixState();
    protected:

        static Save Instance;
        bool openSaveFile();
        void initSaveDocument();
    private:
        Save();
        ~Save();
        void loadFinishedList();

        QString m_saveName;
        QString m_saveFileName;
        QFile m_saveFile;
        QStringList m_finishedLevels;
        QStringList m_savedGames;
        QJsonDocument m_saveDocument;
        QJsonObject m_mainObject;
        bool m_saved;
        bool m_loaded;

        QList<Object*> m_savables;
};

#endif // SAVE_H
