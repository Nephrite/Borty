#ifndef SNOWCLOUD_H
#define SNOWCLOUD_H

#include <QObject>
#include "game/gameobjects/basegameobject.h"
class SnowCloud : public BaseGameObject
{
    Q_OBJECT
    PROPERTY(QString,  stencilTexture,   setStencilTexture)
    PROPERTY(bool,  attachX,  setAttachX);
    PROPERTY(bool,  attachY,  setAttachY);
    PROMOTED_PROPERTY(GLfloat, double, blinkSpeed, setBlinkSpeed)
    PROMOTED_PROPERTY(GLfloat, double, xSpeed, setXSpeed)
    PROMOTED_PROPERTY(GLfloat, double, ySpeed, setYSpeed)

public:
    SnowCloud();
    SnowCloud(QJsonObject& parameters);
    void advance(int timeDelta);  
    void setupSprite();
    void setGameData(GameData* data);
protected:
    b2Vec2 m_basePos;
    GLuint m_stencil;
    float m_time;
    GameData* m_gameData;
};

#endif // SNOWCLOUD_H
