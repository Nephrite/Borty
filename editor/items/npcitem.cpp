#include "npcitem.h"

NPCItem::NPCItem(QGraphicsItem *parent) : BaseItem(parent),
    m_activateEvent(PlayerTouch),
    m_behaviour(LinearWalking),
    m_distance(5),
    m_initDirection(left),
    m_text("what?"),
    m_propertyName(QString("")),
    m_value(0),
    m_singleAction(false)
{

}

NPCItem::NPCItem(QVariantMap *newProperties) : BaseItem(newProperties),
    m_activateEvent(PlayerTouch),
    m_behaviour(LinearWalking),
    m_distance(5),
    m_initDirection(left),
    m_text("what?"),
    m_propertyName(QString("")),
    m_value(0),
    m_singleAction(false)
{
    setup(newProperties);
}

NPCItem::~NPCItem()
{

}
