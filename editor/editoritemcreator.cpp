#include "editor/editoritemcreator.h"

#include "items/baseitem.h"
#include "items/movingplatformitem.h"
#include "items/endlevelblockitem.h"
#include "items/backgrounditem.h"
#include "items/billboardphysicsitem.h"
#include "items/staticbackgrounditem.h"
#include "items/gribboitem.h"
#include "items/tooltipitem.h"
#include "items/npcitem.h"
#include "items/ammoitem.h"
#include "items/animateddecoritem.h"
#include "items/flameitem.h"
#include "items/snowclouditem.h"

#include "common/objecttypes.h"

#include <QJsonObject>


EditorItemCreator::EditorItemCreator()
{

}

BaseItem* EditorItemCreator::create(QVariantMap *map)
{
    bool ok = false;
    int ObjType = map->value("type").toInt(&ok);
    if (!ok) ObjType = map->value("objectType").toInt();

    BaseItem* item;
    switch (ObjType)
    {
    case type::EndLevelBlock:
        item = new EndLevelBlockItem(map);
        break;

    case type::ParallaxBackground:
        item = new BackgroundItem(map);
        break;

    case type::MovingPlatform:
        item = new MovingPlatformItem(map);
        break;
    case type::BillboardPhysicsObject:
        item = new BillBoardPhysicsItem(map);
        break;
    case type::StaticBackground:
        item = new StaticBackgroundItem(map);
        break;
    case type::Gribbo:
        item = new GribboItem(map);
        break;
    case type::ToolTipObject:
        item = new TooltipItem(map);
        break;
    case type::NPC:
        item = new NPCItem(map);
        break;
    case type::Ammo:
        item = new AmmoItem(map);
        break;
    case type::AnimatedDecorObject:
        item = new AnimatedDecorItem(map);
        break;
    case type::Flame:
        item = new FlameItem(map);
        break;
    case type::SnowCloud:
        item = new SnowCloudItem(map);
        break;
    default:
        item = new BaseItem(map);
        break;
    }
    return item;
}

BaseItem *EditorItemCreator::create(QJsonObject prop)
{
    QVariantMap map = prop.toVariantMap();
    return create(&map);
}
BaseItem *EditorItemCreator::create(QJsonObject* prop)
{
    QVariantMap map = prop->toVariantMap();
    return create(&map);
}

