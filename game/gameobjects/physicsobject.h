#ifndef PHYSICSOBJECT_H
#define PHYSICSOBJECT_H

#include "game/gameobjects/basegameobject.h"
#include "propertymodifier.h"

#include <QJsonObject>
#include <Box2D/Box2D.h>


namespace PhysicsLayer {
enum entityCategory {
   Collider           = 1,
   Sensor             = 1<<1,
   BulletPlayerTarget = 1<<2,
   BulletEnemyTarget  = 1<<3,
   Loot               = 1<<4,
   Jump               = 1<<5,
   WorldBorder        = 1<<6
};

}



class PhysicsObject : public BaseGameObject
{
    Q_OBJECT

    Q_PROPERTY_WITH_ACCESSORS(bool,  fixedRotation,   setFixedRotation)
    Q_PROPERTY_WITH_ACCESSORS(bool,  fullSize,   setFullSize)

public:    
    PhysicsObject(QJsonObject&, b2World*);
    PhysicsObject(QJsonObject&, b2World*, bool autoCreatePhysics);
    PhysicsObject(){}
    virtual ~PhysicsObject();
    QPointF physicsPos();
    QPointF visiblePos();

    QRectF boundingRect();
    virtual bool collideWith(int type);
    virtual void solveCollision(PhysicsObject*);
    virtual void endContact(PhysicsObject*);
    virtual void preSolve(PhysicsObject*, b2Contact * contact);
    double bottomY();
    void advance(int timeDelta);

    void glDraw(GlUtils* glutils);   
    b2Body* body;
    PropertyModifier* propertyModifier();


protected:
    virtual void createPhysics(b2World*);
    double m_ySpriteOffset=0;
    double m_damping = 8;
    b2BodyDef bodyDef;
    b2PolygonShape shape;
    b2FixtureDef fixtureDef;
    QRectF visibleObjectRect;
    QVector<int> collisionList;
    PropertyModifier* m_propertyModifier;

};


#endif // PHYSICSOBJECT_H
