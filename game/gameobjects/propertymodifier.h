#ifndef PROPERTYEFFECT_H
#define PROPERTYEFFECT_H

#include <QObject>
#include "./common/object.h"

class PropertyModifier : public Object
{
    Q_OBJECT

    PROPERTY(QString, name, setName)
    PROPERTY(double, value, setValue)


    PROPERTY(int, effectTime, setEffectTime)

    PROPERTY(int, workTime, setWorkTime)
    PROPERTY(bool, isApplied, setApplied)


public:

    enum Operation
    {
        Add,
        Substract,
        Multiply,
        Divide
    };
    Q_ENUM(Operation)
    PROPERTY(Operation, operation, setOperation)
    enum TimeMode
    {
        Once,
        Infinite,
        UntilFail,
        UntilFinish,
        Timed
    };
    Q_ENUM(TimeMode)
   PROPERTY(TimeMode, timeMode, setTimeMode)

   PropertyModifier();
   PropertyModifier(const char* name, int value);
   PropertyModifier(QString& name, int value);

   void addWorkTime(int time);

};

#endif // PROPERTYEFFECT_H
