#include "physicsobject.h"

PhysicsObject::PhysicsObject(QJsonObject& parameters,
                             b2World* world, bool autoCreatePhysics) : BaseGameObject(parameters)
{    
    m_propertyModifier = nullptr;
    m_fullSize = true;
    setup(parameters);   
    if (autoCreatePhysics)
    {
        createPhysics(world);        
        body->SetFixedRotation(m_fixedRotation);
        body->SetTransform(body->GetPosition(), m_rotation*(180.0/M_PI));
    }
}
PhysicsObject::PhysicsObject(QJsonObject& parameters,
                             b2World* world) : BaseGameObject(parameters)
{

    m_propertyModifier = nullptr;
    m_fullSize = true;
    setup(parameters);
    createPhysics(world);
    if (!m_fullSize)
    {
        m_sprite.setOffset(0, -m_height/6);
    }
    body->SetFixedRotation(m_fixedRotation);
    body->SetTransform(body->GetPosition(), m_rotation*(180.0/M_PI));
}
void PhysicsObject::createPhysics(b2World* world)
{    
    bodyDef.type = b2BodyType(m_physicsType);
    bodyDef.position.Set(m_x, m_y);
    body = world->CreateBody(&bodyDef);
    body->SetBullet(false);   
    fixtureDef.density =1;
    fixtureDef.friction = 0.1;
    fixtureDef.restitution = 0;    
    body->SetUserData(this);
    shape.SetAsBox(m_width/2, m_height/2);
    fixtureDef.shape = &shape;
    b2Filter filter;
    filter.categoryBits = PhysicsLayer::Collider;
    filter.maskBits = PhysicsLayer::Collider;
    fixtureDef.filter = filter;
    body->CreateFixture(&fixtureDef);   
}

void PhysicsObject::glDraw(GlUtils *glutils)
{   
    if (!m_visible) return;
    m_sprite.setPos(body->GetPosition());
    m_sprite.setAngle(body->GetAngle()*(180.0/M_PI));
    m_sprite.draw(glutils);
}

QPointF PhysicsObject::physicsPos()
{
    return QPointF(body->GetPosition().x ,
                   body->GetPosition().y );
}

QPointF PhysicsObject::visiblePos()
{
    if (m_physicsType == b2_staticBody)
    {
        return QPointF(m_x, m_y);
    }
    else
    {
        return QPointF(body->GetPosition().x - m_width / 2.0,
                       body->GetPosition().y - m_height / 2.0 );
    }
}



QRectF PhysicsObject::boundingRect()
{
    return QRectF( QPointF(body->GetPosition().x - m_width / 2.0 ,
                           body->GetPosition().y - m_height /2.0),
                   m_size);
}

void PhysicsObject::advance(int timeDelta)
{
    Q_UNUSED(timeDelta);
    body->SetLinearDamping(m_damping);
}

void PhysicsObject::solveCollision(PhysicsObject *obj)
{
    Q_UNUSED(obj);
}

void PhysicsObject::endContact(PhysicsObject *obj)
{
    Q_UNUSED(obj);
}

void PhysicsObject::preSolve(PhysicsObject *obj, b2Contact* contact)
{
    Q_UNUSED(obj);
    Q_UNUSED(contact);
}

double PhysicsObject::bottomY()
{
    return body->GetPosition().y + m_height/2;
}


PhysicsObject::~PhysicsObject()
{
    if(m_propertyModifier != nullptr) delete m_propertyModifier;
    body->GetWorld()->DestroyBody(body);
}

bool PhysicsObject::collideWith(int type)
{    
    return collisionList.contains(type);
}

PropertyModifier* PhysicsObject::propertyModifier()
{
    return m_propertyModifier;
}

