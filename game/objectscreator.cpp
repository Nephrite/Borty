#include "objectscreator.h"
#include "game/gameobjects/basegameobject.h"
#include "game/gameobjects/parallaxbackground.h"
#include "game/gameobjects/actors/frog.h"
#include "game/gameobjects/actors/wind.h"
#include "game/gameobjects/actors/waterblock.h"
#include "game/gameobjects/decorobject.h"
#include "game/gameobjects/physicsobject.h"
#include "game/gameobjects/endlevelblock.h"
#include "game/gameobjects/actors/movingplatform.h"
#include "game/gameobjects/actors/coin.h"
#include "game/gameobjects/actors/springbug.h"
#include "game/gameobjects/billboardphysicsobject.h"
#include "game/gameobjects/staticbackground.h"
#include "game/gameobjects/actors/gribbo.h"
#include "game/gameobjects/actors/hen.h"
#include "game/gameobjects/tooltipobject.h"
#include "game/gameobjects/actors/npc.h"
#include "game/gameobjects/ammo.h"
#include "gameobjects/animateddecorobject.h"
#include "gameobjects/swinginggrass.h"
#include "gameobjects/actors/flame.h"
#include "gameobjects/actors/snowcloud.h"
#include "common/objecttypes.h"

#include <QDebug>

ObjectsCreator::ObjectsCreator()
{
}

void ObjectsCreator::setWorld(b2World * w)
{
    world = w;
}

BaseGameObject* ObjectsCreator::create(QJsonObject &parameters)
{
    BaseGameObject* object = NULL;

    int t = parameters.value("type").toInt();
    if (t == 0) t = parameters.value("objectType").toInt();

    ///qDebug() << "create" << t << " is " + parameters.value("texture").toString();

    switch (t)
    {
    case type::PhysicsObject : {
        object = new PhysicsObject(parameters, world);
        break;
    }
    case type::frog: {
        object = new frog(parameters, world);
        break;
    }
    case type::decorObject: {
        object = new decorObject(parameters);
        break;
    }
    case type::EndLevelBlock: {
        object = new EndLevelBlock(parameters, world);
        break;
    }
    case type::Wind: {
        object = new Wind(parameters, world);
        break;
    }
    case type::WaterBlock: {
        object = new WaterBlock(parameters, world);
        break;
    }
    case type::ParallaxBackground: {
        object = new ParallaxBackground(parameters);
        break;
    }
    case type::MovingPlatform: {
        object = new MovingPlatform(parameters, world);
        break;
    }
    case type::Coin:{
        object = new Coin(parameters, world);
        break;
    }
    case type::SpringBug:{
        object = new SpringBug(parameters, world);
        break;
    }
    case type::BillboardPhysicsObject:{
        object = new BillboardPhysicsObject(parameters, world);
        break;
    }
    case type::StaticBackground:{
        object = new StaticBackground(parameters);
        break;
    }
    case type::Gribbo:{
        object = new Gribbo(parameters, world);
        break;
    }
    case type::Hen:{
        object = new Hen(parameters, world);
        break;
    }
    case type::ToolTipObject:{
        object = new TooltipObject(parameters);
        break;
    }
    case type::NPC:{
        object = new NPC(parameters, world);
        break;
    }
    case type::Ammo:{
        object = new Ammo(parameters, world);
        break;
    }
    case type::AnimatedDecorObject:{
        object = new AnimatedDecorObject(parameters);
        break;
    }
    case type::SwingingGrass:{
        object = new SwingingGrass(parameters);
        break;
    }
    case type::Flame:{
        object = new Flame(parameters);
        break;
    }
    case type::SnowCloud:{
        object = new SnowCloud(parameters);
        break;
    }
    }
    if (object != NULL){ return object;  }

    return NULL;
}
