#include "gamedata.h"

GameData::GameData() :
    m_playerPos(b2Vec2(0,0)),
    m_isPlayerFriendly(false),
    m_isPlayerAngry(false),
    m_attackCount(0),
    m_maxAttackCount(2),
    m_enemyCount(0)
{

}

void GameData::registerEnemy()
{
    m_enemyCount++;
}

void GameData::reset()
{
    m_playerPos = b2Vec2(0,0);
    m_isPlayerFriendly = false;
    m_isPlayerAngry = false;
    m_attackCount = 0;
    m_maxAttackCount = 3;
    m_enemyCount = 0;
}

void GameData::addBullet(BaseGameObject *b)
{
    m_newBullets.append(b);
}

void GameData::bulletsRegisteredNotify()
{
    m_newBullets.clear();
}

QList<BaseGameObject* >& GameData::newBullets()
{
    return m_newBullets;
}
