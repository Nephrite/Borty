#include "editor/editorscene.h"
#include "editor/editorwindow.h"

#include "editor/items/baseitem.h"
#include "editor/autofilldialog.h"

#include <QObject>
#include <QDebug>
#include <QFile>
#include <QFileDialog>
#include <QMouseEvent>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QVariantMap>

#include <QGraphicsItem>
#include <QGraphicsSceneMouseEvent>

#include <math.h>
#include <algorithm>

EditorScene::EditorScene(QWidget *parent) :
    QGraphicsScene(parent)
{ 
    newProperties = nullptr;
    setBackgroundBrush(QBrush(QPixmap("./resources/grid.png")));
    scale = 64;  
    m_ObjectCounter = 0;
}

void EditorScene::setEditorWindow(EditorWindow* window)
{
    editorWindow = window;
}

void EditorScene::enableGrid(bool value)
{
    emit enableGridToItems(value);
}

void EditorScene::setGridSize(int size)
{    
    emit setGridSizeToItems(size);
}

void EditorScene::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if (   event->button()   == Qt::LeftButton
        && m_behaviour.mode    == ItemBehaviour::select
        && event->modifiers() & Qt::ControlModifier
       )
    {
        auto* itemUnderMouse = itemAt(event->scenePos(), QTransform());
        if (itemUnderMouse)
        {
            if (itemUnderMouse->isEnabled() &&
                    itemUnderMouse->flags() & QGraphicsItem::ItemIsSelectable)
                itemUnderMouse->setSelected(!itemUnderMouse->isSelected());
        }
    }
    QGraphicsScene::mousePressEvent(event);
}

void EditorScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if (event->button() == Qt::RightButton)
    {
        createBlock(event->scenePos().x(), event->scenePos().y());
    }
    if (event->button() == Qt::LeftButton && m_behaviour.snapToGrid)
    {
        for (auto& item : selectedItems())
        {
            BaseItem* i = dynamic_cast<BaseItem*>(item);
            if (i) i->toGrid();
        }
    }
    QGraphicsScene::mouseReleaseEvent(event);

}

void EditorScene::setNewProperties(QJsonObject* newData)
{
    newProperties = newData;
}

void EditorScene::createBlock(int x, int y)
{
    if (newProperties == nullptr) return;
    BaseItem* block = objectCreator.create(newProperties);

    if (block->hasHandles()) block->setupHandles();
    QObject::connect(block,
                     &BaseItem::editProperties,
                     editorWindow,
                     &EditorWindow::setObjectForEditing
                     );

    if (block->objectName().isEmpty())
        block->setObjectName("object_"+ block->texture() + QString::number(++m_ObjectCounter));
    block->setBehaviour(&m_behaviour);    
    block->setPosition(x,y);
    block->toGrid();
    this->addItem(block);
}

void EditorScene::keyPressEvent(QKeyEvent *event)
{
    switch ( event->key() )
    {
    case  Qt::Key_Delete :
    {
        for (auto& item : this->selectedItems())
        {            
            BaseItem* it = dynamic_cast<BaseItem*>(item);
            if (it)
            {
                removeItem(item);
                it->deleteLater();
            }
        }
        editorWindow->removeObjectForEditing();
        break;
    }
    case Qt::Key_Down :
    {
        for (auto& item : this->selectedItems())
        {
            item->setPos(item->pos().x(), item->pos().y()+m_behaviour.arrowStep);
        }
        break;
    }
    case Qt::Key_Up :
    {
        for (auto& item : this->selectedItems())
        {
            item->setPos(item->pos().x(), item->pos().y()-m_behaviour.arrowStep);
        }
        break;
    }
    case Qt::Key_Right :
    {
        for (auto& item : this->selectedItems())
        {
            item->setPos(item->pos().x() + m_behaviour.arrowStep, item->pos().y());
        }
        break;
    }
    case Qt::Key_Left :
    {
        for (auto& item : this->selectedItems())
        {
            item->setPos(item->pos().x() - m_behaviour.arrowStep, item->pos().y());
        }
        break;
    }
    case Qt::Key_PageUp :
    {
        for (auto& item : this->selectedItems())
        {
            item->setZValue(item->zValue() + 1);
        }
        break;

    }
    case Qt::Key_PageDown :
    {
        for(auto& item : this->selectedItems())
        {
            item->setZValue(item->zValue() - 1);
        }
        break;
    }
    case Qt::Key_C :
    {
        if (QApplication::keyboardModifiers() | Qt::ControlModifier)
        {
            if (selectedItems().count() > 0)
            {
                BaseItem* item = dynamic_cast<BaseItem*>(selectedItems().at(0));
                if (item )
                {
                    QVariantMap m = item->toMap();
                    buffJsonObject = QJsonObject::fromVariantMap(m);
                    newProperties = &buffJsonObject;
                }
            }
        }
        break;
    }
    }
    event->accept();

}


void EditorScene::showAutofillDialog()
{
    AutofillDialog* dial = new AutofillDialog;
    if (newProperties != nullptr) dial->setJson(newProperties);
    dial->exec();
    if (dial->result() == QDialog::Accepted)
    {
        QJsonObject prop = QJsonDocument::fromJson(dial->blockJson().toUtf8()).object();
        newProperties = &prop;
        int count = 0;
        for (int row = 0; row < dial->rowCount(); row++)
        {
             for (int col = 0; col < dial->columnCount(); col++)
             {
                 if (count < dial->blockCount())
                 {
                     createBlock(dial->startX()+col*dial->horizontalStep() + prop.value("height").toInt()/2,
                                 dial->startY()+row*dial->verticalStep()   + prop.value("width").toInt() /2
                                 );
                 }
                 count++;
             }
        }
    }

    delete dial;
}


void EditorScene::newLevel()
{
    levelProperties()->setAmbientLight(Qt::white);
    levelProperties()->setBackColor(Qt::black);
    clear();
    m_currentFile.clear();   
    m_loaded = true;
}

void EditorScene::saveToFile(QString fileName)
{
    if (fileName.isEmpty()) return;
    m_currentFile = fileName;
    QDir d;
    d.remove(fileName);
    QFile file(fileName);
    if (file.open(QFile::ReadWrite))
    {
        file.write(toJson());
    }
    file.close();
}

void EditorScene::saveToFile()
{
    saveToFile(m_currentFile);
}


void EditorScene::loadLevelFromFile(QString path)
{
    QFile jsonFile(path);
    if ( ! jsonFile.open(QFile::ReadOnly))
    {
        qDebug() << "cannot open level file " << path;
        return;
    }
    editorWindow->removeObjectForEditing();
    m_currentFile = path;
    loadLevelFromJsonString(jsonFile.readAll());

}
void EditorScene::loadLevelFromJsonString(QString json)
{
    QJsonDocument document = QJsonDocument::fromJson(json.toUtf8());
    editorWindow->removeObjectForEditing();
    setupLevel(&document);
}

void EditorScene::setupLevel(QJsonDocument *document)
{
    clear();
    QJsonArray map = document->object().value("map").toArray();
    QVariantMap prop = document->object().value("properties").toObject().toVariantMap();
    m_levelProperties.setup(&prop);
    for (const QJsonValue & value : map)
    {
        BaseItem* item = objectCreator.create(value.toObject());
        addItem(item);
        if (item->hasHandles()) item->setupHandles();
        item->setBehaviour(&m_behaviour);
        QObject::connect(item,
                        &BaseItem::editProperties,
                         editorWindow,
                        &EditorWindow::setObjectForEditing
                        );        
    }
    m_loaded = true;

}

void EditorScene::clear()
{
    for (auto& item : items())
    {      
        if (item->type() == QGraphicsItem::UserType)
        {
            BaseItem* it = dynamic_cast<BaseItem*>(item);
            if (it)
            {
                removeItem(it);
                it->deleteLater();
            }
        }
    }
    newProperties = nullptr;
    QGraphicsScene::clear();    
    m_ObjectCounter = 0;
}

QByteArray EditorScene::toJson()
{    
    QJsonArray array;
    QList <QGraphicsItem*> allItems = items();
    std::sort(allItems.begin(),
              allItems.end(),
              [](QGraphicsItem* a, QGraphicsItem* b) { return a->zValue() < b->zValue(); }
    );    
    for (QGraphicsItem* gitem : allItems)
    {
        if (gitem->type() == QGraphicsItem::UserType) // BaseItem::type == QGraphicsItem::type
        {
            BaseItem* item = qgraphicsitem_cast<BaseItem*>(gitem);
            if (item )
            {               
                array.append( QJsonObject::fromVariantMap(item->toMap()));
            }
        }
    }
    QJsonObject obj;
    obj.insert("map", array);

    QJsonObject properties = QJsonObject::fromVariantMap(m_levelProperties.toMap());
    properties.insert("physicsScale", QJsonValue(scale));
    obj.insert("properties", properties);
    QJsonDocument doc(obj);

    return doc.toJson();
}


bool EditorScene::loaded()
{
    return m_loaded;
}






