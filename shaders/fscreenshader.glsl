#version 130
#ifdef GL_ES

precision mediump int;
precision mediump float;
#endif

uniform sampler2D backtex;
uniform sampler2D objtex;
uniform sampler2D lighttex;

varying vec2 v_texcoord;
uniform vec4 ambient;
uniform float fade;

void main()
{
    vec4 back, fore, light, resultColor;
    //-------------------
    back = texture2D(backtex, v_texcoord);
    fore = texture2D(objtex, v_texcoord);
    light = texture2D(lighttex, v_texcoord);
    //-------------------


    back.rgb *=  (ambient.rgb + (light.rgb*0.5));
    fore.rgb *=  (ambient.rgb + light.rgb*0.2);

    resultColor.a = back.a + fore.a *(1.0-back.a);
    resultColor.rgb = ((fore.rgb + back.rgb*(1.0-fore.aaa))/resultColor.a) ;

    gl_FragColor =(resultColor)*fade;

}
