#include "pausemenu.h"
#include "ui_pausemenu.h"
#include <QPainter>
#include <QKeyEvent>

PauseMenu::PauseMenu(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PauseMenu)
{
    ui->setupUi(this);   
}

PauseMenu::~PauseMenu()
{
    delete ui;
}

void PauseMenu::on_pushButton_2_clicked()
{    
    emit continueGame();
}

void PauseMenu::on_pushButton_clicked()
{    
    emit showMenu();
}


void PauseMenu::paintEvent(QPaintEvent *e)
{
    QPainter pa(this);
    pa.drawPixmap(0,
                  0,
                  back.scaled( width(),
                               height(),
                               Qt::KeepAspectRatioByExpanding
                              )
                  );
    QWidget::paintEvent(e);
}

void PauseMenu::setBackground(QImage *pix)
{
    back = QPixmap::fromImage(*pix);
}

void PauseMenu::keyPressEvent(QKeyEvent *e)
{
    if (e->key() == Qt::Key_Escape)
        emit continueGame();
    QWidget::keyPressEvent(e);
}



