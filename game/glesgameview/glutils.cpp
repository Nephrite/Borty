#include "glutils.h"

#include <GL/gl.h>
#include <QOpenGLTexture>

#include <QDebug>
#include <QFile>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>

GlUtils::GlUtils()
{

}

void GlUtils::initializeShaders()
{

    qDebug() << "Loading shaders";
    QFile shaderConfigFile("./shaders/shaders.json");
    if (!shaderConfigFile.open(QFile::ReadOnly))
    {
        qDebug() <<"GlUtils: Can't open shaders config from file" << shaderConfigFile.fileName();
        return;
    }

    QJsonParseError err;
    QJsonDocument config = QJsonDocument::fromJson(shaderConfigFile.readAll(), &err);

    if (err.error != err.NoError)
    {
        qDebug()<< "GLutils: error parse shader list "<< err.errorString();
        return;
    }

    QJsonArray list = config.object().value("shaderList").toArray();

    bool ok = true;
    for (auto val : list)
    {
        QJsonObject shader = val.toObject();
        QString name = shader.value("name").toString();
        QString vertexProgram =  shader.value("vertex").toString();
        QString fragmentProgram =  shader.value("fragment").toString();
        QOpenGLShaderProgram* program = loadShader(name, vertexProgram, fragmentProgram);
        if (program != nullptr)
        {
             qDebug()<< "create shader " << name << " log " << program->log();
            m_shaders.insert(name, program);
        }
        else
        {
            ok = false;
            qDebug()<< "Can't create shader " << name;
        }
    }

    if (!ok)
    {
        qDebug()<<"Some shaders not loaded! ";
    }
    else
    {
        qDebug() << "All shaders loaded";
    }
    m_currentShader = m_shaders.first();

}

QOpenGLShaderProgram *GlUtils::loadShader(QString& name, QString& vertexfile, QString& fragmentFile)
{
    QOpenGLShaderProgram* prog = new QOpenGLShaderProgram();

    if (!prog->addShaderFromSourceFile(QOpenGLShader::Vertex, vertexfile))
        qDebug()<<"GlUtils:cant add shader: " << vertexfile;

    if (!prog->addShaderFromSourceFile(QOpenGLShader::Fragment, fragmentFile))
        qDebug()<<"GlUtils:cant add shader: " << fragmentFile;

    if (!prog->link())
        qDebug()<<"GlUtils:cant link shader: "<< fragmentFile;

    prog->setObjectName(name);
    if (prog->isLinked())
        return prog;
    else
    {
        qDebug()<<"GlUtils:cant load shader";
        delete prog;
        return nullptr;
    }
}

QOpenGLShaderProgram* GlUtils::getShaderByName(QString name)
{
    if (m_shaders.contains(name))
        return m_shaders.value(name);
    else return nullptr;
}


void GlUtils::setShader(QString name)
{
    if (m_currentShader->objectName() == name)

    if (!m_shaders.contains(name))
    {
        qDebug() << "Cant' select shader" << name;
    }
    setShader(m_shaders.value(name));
}

void GlUtils::setShader(QOpenGLShaderProgram *program)
{
    if (program == nullptr)
    {
        qDebug() << "Glutils: attempt to bind null shader! \n maybe crash in few seconds :(";
        return;
    }   
    m_currentShader = program;
    m_currentShader->bind();
    glActiveTexture(GL_TEXTURE0);
    m_currentShader->setUniformValue("mvp_matrix", m_projection * m_objectMatrix);
    m_currentShader->setUniformValue("texture", 0);
    quintptr offset = 0;
    int vertexLocation = m_currentShader->attributeLocation("a_position");
    m_currentShader->enableAttributeArray(vertexLocation);
    m_currentShader->setAttributeBuffer(vertexLocation, GL_FLOAT, offset, 3, sizeof(VertexData));

    offset += sizeof(QVector3D);

    int texcoordLocation = m_currentShader->attributeLocation("a_texcoord");
    m_currentShader->enableAttributeArray(texcoordLocation);
    m_currentShader->setAttributeBuffer(texcoordLocation, GL_FLOAT, offset, 2, sizeof(VertexData));


}

void GlUtils::setContext(QOpenGLContext * c)
{
    m_context = c;
    initializeOpenGLFunctions();
}



void GlUtils::releaseShader()
{
    m_currentShader->release();
    glActiveTexture(GL_TEXTURE0);
}

void GlUtils::setView(QRectF rect)
{
    m_projection.setToIdentity();
    m_projection.ortho( rect.x()
                      , rect.x()+rect.width()
                      , rect.y()+rect.height()
                      , rect.y()
                      ,-100
                      , 100
                      );

}


void GlUtils::translate(GLfloat x, GLfloat y, GLfloat z)
{
    m_objectMatrix.translate(x, y, z);
}

void GlUtils::lookAt(GLfloat x, GLfloat y, GLfloat z, GLfloat angle)
{
    m_objectMatrix.setToIdentity();
    m_objectMatrix.translate(x, y, z);
     if (fabs(angle) > 0.001)  m_objectMatrix.rotate(angle, 0,0,1);
}

void GlUtils::lookAt(GLfloat x, GLfloat y, GLfloat z)
{
    m_objectMatrix.setToIdentity();
    m_objectMatrix.translate(x, y, z);
}
void GlUtils::lookAt(b2Vec2 point, GLfloat z, GLfloat angle)
{
    m_objectMatrix.setToIdentity();
    m_objectMatrix.translate(point.x, point.y, z);
    m_objectMatrix.rotate(angle,0,0,1);
}

void GlUtils::lookAt(b2Vec2 point, GLfloat z)
{
    m_objectMatrix.setToIdentity();
    m_objectMatrix.translate(point.x, point.y, z);
}

void GlUtils::addTextureToShader(const char * uniform, QOpenGLTexture* texture, GLuint target)
{
    glActiveTexture(target);
    texture->bind();  
    m_currentShader->setUniformValue(uniform, texture->textureId());
    GLuint location = glGetUniformLocation(m_currentShader->programId(), uniform);
    glUniform1i(location, target-GL_TEXTURE0);

}

void GlUtils::addTextureToShader(const char *uniform, GLuint textureId, GLuint target)
{
    glActiveTexture(target);
    glBindTexture(GL_TEXTURE_2D, textureId);
    m_currentShader->setUniformValue(uniform, textureId);
    GLuint location = glGetUniformLocation(m_currentShader->programId(), uniform);
    glUniform1i(location, target-GL_TEXTURE0);
}

