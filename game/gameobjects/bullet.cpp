
#include "bullet.h"
#include "common/objecttypes.h"
#include "game/gameobjects/decorobject.h"

Bullet::Bullet(QJsonObject& parameters, b2World *world)
    : PhysicsObject(parameters, world)
{
    setup(parameters);
    m_state = fly;
    body->SetType(b2_dynamicBody);
    body->SetFixedRotation(false);
    body->SetBullet(true);

    m_propertyModifier = nullptr;
}

Bullet::Bullet(type::bullets t, b2World *world) :
    m_affectProperty(QString("health")),
    m_value(-1),
    m_isTasty(true),
    m_bulletType(t),
    m_target(AllTarget),
    m_timer(0)
{
    m_height = 32/64.0;
    m_width = 32/64.0;
    m_ySpriteOffset = -1;
    m_maxVelocity = 8;
    switch (t) {
    case type::Note:
        m_texture = "note.png";
        m_isTasty = false;
        break;
    case type::Apple:
         m_texture = "apple.png";
         m_isTasty = true;
        break;
    case type::Tomato:
         m_texture = "tomato.png";
         m_isTasty = true;
        break;
    case type::Egg:
         m_texture = "egg.png";
         m_isTasty = false;
         m_height = 40/64.0;
         m_width = 32/64.0;
        break;
    case type::Carrot:
         m_texture = "carrot.png";
         m_isTasty = true;
         m_height = 45/64.0;
         m_width = 25/64.0;
        break;
    case type::Snowball:
         m_texture = "snowball.png";
         m_isTasty = false;
         m_height = 32/64.0;
         m_width = 32/64.0;
        break;
    case type::Pumpkin:
         m_texture = "pumpkin.png";
         m_isTasty = false;
         m_height = 86/64.0;
         m_width = 86/64.0;
         m_value = -10;
         m_ySpriteOffset = -86/64.0/2-0.5;
         m_maxVelocity = 4;
        break;
    default:
        break;
    }

    m_textureStyle = "bullet";
    m_objectType = type::Bullet;
    m_visible = true;
    m_active = true;
    m_horizontalMapping=(Drawable::StretchedMapping);
    m_verticalMapping=(Drawable::StretchedMapping);

    m_state = fly;
    createPhysics(world);

    body->SetType(b2_dynamicBody);
    body->SetFixedRotation(false);
    body->SetBullet(true);

    setupSprite();

    m_sprite.setShaderName("animatedSprite");

    m_shadowSprite.addTexture("./resources/textures/shadow_small.png");
    m_shadowSprite.setOffset(0, 0);
    m_shadowSprite.setAsBox(m_width, m_height/2);
    m_shadowSprite.setPos(body->GetPosition());

    m_propertyModifier = new PropertyModifier(m_affectProperty, m_value);
    m_propertyModifier->setTimeMode(PropertyModifier::Once);
}

Bullet::~Bullet()
{

}

void Bullet::createPhysics(b2World *world)
{
    bodyDef.type = b2BodyType(m_physicsType);
    bodyDef.position.Set(m_x, m_y);
    body = world->CreateBody(&bodyDef);
    body->SetBullet(false);
    fixtureDef.density =1;
    fixtureDef.friction = 0.1;
    fixtureDef.restitution = 0;
    body->SetUserData(this);

    circle.m_radius = (m_height+m_width)/4.0;
    fixtureDef.shape = &circle;
    b2Filter filter;
    filter.categoryBits = PhysicsLayer::Collider;
    filter.maskBits = PhysicsLayer::Collider;
    fixtureDef.filter = filter;
    body->CreateFixture(&fixtureDef);
}

void Bullet::disable()
{
    b2Filter filter;
    filter.categoryBits = 0;
    filter.maskBits = 0;
    b2Fixture* fix;
    fix = body->GetFixtureList();
    fix->SetFilterData(filter);
    m_state = fading;
    if (m_bulletType == type::Pumpkin)
    {
        QJsonObject prop;
        prop.insert("objectType", type::decorObject);
        prop.insert("width", 162.0);
        prop.insert("height", 59.0);
        prop.insert("textureStyle", "food");
        prop.insert("texture", "pumpkin_broken.png");
        prop.insert("x", body->GetPosition().x*64+59/2);
        prop.insert("y", body->GetPosition().y*64+0.5);
        emit addNewObject(new decorObject(prop));
    }
}

Bullet::Bullet(b2World *world) :
    m_affectProperty(QString("health")),
    m_value(-1),
    m_isTasty(true),
    m_target(EnemyTarget),
    m_timer(0)
{
    m_height = 32/64.0;
    m_width = 32/64.0;
    m_textureStyle = "bullet";
    m_texture = "apple.png";
    m_metaType = "bullet";
    m_objectType = type::Bullet;
    m_visible = true;
    m_active = true;
    m_horizontalMapping=(Drawable::StretchedMapping);
    m_verticalMapping=(Drawable::StretchedMapping);

    m_state = fly;
    createPhysics(world);

    body->SetType(b2_dynamicBody);
    body->SetFixedRotation(false);
    body->SetBullet(true);

    setupSprite();
    m_ySpriteOffset = 2;
   // m_sprite.setOffset(0, m_ySpriteOffset);
    m_sprite.setShaderName("default");

    m_shadowSprite.addTexture("./resources/textures/shadow_small.png");
    m_shadowSprite.setAsBox(m_width, m_height/2);
   // m_shadowSprite.setOffset(0, m_height/4);
    m_shadowSprite.setPos(body->GetPosition());

}

bool Bullet::collideWith(int type)
{
    Q_UNUSED(type);
    return true;
}

void Bullet::preSolve(PhysicsObject *object, b2Contact *contact)
{
    Q_UNUSED(object);
    contact->SetEnabled(false);
    if (!object->collideWith(m_objectType)) disable();
}

void Bullet::glDraw(GlUtils *glutils)
{
    m_shadowSprite.setPos(body->GetPosition());
    m_shadowSprite.draw(glutils);
   // m_sprite.setOffset(0,m_ySpriteOffset);
    PhysicsObject::glDraw(glutils);
}

void Bullet::setPos(b2Vec2 pos, double offset)
{
    m_ySpriteOffset = offset;   
    m_shadowSprite.setOffset(0,0);

    if(m_bulletType != type::Pumpkin)
    {
        body->SetTransform(pos+b2Vec2(0, m_height/2), 0);
        m_sprite.setOffset(0, m_ySpriteOffset);
    }
    else
    {
        body->SetTransform(pos-b2Vec2(0, circle.m_radius), 0);
    }
}

double Bullet::bottomY()
{
    return body->GetPosition().y + m_height/2;
}

void Bullet::advance(int timeDelta)
{
    if (! m_active) return;
    BaseGameObject::advance(timeDelta);
    if (m_state == fading)
    {
        //m_sprite.setOffset(0, -m_sprite.opacity());
        m_sprite.addOpacity(-0.1);
        if (sprite()->opacity() <0.1)
        {
           deleteMe(this);
        }
    }
    m_timer += timeDelta;
    if (m_timer > 5000)
    {
        m_timer = 0;
        m_state = fading;
    }
    if (body->GetAngularVelocity() < m_height*3.14)
        body->ApplyTorque(m_height*3.14);
    body->SetLinearVelocity(moveVec);
}

void Bullet::onScreenLeave()
{
    m_state = fading;
}

void Bullet::setDirection(Direction::directions dir)
{
    m_direction = dir;
    moveVec = Direction::dirToVector(m_direction);
    moveVec*=m_maxVelocity;
}

void Bullet::setTarget(Bullet::BulletTarget t)
{
    m_target = t;
    b2Filter filter;
    filter.maskBits =  PhysicsLayer::Collider | PhysicsLayer::Sensor;
    switch (m_target) {
    case PlayerTarget:
        filter.categoryBits = PhysicsLayer::Sensor|PhysicsLayer::Collider | PhysicsLayer::BulletPlayerTarget;
        break;

    case EnemyTarget:
        filter.categoryBits = PhysicsLayer::Sensor|PhysicsLayer::Collider | PhysicsLayer::BulletEnemyTarget;

        break;
    case AllTarget:
        filter.categoryBits = PhysicsLayer::Sensor|PhysicsLayer::Collider | PhysicsLayer::BulletEnemyTarget | PhysicsLayer::BulletPlayerTarget;
        break;
    default:
        break;
    }
    body->GetFixtureList()[0].SetFilterData(filter);
}
