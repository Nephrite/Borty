#ifndef GAMECONTROLLER_H
#define GAMECONTROLLER_H

#include <QStackedWidget>

#include "glesgameview/glesgameview.h"

#include "ui/worldMap/worldmap.h"
#include "ui/mainmenu.h"
#include "ui/pausemenu.h"
#include "game/gameobjects/player.h"
#include "headup.h"
#include "ui/endlevelscreen.h"
#include "ui/saveselectwidget.h"
#include <QSharedPointer>
#include <QTime>

class GameController : public QObject
{
    Q_OBJECT
public:

    enum gameState{game, finishwaiting, finish};

    GameController();
    ~GameController();
    void setGameWidget(QStackedWidget* widget);
    void setupGameWidget();
    void setLevel(QString file);

public slots:

    void pause();
    void start();
    void startNextLevel();
    void continueGame();
    void advance();
    void stopGame();
    void showLevelPackWidget();
    void showMenu();
    void exitGame();
    void showGameView();
    void showEndLevelScreen();
    void showSaveSelectWidget();
    void levelFinished(Player::finishReason reason);
    void onRunEditor();

private:
    void setupMainMenu();
    void setupPauseMenu();
    void setupLevelSelectWidget();
    void setupEndLevelScreen();
    void setupSaveSelectWidget();

    void finishLevel();

    gameState m_gameState;

    bool wait;
    bool isLevelFinished;
    bool m_paused;
    int m_timeDelta;
    int m_advanceTime;
    QString m_levelFile;
    GlesGameView* p_gameView;
    GameScene* p_gameScene;
    HeadUp* p_headUp;

    WorldMap* p_levelSelectWidget;
    QStackedWidget* p_gameWidget;
    MainMenu* p_mainMenu;
    PauseMenu* p_pauseMenu;
    EndLevelScreen* p_endLevelScreen;
    SaveSelectWidget* m_saveSelectWidget;
    QTimer* p_timer;
    QTime m_time;
    Player* p_playerptr;
    Player::finishReason m_finishReason;
    int m_elapsedTime;
};

#endif // GAMECONTROLLER_H
