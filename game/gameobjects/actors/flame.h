#ifndef FLAME_H
#define FLAME_H

#include <QObject>
#include <QOpenGLTexture>
#include "game/gameobjects/basegameobject.h"

class Flame : public BaseGameObject
{
    Q_OBJECT
    Q_PROPERTY_WITH_ACCESSORS(QString,  stencilTexture,   setStencilTexture)
    Q_PROPERTY_WITH_ACCESSORS(QString,  sparcleTexture,   setSparcleTexture)
public:
    Flame();
    Flame(QJsonObject& parameters);
    ~Flame();
    void setupSprite();
    void advance(int timedelta);
protected:

    GLuint m_stencil;
    GLuint m_map;
    GLuint m_mainTex;

};

#endif // FLAME_H
