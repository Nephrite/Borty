#ifndef PLAYERSTATISTICS_H
#define PLAYERSTATISTICS_H

#include <common/object.h>
#include <QObject>

class PlayerStatistics : public Object
{
    Q_OBJECT

    PROPERTY(int, coins, setCoins)
    PROPERTY(int, lives, setLives)
    PROPERTY(int, health, setHealth)

public:
    PlayerStatistics();
    PlayerStatistics(QJsonObject& obj);
    ~PlayerStatistics();
};

#endif // PLAYERSTATISTICS_H
