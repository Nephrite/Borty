#include "playerstatistics.h"
#include "common/save.h"

PlayerStatistics::PlayerStatistics():
    m_coins(0),
    m_lives(5),
    m_health(5)
{
    setObjectName("PlayerStat");
    Save::instance()->registerSavable(this);
    Save::instance()->restoreState(this);
}

PlayerStatistics::~PlayerStatistics()
{
    Save::instance()->unregisterSavable(this);
}

PlayerStatistics::PlayerStatistics(QJsonObject& obj):
    m_coins(0),
    m_lives(5),
    m_health(5)
{
    setObjectName("PlayerStat");
    Save::instance()->registerSavable(this);
    setup(obj);
}
