#include "serializable.h"
#include <QDebug>
Serializable::Serializable(QObject *parent) : QObject(parent)
{

}

Serializable::Serializable(QVariantMap * map)
{
    setup(map);
}

Serializable::~Serializable()
{

}

void Serializable::setup(QVariantMap * map)
{
    QString key;   
    for (int i = 0;
             i < metaObject()->propertyCount();
             i++
         )
    {
        key = QString(metaObject()->property(i).name());
        if (map->contains(key) && metaObject()->property(i).isDesignable())
        {
            if (metaObject()->property(i).isEnumType())
            {
                setProperty(key.toLatin1(), map->value(key).toInt());
            }
            else{
                setProperty(key.toLatin1(), map->value(key));
            }
        }
    }
}

QVariantMap Serializable::toMap()
{
    QVariantMap map;
    for(int i = 0; i<metaObject()->propertyCount(); i++)
    {
        if(   metaObject()->property(i).isStored(this)
           && metaObject()->property(i).isUser()
          )
        {
            map.insert( metaObject()->property(i).name()
                      , metaObject()->property(i).read(this));
        }
    }
    map.insert("childs", m_ChildsProperties);
    return map;
}
