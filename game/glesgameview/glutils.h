#ifndef GLUTILS_H
#define GLUTILS_H

#include <QOpenGLShaderProgram>
#include <QGLFunctions>
#include <QMatrix4x4>
#include "Box2D/Box2D.h"

#include <QOpenGLTexture>
struct VertexData
{
    QVector3D position;
    QVector2D texCoord;
};


class GlUtils : public QOpenGLFunctions
{
public:
    GlUtils();
    void initializeShaders();
    void setContext(QOpenGLContext*);

    void setShader(QString name);
    void setShader(QOpenGLShaderProgram* program);
    void releaseShader();
    void setView(QRectF rect);
    void translate(GLfloat x, GLfloat y, GLfloat z);
    void lookAt(GLfloat x, GLfloat y, GLfloat z, GLfloat angle);
    void lookAt(GLfloat x, GLfloat y, GLfloat z);
    void lookAt(b2Vec2 point, GLfloat z, GLfloat angle);
    void lookAt(b2Vec2 point, GLfloat z);
    void addTextureToShader(const char* uniform, QOpenGLTexture* texture, GLuint target);
    void addTextureToShader(const char* uniform, GLuint textureId, GLuint target);
    QOpenGLShaderProgram* loadShader(QString &name, QString &vertexfile, QString &fragmentFile);
    QColor m_ambientColor;
    GLuint m_backTextureId;
    GLuint m_foreTextureId;
    GLuint m_lightTextureId;
    QMatrix4x4 m_projection;
    QOpenGLShaderProgram* shader(){return m_currentShader;}
    QOpenGLShaderProgram *getShaderByName(QString name);
    QOpenGLShaderProgram* m_currentShader;
    QMatrix4x4 m_objectMatrix;

    GLuint m_time;
    void setTime(GLuint t){m_time = t;}

    QMap<QString, QOpenGLShaderProgram*> m_shaders;

protected:
    QOpenGLContext* m_context;
};

#endif // GLUTILS_H
