#ifndef QPROPERTYACCESSMACRO_H
#define QPROPERTYACCESSMACRO_H


#define Q_PROPERTY_WITH_ACCESSORS(type, name, setter)   \
    protected: type m_##name;                           \
    public: Q_PROPERTY(type name READ name WRITE setter)\
    type const& name () const { return m_##name; }      \
    void setter (type const &v) { m_##name = v; }

#define Q_MEMBER_WITH_ACCESSORS(type, name, setter)                                \
                  protected: type m_##name;                                              \
                  public:                                                                \
                  type const& name () const { return m_##name; }                         \
                  public: void setter (type const &v) { m_##name = v; }                          \


#endif // QPROPERTYACCESSMACRO_H

