#include "editorsceneview.h"

#include <QDebug>
#include <QWheelEvent>
#include <QMouseEvent>
#include <QGraphicsItem>
EditorSceneView::EditorSceneView(QWidget *parent)
{
    setParent(parent);
    setInteractive(true);
    setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    middlePressed = false;    
}

void EditorSceneView::setScale(qreal x, qreal y)
{    
    scale(x,y);
}



void EditorSceneView::wheelEvent(QWheelEvent *event)
{
  QPointF center = mapToScene(viewport()->rect().center());
  QPointF pos = mapToScene(event->pos());
  QPointF d =  pos - center;
  if( event->delta() > 0)
  {
      scale(1.05, 1.05);
      centerOn(pos - d/2);

  }
  else
  {
      scale(0.95, 0.95);
      centerOn(pos - d/2);
  }

  QGraphicsView::wheelEvent(event);
}

void EditorSceneView::mouseReleaseEvent(QMouseEvent *event)
{

    if (event->button() == Qt::MiddleButton)
    {
       middlePressed = false;

    }
    QGraphicsView::mouseReleaseEvent(event);

}
void EditorSceneView::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::MiddleButton)
    {
        middlePressed = true;
        wheelPresPos = event->pos();
    }

    QGraphicsView::mousePressEvent(event);
}


void EditorSceneView::mouseMoveEvent(QMouseEvent *event)
{
    if (middlePressed)
    {
        float dx = event->pos().x() - wheelPresPos.x();
        float dy = event->pos().y() - wheelPresPos.y();
        QPointF center = mapToScene(viewport()->rect().center());
        centerOn(center.x() - dx, center.y() - dy);
    }
    wheelPresPos = event->pos();
    QGraphicsView::mouseMoveEvent(event);
}

void EditorSceneView::paintEvent(QPaintEvent *event)
{
    QGraphicsView::paintEvent(event);
    QPainter p(viewport());
    p.setPen(Qt::red);
    int h = mapFromScene(0,0).y();
    p.drawLine(0, h, width(), h);


    int w = mapFromScene(0,0).x();
    p.drawLine(w, 0, w, height());

   /* //marking items positions with red point
    p.setPen(QPen(Qt::red, 10));
    for (auto& item : scene()->items())
    {
        p.drawPoint(mapFromScene(item->pos()));
    }
    */
}



