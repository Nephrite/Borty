#include "itemselectwidget.h"
#include "ui_itemselectwidget.h"
#include "editor/itembutton.h"
#include "common/objecttypes.h"

#include <QFile>
#include <QFileDialog>

#include <QJsonDocument>
#include <QJsonArray>

#include <QScrollArea>

#include <QDebug>
#include <QDir>

ItemSelectWidget::ItemSelectWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ItemSelectWidget)
{
    ui->setupUi(this);
    QDir texturesDir("./resources/textures");
    ui->selectStyleBox->addItems(texturesDir.entryList(QDir::Dirs| QDir::NoDotAndDotDot));

    setupButtons("./resources/objects.json");
    setupButtons("./resources/autoimportobjects.json");

    newProperties = new QJsonObject;
    ui->treeWidget->setHeaderHidden(true);

}

ItemSelectWidget::~ItemSelectWidget()
{
    delete ui;
}


void ItemSelectWidget::selectedObject(QJsonObject* prop)
{
    newProperties = prop;
    emit sendNewProperties(prop);
}

void ItemSelectWidget::setupButtons(QString file)
{
    QFile jsonFile(file);
    if ( ! jsonFile.open(QFile::ReadOnly))
    {
        qDebug()<<"no file " << file;
        return;
    }
    QJsonDocument document = QJsonDocument::fromJson(jsonFile.readAll());
    jsonFile.close();
    QJsonArray objects = document.object().value("objects").toArray();
    QString metaType;

    QMetaEnum typeEnum = QMetaEnum::fromType<type::types>();

    for (QJsonValue value : objects)
    {
        int t = value.toObject().value("objectType").toInt();
        QString objectType = typeEnum.valueToKey(t);
        QJsonObject obj = value.toObject();
        ItemButton* button = new ItemButton(obj);
        allItemButtons.append(button);
        QString icon("./resources/textures/"
                     + obj.value("textureStyle").toString()
                     + "/"
                     + obj.value("texture").toString());
        button->setIconFile(m_thumbnailer.thumb(icon));
        QObject::connect(button, SIGNAL(sendProperies(QJsonObject*)),
                           this, SLOT (selectedObject(QJsonObject*)));
        if (objectListTabs.contains(objectType))
        {          
            QTreeWidgetItem* root = objectListTabs.value(objectType);

            addTreeItem(root, button);
        }
        else
        {
           QTreeWidgetItem* root = new QTreeWidgetItem(ui->treeWidget);
           root->setChildIndicatorPolicy(QTreeWidgetItem::ShowIndicator);
           ui->treeWidget->addTopLevelItem(root);
           root->setText(0, objectType);
           objectListTabs.insert(objectType, root);

           addTreeItem(root, button);

        }
    }
}

void ItemSelectWidget::addTreeItem(QTreeWidgetItem *parent, QWidget *widget)
{
    QTreeWidgetItem* item = new QTreeWidgetItem(parent);  
    item->setText(0, widget->objectName());
    parent->addChild(item);
    ui->treeWidget->setItemWidget(item, 0, widget);
}



void ItemSelectWidget::on_displaySelectedStyleButton_clicked()
{
    if (ui->selectStyleBox->currentIndex() == 0)
    {
        for (ItemButton* button : allItemButtons)
            button->setVisible(true);
    }
    else
    {
        QString style = ui->selectStyleBox->currentText();
        for (ItemButton* button : allItemButtons)
        {
            if (button->objectStyle() == style)
            {
               // ui->treeWidget->setItemHidden();
                button->setVisible(true);
            }
            else
            {
                button->setVisible(false);
            }
        }
    }

}
