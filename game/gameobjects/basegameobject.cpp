#include "basegameobject.h"


#include "common/qpropertyaccessmacro.h"
#include "common/objecttypes.h"
#include "common/config.h"

#include "game/glesgameview/glesdrawable.h"
#include "game/glesgameview/glutils.h"

#include "Box2D/Box2D.h"


#include <QObject>
#include <QMetaObject>
#include <QMetaProperty>
#include <QJsonObject>
#include <QPointF>
#include <QRectF>
#include <QVector>
#include <QDebug>

#include <math.h>

QStringList BaseGameObject::scalableProperties
                            = QStringList()
                            << "width"
                            << "height"
                            << "physicsWidth"
                            << "physicsHeight"
                            << "x"
                            << "y"
                            << "xDistance"
                            << "yDistance"
                            << "yMargin";

bool BaseGameObject::isScalable(QString p)
{
    return BaseGameObject::scalableProperties.contains(p);
}

void BaseGameObject::setupSprite()
{
    m_sprite.setTililingType(m_horizontalMapping, m_verticalMapping);
    m_sprite.setAsBox(m_width, m_height);
    m_sprite.addTexture("./resources/textures/"+m_textureStyle+"/"+m_texture, QOpenGLTexture::Nearest);
    m_sprite.setPos(b2Vec2(m_x, m_y));
    m_sprite.setAngle(m_rotation);
    m_sprite.setZvalue(m_zValue);
}

BaseGameObject::BaseGameObject():
    m_width(10),
    m_height(10),
    m_x(0),
    m_y(0),
    m_zValue(0),
    m_yMargin(0),   
    m_texture("transperent.png"),
    m_textureStyle("forest"),    
    m_metaType("object"),
    m_objectType(type::PhysicsObject),
    m_rotation(0),
    m_physicsType(b2_staticBody),
    m_blendMode(0),
    m_verticalMapping(Drawable::StretchedMapping),
    m_horizontalMapping(Drawable::StretchedMapping)

{

}

BaseGameObject::BaseGameObject(QJsonObject& map):
    m_width(10),
    m_height(10),
    m_x(0),
    m_y(0),
    m_zValue(0),
    m_yMargin(0),   
    m_texture("transperent.png"),
    m_textureStyle("forest"),
    m_metaType("object"),
    m_objectType(type::PhysicsObject),
    m_rotation(0),
    m_physicsType(b2_staticBody),
    m_blendMode(0),
    m_verticalMapping(Drawable::StretchedMapping),
    m_horizontalMapping(Drawable::StretchedMapping),
    m_visible(true),
    m_active(false)
{

    setup(map);
    m_size = QSizeF(m_width, m_height);
    setupSprite();
}

BaseGameObject::~BaseGameObject()
{
    qDebug()<< "delete" << m_objectType;

}

void BaseGameObject::setup(QJsonObject &object)
{
    setupProperties(object);
    setupMembers(object);
}


void BaseGameObject::glDraw(GlUtils* glutils)
{
    if (!visible()) return;
    m_sprite.draw(glutils);
}

void BaseGameObject::setGameData(GameData *gameData)
{
    Q_UNUSED(gameData)
}

void BaseGameObject::onScreenEnter()
{
    m_isOnScreen = true;
    m_active = true;
}

void BaseGameObject::onScreenLeave()
{
    m_isOnScreen = false;
    m_active = false;
}

bool BaseGameObject::isOnScreen()
{
    return m_isOnScreen;
}

QRectF BaseGameObject::boundingRect()
{
    return QRectF(m_x - m_width / 2.0 ,
                  m_y - m_height / 2.0,
                  m_width,
                  m_height
                  );
}

QSizeF BaseGameObject::size()
{
    return m_size;
}

double BaseGameObject::bottomY()
{
    return m_sprite.bottomY()+m_yMargin;
}

bool BaseGameObject::isAboveOf(BaseGameObject *other)
{
    return bottomY() > other->bottomY();
}

bool BaseGameObject::equalZero(float x)
{
    return std::fabs(x) < 0.001;
}

bool BaseGameObject::equal(b2Vec2 that, b2Vec2 other)
{
    return equalZero(std::fabs(that.x - other.x)) && equalZero(std::fabs(that.y - other.y));
}

void BaseGameObject::advance(int timeDelta)
{
    m_sprite.shaderArgs()->time+=timeDelta;
}


void BaseGameObject::setupProperties(QJsonObject &object)
{
    //qDebug()<<"setup";

    QVariantMap props;
    if (object.contains("properties"))
    props = object.value("properties").toObject().toVariantMap();

    else props = object.toVariantMap();

    double scale = Config::instance().scale();

    QString key;
    QVariant value;
    for (int i = 0;
             i < metaObject()->propertyCount();
             i++
         )
    {
        key = QString(metaObject()->property(i).name());
        if (props.contains(key))
        {
            value = props.value(key);
            if (isScalable(key))
            {
                if (  QString(value.typeName()) == "double"
                   || QString(value.typeName()) == "float"
                   )
                {
                    double v = value.toDouble()/ scale;
                    setProperty(key, QVariant(v));
                }
                else if (QString(value.typeName()) == "int")
                {
                    int v = value.toInt() / scale;
                    setProperty(key, QVariant(v));
                    //metaObject()->property(i).write(this, QVariant(v));
                }
            }
            else
            {
                setProperty(key, value);
                //metaObject()->property(i).write(this, value);
            }
        }
    }
}


