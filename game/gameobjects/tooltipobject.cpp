#include "tooltipobject.h"

#include <QImage>
#include <QPainter>
#include <game/gamedata.h>
#include <QFontDatabase>
#include "common/objecttypes.h"

TooltipObject::TooltipObject() :
    BaseGameObject(),
    m_text("text"),
    m_lifetime(3000),
    m_attachToCenter(true),
    m_textColor(Qt::black),
    m_timer(0),
    m_fadeValue(0),
    m_fadeState(unfade)
{
    selectPaneForTextLength(m_text.length());
    setupSprite();
}
TooltipObject::TooltipObject(QString text) :
    BaseGameObject(),
    m_text(text),
    m_lifetime(3000),
    m_attachToCenter(true),
    m_textColor(Qt::white),
    m_timer(0),
    m_fadeValue(0),
    m_fadeState(unfade)
{
    m_objectType = type::ToolTipObject;
    m_metaType = "object";
    selectPaneForTextLength(m_text.length());
    m_height = 256/64;
    m_width = 320/64;
    m_active = true;
    m_isOnScreen = m_attachToCenter;
    m_x = 0;
    m_y = 0;
    m_sprite.setTililingType(m_horizontalMapping, m_verticalMapping);  
    m_sprite.setPos(b2Vec2(m_x, m_y));
    m_sprite.setAngle(m_rotation);
    m_sprite.setZvalue(m_zValue);
    setupSprite();    
}

TooltipObject::TooltipObject(QString text, b2Vec2 pos) :
    BaseGameObject(),
    m_text(text),
    m_lifetime(3000),
    m_attachToCenter(false),
    m_textColor(Qt::white),
    m_timer(0),
    m_fadeValue(0),
    m_fadeState(unfade)
{
    m_objectType = type::ToolTipObject;
    m_metaType = "object";
    selectPaneForTextLength(m_text.length());
    m_textureStyle = "ui";
    m_height = 256/64;
    m_width = 320/64;
    m_active = true;
    m_isOnScreen = m_attachToCenter;
    m_x = pos.x;
    m_y = pos.y;
    m_sprite.setTililingType(m_horizontalMapping, m_verticalMapping);   
    m_sprite.setPos(b2Vec2(m_x, m_y));
    m_sprite.setAngle(m_rotation);
    m_sprite.setZvalue(m_zValue);
    setupSprite();
}

TooltipObject::TooltipObject(QJsonObject &parameters)
    : BaseGameObject(parameters),
      m_text("no text"),
      m_lifetime(3000),
      m_attachToCenter(true),
      m_textColor(Qt::black),
      m_timer(0),
      m_fadeValue(0),
      m_fadeState(unfade)
{    
    setup(parameters);
    m_visible = true;
    m_objectType = type::ToolTipObject;
    setupSprite();
   // m_active = true;
   // m_isOnScreen = false;
}

TooltipObject::~TooltipObject()
{

}

void TooltipObject::advance(int timedelta)
{
    if (!m_active) return;
    m_timer += timedelta;
    if (m_fadeState == wait)
    {
        if (m_timer < m_delay) return;
        else{
            m_timer = 0;
            m_fadeState = unfade;
        }
    }

    switch (m_fadeState) {
    case fade:
        if (m_fadeValue > 0.0) m_fadeValue-=0.05;
        else
        {
            m_fadeValue = 0.0;
            m_fadeState = nofade;
            emit deleteMe(this);
        }
        break;
    case unfade:
        if (m_fadeValue < 1.0) m_fadeValue+=0.05;
        else
        {
            m_timer = 0;
            m_fadeValue = 1.0;
            m_fadeState = nofade;
        }
    case nofade:
        if (m_lifetime > 0 && m_timer > m_lifetime )
        {
            m_fadeState = fade;
        }
        break;
    default:
        break;
    }

}

void TooltipObject::glDraw(GlUtils *glutils)
{   
    if (m_attachToCenter)
    {
        m_sprite.setPos(m_gameData->viewport().center());
    }
    m_sprite.setOpacity(m_fadeValue);
    m_sprite.draw(glutils);
}

void TooltipObject::onScreenLeave()
{
    if(m_timer > 100)
        emit deleteMe(this);
}

void TooltipObject::setPosition(b2Vec2 pos)
{
    m_sprite.setPos(pos);
}

double TooltipObject::bottomY()
{
    return 999.9;
}

void TooltipObject::setGameData(GameData *data)
{
    assert(data != nullptr);
    m_gameData = data;
    if (m_attachToCenter)m_sprite.setPos(m_gameData->viewport().center());
    m_isOnScreen = m_gameData->viewport().intersects(boundingRect());
}

void TooltipObject::startFading()
{
     m_fadeState = fade;
}

void TooltipObject::startUnfading()
{
       m_fadeState = unfade;
}

void TooltipObject::setupSprite()
{
    QPixmap pane("./resources/textures/"+m_textureStyle+'/'+m_texture);

    QTextOption format;
    format.setAlignment(Qt::AlignCenter);
    format.setWrapMode(QTextOption::WrapAtWordBoundaryOrAnywhere);

    QPainter pa(&pane);
    pa.setPen(m_textColor);

    QFont font(QFontDatabase::applicationFontFamilies(QFontDatabase::GeneralFont).at(0));

    font.setPixelSize(32);
    font.setLetterSpacing(QFont::AbsoluteSpacing, 1.5);
    pa.setFont(font);

    pa.drawText(QRectF(24,
                       24,
                       pane.width()-48,
                       pane.height()-48),
                m_text,
                format
                );
    pa.end();
    m_sprite.setShaderName("default");
    m_sprite.addTexture(pane);
    m_sprite.setAsBox(pane.width()/64.0,
                      pane.height()/64.0);
}

void TooltipObject::selectPaneForTextLength(int length)
{
    if (length < 15)
        m_texture = "black_pane_3.png";
    else if (length < 20)
        m_texture = "black_pane_2.png";
    else if (length < 30)
        m_texture = "black_pane_1.png";
    else
        m_texture = "black_pane.png";

    m_textureStyle = "ui";
}
