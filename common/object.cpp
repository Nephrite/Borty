#include <QJsonObject>
#include <QJsonArray>
#include <QMetaProperty>
#include <QMetaObject>
#include <QDebug>

#include "object.h"

Object::Object(QObject *parent) : QObject(parent)
{

}

Object::Object(const Object &other)
{   
    auto value = Object::toJsonObject(&other);
    setup(value);
}

Object *Object::create(QJsonObject &object)
{
    QString type = object.value("type").toString();
    if (type.isEmpty())
        type = object.value("objectType").toString();
    Object* obj = (Object*)QMetaType::construct(QMetaType::type(type.toLatin1()), 0, 0);
    if (obj != nullptr)
    {
        obj->setup(object);
    }
    return obj;
}

Object* Object::toObject(QObject *object)
{
    return dynamic_cast<Object*>(object);
}

const QMetaObject* Object::metaObjectOf(QString &type)
{
    return QMetaType::metaObjectForType(QMetaType::type(type.toLatin1()));
}

const QMetaObject* Object::metaObjectOf(const char* type)
{
    return QMetaType::metaObjectForType(QMetaType::type(type));
}

void Object::setupProperties(QJsonObject &object)
{
    QJsonObject props;
    if (object.contains("properties"))
    props = object.value("properties").toObject();
    else props = object;

    QString&& type = props.value("qtype").toString();
    const QMetaObject* metaobj = metaObjectOf(type);

    if (metaobj == nullptr)
        metaobj = metaObject();

    QString key;
    for (int i = 0;
             i < metaobj->propertyCount();
             i++
         )
    {
        key = QString(metaobj->property(i).name());
        if (props.contains(key) && metaobj->property(i).isDesignable())
        {
            if (metaobj->property(i).isEnumType())
            {
                setProperty(key, props.value(key).toInt());
            }

            else{
                setProperty(key, props.value(key));
            }
        }
    }
}

QString Object::toString()
{
    return QString("%1:%2").arg(metaObject()->className()).arg(objectName());
}

QVariantMap Object::toVariantMap()
{
    return Object::toVariantMap(this);
}

QVariantMap Object::toVariantMap(const Object *object)
{
    QVariantMap map;
    for(int i = 0; i<object->metaObject()->propertyCount(); i++)
    {
        if(   object->metaObject()->property(i).isStored(object)
           && object->metaObject()->property(i).isUser()
          )
        {
            map.insert( object->metaObject()->property(i).name()
                      , object->metaObject()->property(i).read(object));
        }
    }
    return map;
}

QJsonObject Object::toJsonObject()
{
    return Object::toJsonObject(this);
}


QJsonObject Object::toJsonObject(const Object* object)
{
    QJsonObject obj;

    QJsonObject props;
    const QMetaObject* metaobj = object->metaObject();
    for(int i = 0; i < metaobj->propertyCount(); i++)
    {
        if(   metaobj->property(i).isStored(object)
           && metaobj->property(i).isUser()
          )
        {
            props.insert( metaobj->property(i).name()
                      , metaobj->property(i).read(object).toJsonValue());
        }
    }
    obj.insert("properties",props);

    auto childs = object->children();
    if (childs.count() > 0)
    {
        QJsonArray ch;
        foreach (auto& child, childs)
        {
            Object* o = Object::toObject(child);
            if (o != nullptr)
            {
                ch.append(o->toJsonObject());
            }
        }

        obj.insert("childs", ch);
    }
    return obj;
}

void Object::setupMembers(QJsonObject &object)
{
    QJsonArray childs = object.value("childs").toArray();

    for (auto child : childs)
    {
        QJsonObject childData = child.toObject();
        setupChildMember(childData);
    }
}


void Object::setupChildMember(QJsonObject& object)
{       
    QString type = object.value("type").toString();
    QString name = object.value("name").toString();

    const QMetaObject* metaobj = metaObjectOf(type);


    if (metaobj->indexOfProperty(name.toLatin1()) != -1)
    {
        foreach (auto child, children())
        {
            if (child->objectName() == name
                    && child->metaObject()->className() == type) // found child QObject of with that name
            {
                Object* childObj = Object::toObject(child);
                if (childObj != nullptr)
                {
                    childObj->setup(object);
                    return;
                }
            }
        }
    }
}


void Object::setProperty(const char* name, const QVariant &value)
{
   // qDebug()<< "set " << name << value.toString();
    QObject::setProperty(name, value);
}

void Object::setProperty(const QString name, const QVariant &value)
{
   // qDebug()<< "set " << name << value.toString();
    QObject::setProperty(name.toLatin1(), value);
}
void Object::setup(QJsonObject& obj)
{
    setupProperties(obj);

    setupMembers(obj);
}

