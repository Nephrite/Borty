#ifndef NEWLEVELDIALOG_H
#define NEWLEVELDIALOG_H

#include <QDialog>
#include "editor/itembutton.h"
namespace Ui {
class AutofillDialog;
}

class AutofillDialog : public QDialog
{
    Q_OBJECT    
public:
    explicit AutofillDialog(QWidget *parent = 0);
    ~AutofillDialog();
    void setJson(QJsonObject *json);
    int startX();
    int startY();
    int horizontalStep();
    int verticalStep();
    int rowCount();
    int columnCount();
    int blockCount();
    QString blockJson();
public slots:
    void accepted();

private slots:
    void on_rowsSpin_valueChanged(int arg1);

    void on_colunmsSpin_valueChanged(int arg1);

private:
    Ui::AutofillDialog *ui;

};

#endif // NEWLEVELDIALOG_H
