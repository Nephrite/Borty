#include "endlevelblock.h"
#include "common/objecttypes.h"
#include "game/gameobjects/player.h"

EndLevelBlock::EndLevelBlock()
{

}

EndLevelBlock::EndLevelBlock(QJsonObject& parameters,
                             b2World* world) : PhysicsObject(parameters, world)
{   
    m_physicsType = b2_staticBody;
    m_objectType = type::EndLevelBlock;
}

bool EndLevelBlock::isAboveOf(BaseGameObject *other)
{
    if (other->as<Player*>())
        return false;
    else
        return BaseGameObject::isAboveOf(other);

}

EndLevelBlock::~EndLevelBlock()
{

}

