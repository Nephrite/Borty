#ifndef ENDLEVELBLOCK_H
#define ENDLEVELBLOCK_H

#include <QObject>
#include "editor/items/baseitem.h"

class EndLevelBlockItem : public BaseItem
{
    Q_OBJECT
    Q_PROPERTY(QString nextLevelFile READ nextLevelFile WRITE setNextLevelFile DESIGNABLE true USER true)
    QString m_nextLevelFile;

public:

    explicit EndLevelBlockItem();
    explicit EndLevelBlockItem(QVariantMap *newProperties);
    ~EndLevelBlockItem();
    QString nextLevelFile() const
    {
        return m_nextLevelFile;
    }
    public slots:
    void setNextLevelFile(QString nextLevelFile)
    {
        m_nextLevelFile = nextLevelFile;
    }
};

#endif // ENDLEVELBLOCK_H
