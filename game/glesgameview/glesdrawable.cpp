#include "glesdrawable.h"

#include <QMatrix4x4>
#include <QDebug>
#include <QMetaType>
#include <QMetaObject>



Drawable::Drawable() :   
    m_opacity(1.0),
    m_shaderName("default"),
    m_bindAllTextures(false),
    m_angle(0),
    m_mapping(Normal),
    m_size(b2Vec2(0,0)),
    m_pos(b2Vec2(0,0)),
    m_tiling(QPointF(1.0,1.0)),
    m_flipping(QPointF(1.0,1.0)),
    m_offset(b2Vec2(0,0)),
    m_frameCount(1),
    m_columnCount(1),
    m_rowCount(1),
    m_currentFrame(0),
    m_currentRow(0),
    m_currentColumn(0),    
    m_zValue(0)
{

}
/*
Drawable::Drawable(QObject *parent):
    Object(parent),
    m_mapping(Normal),
    m_size(b2Vec2(0,0)),
    m_pos(b2Vec2(0,0)),
    m_shaderName("default"),
    m_angle(0),
    m_tiling(QPointF(1.0,1.0)),
    m_flipping(QPointF(1.0,1.0)),
    m_offset(b2Vec2(0,0)),
    m_frameCount(1),
    m_columnCount(1),
    m_rowCount(1),
    m_currentFrame(0),
    m_currentRow(0),
    m_currentColumn(0),
    m_opacity(1.0),
    m_zValue(0)
{

}
*/
Drawable::~Drawable()
{
    for (auto& tex : m_textures)
    {
        tex->release();
        tex->destroy();
        delete tex;
    }
    VertexArrayBuf.release();
    VertexArrayBuf.destroy();
    indexBuf.release();
    indexBuf.destroy();
}

void Drawable::updateTiling()
{
    if (m_textures.size() > 0)
    {
        if (m_horizontalMapping == TiledMapping )
        {
            m_tiling.setX(float((m_textures[0]->width()) / 64.0 * m_size.x  /2));
        }
        if (m_verticalMapping == TiledMapping )
        {
            m_tiling.setY(float((m_textures[0]->height()) /64.0 * m_size.y  /2));
        }
    }
    else
    {
        m_tiling.setX(1.0);
        m_tiling.setY(1.0);
    }
}

void Drawable::setAsBox(b2Vec2 size)
{  
    m_size = size;
    initGeometry();
    updateTiling();
}
void Drawable::setAsBox(double w, double h)
{  
    m_size = b2Vec2(w, h);   
    initGeometry();
    updateTiling();
}

void Drawable::selectTexture(int id)
{
    if (id < m_textures.size())
    {
        m_currentTexture = id;
    }    
    else
    {
        qDebug() << "There is no texture with id" << id;
    }
}

void Drawable::setOffset(double x, double y)
{
    m_offset.x = x;
    m_offset.y = y;
}


double Drawable::bottomY()
{
    return m_pos.y + m_size.y /2;
}
int Drawable::addTexture(QString file, QOpenGLTexture::Filter filter)
{    
    QOpenGLTexture* tex = new QOpenGLTexture(QImage(file));

    if (! tex->isCreated())
    {
        qDebug() << "Texture not loaded" << file;
        delete tex;
        tex = new QOpenGLTexture(QImage("./resources/textures/transperent.png"));
    }
    tex->setMinificationFilter(filter);
    tex->setMagnificationFilter(filter);
    tex->setWrapMode(QOpenGLTexture::Repeat);

    m_textures.append(tex);
    m_currentTexture = m_textures.size()-1;
    if (m_currentTexture == 0 ) updateTiling();
    return m_currentTexture;
}

int Drawable::addTexture(QString file)
{
    return addTexture(file, QOpenGLTexture::Nearest);
}

int Drawable::addTexture(QPixmap &image)
{
    QOpenGLTexture* tex = new QOpenGLTexture(image.toImage());

    if (! tex->isCreated())
    {
        qDebug() << "Texture not loaded";
        delete tex;
        tex = new QOpenGLTexture(QImage("./resources/textures/transperent.png"));
    }

    tex->setMinificationFilter(QOpenGLTexture::Nearest);
    tex->setMagnificationFilter(QOpenGLTexture::Nearest);
    tex->setWrapMode(QOpenGLTexture::ClampToBorder);
    m_textures.append(tex);
    m_currentTexture = m_textures.size()-1;
    if (m_currentTexture == 0 ) updateTiling();
    return m_currentTexture;
}

void Drawable::setTexMappingType(Mapping m)
{
    m_mapping = m;
}

void Drawable::setTililingType(Drawable::TextureMappingType tx, Drawable::TextureMappingType ty)
{
    m_horizontalMapping = tx;
    m_verticalMapping = ty;
    updateTiling();
}

void Drawable::setTililingType(int tx, int ty)
{
    m_horizontalMapping = (Drawable::TextureMappingType)tx;
    m_verticalMapping = (Drawable::TextureMappingType)ty;
    updateTiling();
}

void Drawable::setVerticalFlipped(bool flipped)
{
    if (flipped) m_flipping.setY(-1);
    else m_flipping.setY(1);
}
void Drawable::setHorizontalFlipped(bool flipped)
{
    if (flipped) m_flipping.setX(-1);
    else m_flipping.setX(1);
}

void Drawable::setFrameCount(int c)
{
    m_frameCount = c;
    updateFrameCoord();
}

void Drawable::setRowCount(int c)
{
    m_rowCount = c;
    updateFrameCoord();
}

void Drawable::setColumnCount(int c)
{
    m_columnCount = c;
    updateFrameCoord();
}

void Drawable::setCurrentFrame(int f)
{
    m_currentFrame = (f > m_frameCount) ? f%m_frameCount : f;
    updateFrameCoord();
}

void Drawable::setNextFrame(Drawable::FrameOrder order)
{
    switch (order) {
    case Direct:
        m_reverseFrameOrder = false;
        break;
    case Reverse:
        m_reverseFrameOrder = true;
        break;
    case Loop:
        if (m_currentFrame == m_frameCount-1)
            m_reverseFrameOrder = true;
        else if (m_currentFrame == 0)
            m_reverseFrameOrder = false;
        break;
    default:
        break;
    }

    if (m_reverseFrameOrder)
    {
        m_currentFrame = (m_currentFrame-1) % m_frameCount;
    }
    else
    {
        m_currentFrame = (m_currentFrame+1) % m_frameCount;
    }
    updateFrameCoord();
}

void Drawable::updateFrameCoord()
{
    if (m_currentFrame == 0){
        m_currentRow = 0;
        m_currentColumn = 0;
    } else
    {
        m_currentColumn = m_currentFrame%m_columnCount ;
        m_currentRow    = m_currentFrame/m_columnCount;

    }
}

void Drawable::turn(double angle)
{
    m_angle += angle;
}


void Drawable::setFlipping(double x, double y)
{
    m_flipping = QPointF(x,y);
}

void Drawable::initGeometry()
{
    if (m_mapping != VerticalFlipped)
    {
        m_vertices[0] =
                VertexData({QVector3D( m_size.x/-2.0, m_size.y /2.0 , 0.0f), QVector2D(0.0f, 1.0f)});
        m_vertices[1] =
                VertexData({QVector3D( m_size.x/-2.0, m_size.y/-2.0 , 0.0f), QVector2D(0.0f, 0.0f)});
        m_vertices[2] =
                VertexData({QVector3D( m_size.x/2.0 , m_size.y/-2.0 , 0.0f), QVector2D(1.0f, 0.0f)});
        m_vertices[3] =
                VertexData({QVector3D( m_size.x/2.0 , m_size.y /2.0 , 0.0f), QVector2D(1.0f, 1.0f)});
    }
    else
    {
        m_vertices[0] =
                VertexData({QVector3D( m_size.x/-2.0, m_size.y /2.0 , 0.0f), QVector2D(0.0f, 0.0f)});
        m_vertices[1] =
                VertexData({QVector3D( m_size.x/-2.0, m_size.y/-2.0 , 0.0f), QVector2D(0.0f, 1.0f)});
        m_vertices[2] =
                VertexData({QVector3D( m_size.x/2.0 , m_size.y/-2.0 , 0.0f), QVector2D(1.0f, 1.0f)});
        m_vertices[3] =
                VertexData({QVector3D( m_size.x/2.0 , m_size.y /2.0 , 0.0f), QVector2D(1.0f, 0.0f)});
    }


    /*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*\
     * 0┌─────────────────> W                *
     *  │                                    *
     *  │    1┌────┐2                        *
     *  │     │   ╱│   Triangles:            *
     *  v     │  ╱ │                         *
     *  H     │ ╱  │  0 > 2 > 1 > 0 > 3 > 2  *
     *        │╱   │     ccw    front-face   *
     *       0└────┘3                        *
    \*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

    if (VertexArrayBuf.isCreated())
    {
        VertexArrayBuf.release();
        VertexArrayBuf.destroy();
    }

    VertexArrayBuf.create();
    VertexArrayBuf.bind();
    VertexArrayBuf.allocate(m_vertices, 4 * sizeof(VertexData));


    m_elCount = 6;

    indices[0]= 0;
    indices[1]= 2;
    indices[2]= 1;
    indices[3]= 0;
    indices[4]= 3;
    indices[5]= 2;

    if (indexBuf.isCreated())
    {
        indexBuf.release();
        indexBuf.destroy();
    }

    indexBuf = QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);
    indexBuf.create();
    indexBuf.bind();
    indexBuf.allocate(indices, m_elCount * sizeof(GLushort));    
}

void Drawable::draw(GlUtils* glUtils)
{
    glUtils->lookAt(m_pos+m_offset, m_zValue, m_angle);
    bind(glUtils);

    auto shader = glUtils->shader();
    shader->setUniformValue("tiling", m_tiling);
    shader->setUniformValue("flipping", m_flipping);
    shader->setUniformValue("opacity", m_opacity);

    shader->setUniformValue("time", GLfloat(m_shaderArguments.time/1000.0));
    shader->setUniformValue("arg1", m_shaderArguments.arg1);
    shader->setUniformValue("arg2", m_shaderArguments.arg2);
    shader->setUniformValue("arg3", m_shaderArguments.arg3);


    if (m_shaderName == "animatedSprite")
    {

        if (m_flipping.x() > 0)
        {
            if (m_flipping.y() > 0)  // 1 1 ok
            {
                shader->setUniformValue("column", m_currentColumn);
                shader->setUniformValue("frameW", GLfloat(1.0/(m_columnCount)));
                shader->setUniformValue("row",    m_currentRow);
                shader->setUniformValue("frameH", GLfloat(1.0/(m_rowCount)));
            }
            else // 1 -1 ?
            {
                shader->setUniformValue("column", m_columnCount+m_currentColumn);
                shader->setUniformValue("frameW", GLfloat(1.0/(m_columnCount)));
                shader->setUniformValue("row",   -m_rowCount+m_currentRow+1);
                shader->setUniformValue("frameH", GLfloat(1.0/(m_rowCount)));
            }
        }
        else
        {
            if (m_flipping.y() > 0) // -1 1 ok
            {
                shader->setUniformValue("column", -m_currentColumn-1);
                shader->setUniformValue("frameW", GLfloat(1.0/(m_columnCount)));
                shader->setUniformValue("row",   -m_rowCount+m_currentRow);
                shader->setUniformValue("frameH", GLfloat(1.0/(m_rowCount)));
            }
            else // -1 -1 ?
            {

                shader->setUniformValue("column", -m_columnCount-1);
                shader->setUniformValue("frameW", GLfloat(1.0/(m_columnCount)));
                shader->setUniformValue("row",   -m_rowCount+m_currentRow+1);
                shader->setUniformValue("frameH", GLfloat(1.0/(m_rowCount)));

            }
        }
    }

    glUtils->glDrawElements(GL_TRIANGLES, m_elCount, GL_UNSIGNED_SHORT, 0);
    glUtils->releaseShader();
    release();

}

void Drawable::drawBinded(GlUtils *glutils)
{

       // glUtils->setShader(shader);
    glutils->glDrawElements(GL_TRIANGLES, m_elCount, GL_UNSIGNED_SHORT, 0);
   // glUtils->releaseShader();
}


void Drawable::bind(GlUtils *glutils)
{  
    VertexArrayBuf.bind();
    indexBuf.bind();
    glutils->setShader(m_shaderName);

    if (m_bindAllTextures)
    {
        for (int i = 0; i < m_textures.count(); ++i)
        {          
            QString name = QString("texture%1").arg(i);          
            glutils->addTextureToShader(name.toLatin1(), m_textures[i], GL_TEXTURE0+i );
        }
    }
    else
    {
        m_textures[m_currentTexture]->bind();
    }


}

void Drawable::bind()
{
    m_textures[m_currentTexture]->bind();
    VertexArrayBuf.bind();
    indexBuf.bind();
}
void Drawable::lookHere(GlUtils* glutils)
{
    glutils->lookAt(m_pos, 0, m_angle);
}
void Drawable::release()
{
   m_textures[m_currentTexture]->release();
   VertexArrayBuf.release();
   indexBuf.release();
}
