#ifndef FROG_H
#define FROG_H
#include <game/gameobjects/basegameobject.h>
#include <game/gameobjects/physicsobject.h>

class frog : public PhysicsObject
{
public:  
    frog (QJsonObject &parameters, b2World*);
    frog();
    ~frog();
    void advance(int timeDelta);
    void solveCollision(PhysicsObject *o);
protected:

    b2Vec2 impulse;
    float m_jumpForce;
    float time;
    float lastY;
};


#endif // FROG_H
