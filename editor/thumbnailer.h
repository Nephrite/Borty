#ifndef THUMBNAILER_H
#define THUMBNAILER_H

#include <QString>
#include <QFileInfo>

class Thumbnailer
{
public:
    Thumbnailer();
    QString thumb(QString fileName);
    void forceRegenerate();
private:
    void checkFolder();
    bool fileExists(QString fileName);
    QFileInfo m_fileInfo;
    void createThumb(QString sourceFile, QString thumbName);
};

#endif // THUMBNAILER_H
