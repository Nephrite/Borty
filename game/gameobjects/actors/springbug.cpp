#include "springbug.h"
#include "common/objecttypes.h"
#include "../player.h"


#include <QDebug>

SpringBug::SpringBug()
{

}

SpringBug::SpringBug(QJsonObject &parameters, b2World * world): PhysicsObject(parameters, world)
{
    m_physicsType = b2_dynamicBody;
    body->SetType(b2_dynamicBody);
    impulse = b2Vec2(1.0, 0);
    maxDistance = 2.0;
    basePoint = body->GetPosition();
    body->SetLinearVelocity(impulse);
    collisionList.append(type::Player);
    m_sprite.setOffset(0, 0.05);
}

void SpringBug::advance(int timeDelta)
{
    Q_UNUSED(timeDelta);
    if (fabs(body->GetPosition().x - basePoint.x) > maxDistance //if walk too far
     || equalZero(body->GetLinearVelocity().x))                 // or stacked
    {
        impulse.x = -(impulse.x);                               //change direction
        m_sprite.setHorizontalFlipped(impulse.x < 0);
    }
    body->SetLinearVelocity(impulse);         
}

void SpringBug::solveCollision(PhysicsObject *o)
{
    if (o->objectType() != type::Player) return;
    Player* p = dynamic_cast<Player*>(o);
    if (p)
    {
        if (
           p->body->GetPosition().x < body->GetPosition().x + m_width*0.6
        && p->body->GetPosition().x > body->GetPosition().x - m_width*0.6
           )
        {
            p->applyImpulse(0,-20);
            p->body->SetLinearVelocity(body->GetLinearVelocity()+b2Vec2(0,-20));
        }
    }
}
