#include "npc.h"
#include "common/objecttypes.h"
#include "game/gameobjects/player.h"
#include "game/gameobjects/tooltipobject.h"

NPC::NPC() : Actor(),
    m_activateEvent(PlayerTouch),
    m_behaviour(LinearWalking),
    m_distance(5),
    m_initDirection(Direction::left),
    m_text("what?"),
    m_propertyName(QString("")),
    m_value(0),
    m_singleAction(false),
    m_disabled(false),    
    m_toolTipTime(5000),
    m_tooltip(nullptr)
{   
    m_direction = Direction::directions(m_initDirection);
    collisionList.append(type::Player);
    m_active = m_activateEvent == OnCreate;
    m_basePoint = body->GetPosition();
    m_propertyModifier = nullptr;
    m_maxVelocity = 0.5;
    m_sprite.setShaderName("animatedSprite");

}

NPC::NPC(QJsonObject &properties, b2World*world) : Actor(properties, world),
    m_activateEvent(PlayerTouch),
    m_behaviour(LinearWalking),
    m_distance(5),
    m_initDirection(Direction::left),
    m_text("what?"),
    m_propertyName(QString("")),
    m_value(0),
    m_singleAction(false),
    m_disabled(false),
    m_tooltip(nullptr),
    m_toolTipTime(5000)
{
    setup(properties);
    m_damping = 5;

    m_direction = Direction::directions(m_initDirection);
    collisionList.append(type::Player);
    m_active = m_activateEvent == OnScreenEnter;
    m_basePoint = body->GetPosition();

    m_propertyModifier = nullptr;
    if (!m_propertyName.isEmpty() && m_value != 0)
    {
        m_propertyModifier = new PropertyModifier(m_propertyName, m_value);
    }
    setupSprite();
    m_sprite.setShaderName("animatedSprite");
    setupShadowSprite();
    m_maxVelocity = 0.5;
}

NPC::~NPC()
{

}

void NPC::preSolve(PhysicsObject* obj, b2Contact *contact)
{   
    Q_UNUSED(contact);
    if (obj->objectType() == type::Player && !m_disabled && m_tooltipTimer > 5000)
    {
        if(m_activateEvent == PlayerTouch) m_active = true;
        if (m_singleAction)
        {
            m_disabled = true;
        }
        if (!m_text.isEmpty())
        {
            if (m_tooltip == nullptr)
            {
                m_tooltip = new TooltipObject(m_text, body->GetPosition()-b2Vec2(0, m_height+1));
                m_tooltip->setLifeTime(m_toolTipTime);
                emit addNewObject(m_tooltip);
                m_tooltipTimer = 0;
            }
        }
    }
}

void NPC::glDraw(GlUtils *glutils)
{
    if (!visible()) return;

    m_sprite.setPos(body->GetPosition());

    m_shadowSprite.setPos(body->GetPosition());
    m_shadowSprite.draw(glutils);

    if (Direction::isLeft(m_direction))
    {
        m_sprite.setFlipping(1.0, 1.0);
    }
    else if (Direction::isRight(m_direction))
    {
        m_sprite.setFlipping(-1.0, 1.0);
    }

    m_sprite.draw(glutils);

    if (m_tooltip != nullptr)
        m_tooltip->setPosition(body->GetPosition()-b2Vec2(0, m_height+1));
}

void NPC::advance(int timeDelta)
{
    body->SetLinearDamping(m_damping);
    if (!m_active) return;
    BaseGameObject::advance(timeDelta);
    m_tooltipTimer += timeDelta;

    if (m_tooltipTimer > m_toolTipTime-timeDelta && m_tooltip != nullptr)
    {
        m_tooltip = nullptr;
        m_tooltipTimer = 0;
    }

   // qDebug()<< m_behaviour;
    switch (m_behaviour) {
    case Standing:
        m_direction = Direction::stand;
        break;
    case CircleWalking:
        if ((body->GetPosition() - m_basePoint).Length() > m_distance)
        {
            m_basePoint = body->GetPosition();
            m_direction = Direction::next(m_direction);
        }
        break;
    case LinearWalking:
        if ((body->GetPosition() - m_basePoint).Length() > m_distance*0.9)
        {
            m_basePoint = body->GetPosition();
            m_direction = Direction::invert(m_direction);
        }
        break;
    case FollowPlayer:
        if ((body->GetPosition() - m_basePoint).Length() > m_distance*0.9)
        {
            m_basePoint = body->GetPosition();
            m_direction = Direction::vecToDir(body->GetPosition() - m_gamedata->playerPos());
        }
    default:
        break;
    }

    b2Vec2  m_moveVec = Direction::dirToVector(m_direction);
    m_moveVec *= m_maxVelocity;
    applyImpulse(m_moveVec);

}

void NPC::onScreenEnter()
{
    m_isOnScreen = true;
    if (m_activateEvent == OnScreenEnter && !m_disabled)
    {
        m_active = true;
        if (!m_text.isEmpty())
        {
            if (m_tooltip == nullptr)
            {
                m_tooltip = new TooltipObject(m_text, body->GetPosition()-b2Vec2(0, m_height+1));
                m_tooltip->setLifeTime(m_toolTipTime);
                emit addNewObject(m_tooltip);
                m_tooltipTimer = 0;
            }
        }
    }
}
