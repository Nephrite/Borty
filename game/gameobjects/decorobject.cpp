#include "decorobject.h"

decorObject::decorObject()
{
    m_x = 100;
    m_y = 100;
    m_width = 100;
    m_height = 100;
}

decorObject::decorObject(QJsonObject& parameters) : BaseGameObject(parameters)
{

}

decorObject::~decorObject()
{

}

QPointF decorObject::visiblePos()
{
    return QPointF(m_x, m_y);
}
