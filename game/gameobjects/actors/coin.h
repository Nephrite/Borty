#ifndef COIN_H
#define COIN_H

#include <game/gameobjects/basegameobject.h>
#include <game/gameobjects/physicsobject.h>


class Coin : public PhysicsObject
{
public:
    Coin();
    Coin(QJsonObject& parameters, b2World* world);
    bool collideWith(int type);
};

#endif // COIN_H
