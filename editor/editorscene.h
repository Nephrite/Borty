#ifndef EDITORSCENE_H
#define EDITORSCENE_H

#include "itembutton.h"
#include "editoritemcreator.h"
#include "3rdParty/QPropertyEditor/QPropertyEditorWidget.h"
#include "common/levelproperties.h"
#include <QGraphicsScene>

class BaseItem;
class QGraphicsSceneMouseEvent;
class QKeyEvent;
class QGraphicsScene;

class EditorWindow;

class EditorScene :  public QGraphicsScene
{
    Q_OBJECT

public:
    QByteArray toJson();
    EditorScene(QWidget *parent = 0);
    void saveToFile(QString fileName);
    void saveToFile();
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void keyPressEvent(QKeyEvent *event);

    void newLevel();
    void loadLevelFromFile(QString path);
    void loadLevelFromJsonString(QString json);
    void showAutofillDialog();
    void setEditorWindow(EditorWindow*);   
    LevelProperties* levelProperties(){return &m_levelProperties;}
    bool loaded();

    ItemBehaviour m_behaviour;

public slots:

    void setNewProperties(QJsonObject*);
    void enableGrid(bool);
    void setGridSize(int);
protected:
    LevelProperties m_levelProperties;
    void setupLevel(QJsonDocument* document);
    void clear();
    EditorWindow* editorWindow;
    int scale;
    QJsonObject* newProperties;
    QJsonObject  buffJsonObject;
    EditorItemCreator objectCreator;
    void normalizeZValue();
    void createBlock(int, int);
    bool m_loaded;
    QString m_currentFile;
    int m_ObjectCounter;
signals:   

    void enableGridToItems(bool);
    void setGridSizeToItems(int);
};

#endif // EDITORSCENE_H
