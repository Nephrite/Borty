#ifndef SENSOR_H
#define SENSOR_H

#include <QObject>
#include "physicsobject.h"

class Player;
class Sensor : public PhysicsObject
{
    Q_OBJECT
public:
    Sensor(b2World* world, int x, int y);

    bool collideWith(int type){Q_UNUSED(type);return true;}
    void preSolve(PhysicsObject* o, b2Contact * contact);
    void solveCollision(PhysicsObject* o);
    void endContact(PhysicsObject* o);
    bool onGround();
private:
    PhysicsObject* objA;
    PhysicsObject* objB;
    int m_contactCount;
};

#endif // SENSOR_H
