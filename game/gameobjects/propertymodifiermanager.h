#ifndef PROPERTYEFFECTMANAGER_H
#define PROPERTYEFFECTMANAGER_H

#include <QObject>
#include "common/object.h"
#include <game/gameobjects/propertymodifier.h>
#include <QVector>
#include <QList>

class PropertyModifierManager : public Object
{
public:

    PropertyModifierManager(Object* target);
    PropertyModifierManager();
    ~PropertyModifierManager();
    void setup(QJsonObject& object) override;
    void setTarget(Object* target);
    void clearModifiers();
    void addModifier(PropertyModifier* modifier);

    void advance(int timedelta);
    void processLevelFailed();
    void processLevelFinished();
    bool contains(PropertyModifier* modifier);
protected:

    int m_time;
    Object* m_target;
    QList<PropertyModifier*> m_modifiers;
    void applyAll();
    void disableAll();
    void applyEffect(PropertyModifier* modifier);
    void disableEffect(PropertyModifier* modifier);
};

#endif // PROPERTYEFFECTMANAGER_H
