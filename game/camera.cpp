#include "camera.h"
#include "game/gameobjects/player.h"

#include <QDebug>

Camera::Camera()
{

}

QRectF Camera::viewport(float w, float h)
{
    b2Vec2 m_playerPos = m_player->body->GetPosition();  

    if (m_player->speed().x > 0.001)
    {
        m_target.setX(m_playerPos.x + 6);
    }
    else if (m_player->speed().x < -0.001)
    {
        m_target.setX(m_playerPos.x - 6);
    }

    if (m_player->speed().y > 0.001)
    {
        m_target.setY(m_playerPos.y );
    }
    else if (m_player->speed().y < -0.001)
    {
        m_target.setY(m_playerPos.y - 4);
    }


    if ( (m_playerPos-m_lastPos).Length() < 0.01)
    {
        m_standPoint = m_playerPos;
    }

    bool xfreemove = fabs((m_playerPos.x - m_standPoint.x)) < 2;

    if (fabs(m_center.x() - m_target.x()) > 0.01 && !xfreemove)
    {
        m_lastCenter.setX(m_center.x());
        m_center.setX(m_center.x()+ ((m_target.x()-m_center.x())/32.0));
    }

    bool yfreemove = fabs((m_playerPos.y - m_standPoint.y)) < 1;

    if (fabs(m_center.y() - m_target.y()) > 0.01 && !yfreemove)
    {
        m_lastCenter.setY(m_center.y());
        m_center.setY( m_center.y()+((m_target.y()-m_center.y())/32.0));
    }

    if (h > 760)
    {
        m_size.setHeight(h / 64);
        m_size.setWidth(w / 64);
    }
    else
    {
        m_size.setHeight(h / (64-16));
        m_size.setWidth(w / (64-16));
    }
    m_lastPos = m_playerPos;
    return calculateView(m_center, m_size);
}


b2Vec2 Camera::interpolate(b2Vec2 start, b2Vec2 end, float value)
{

    b2Vec2 result(
                start.x + ((end.x - start.x)*value),
                start.y + ((end.y - start.y)*value)
                );

    return result;
}


void Camera::setPlayer(Player* player)
{
    m_player = player;  
    m_center = QPointF(m_player->body->GetPosition().x, m_player->body->GetPosition().y);
}

void Camera::reset()
{
    m_view = QRectF(-8, -6, 16, 12 ); // 1024x768
    m_size = QSizeF(16,12);

}

QRectF Camera::calculateView(QPointF center, QSizeF size)
{
    m_view.setSize(size);
    m_view.setX(center.x() - size.width() /2.0);
    m_view.setY(center.y() - size.height() /2.0);
    return m_view;
}
