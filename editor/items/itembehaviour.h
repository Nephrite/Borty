#ifndef ITEMBEHAVIOUR_H
#define ITEMBEHAVIOUR_H

class ItemBehaviour
{
public:
    ItemBehaviour();   
    enum Mode {
        move,
        rotate,
        select
    };
    Mode mode;
    void setMode(int m){
        mode  = Mode(m);
    }
    void setMode(Mode m){
        mode  = m;
    }
    int gridSize = 8;
    int arrowStep = 16;
    bool snapToGrid = false;
    bool showCollides = false;
    bool showPhysicsBodies = true;
};

#endif // ITEMBEHAVIOUR_H
