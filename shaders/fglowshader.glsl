#version 130
#ifdef GL_ES
// Set default precision to medium
precision mediump int;
precision mediump float;
#endif


uniform sampler2D alphaMask;

varying vec2 v_texcoord;

uniform vec4 color;



void main()
{
    vec4 color = texture2D(alphaMask, v_texcoord);
    color.rgb = color.rgb*color.aaa;
    gl_FragColor = color;
}




