#ifndef NPC_H
#define NPC_H

#include <QObject>
#include "game/gameobjects/actors/actor.h"
#include "game/gameobjects/tooltipobject.h"

class NPC : public Actor
{
    Q_OBJECT
    Q_PROPERTY_WITH_ACCESSORS(int, activateEvent, setActivateEvent)
    Q_PROPERTY_WITH_ACCESSORS(int, behaviour, setBehaviour)
    Q_PROPERTY_WITH_ACCESSORS(int, distance, setDistance)
    Q_PROPERTY_WITH_ACCESSORS(int, initDirection, setInitDirection)
    Q_PROPERTY_WITH_ACCESSORS(QString, text, setText)
    Q_PROPERTY_WITH_ACCESSORS(QString, propertyName, setPropertyName)
    Q_PROPERTY_WITH_ACCESSORS(int, value, setValue)
    Q_PROPERTY_WITH_ACCESSORS(bool, singleAction, setSingleAction)

public:    
    NPC();
    NPC(QJsonObject& properties, b2World* world);
    ~NPC();
    void advance(int timeDelta);
    void onScreenEnter();
    void preSolve(PhysicsObject* obj, b2Contact* contact);
    void glDraw(GlUtils* glutils);
    enum ActivateOnEvent{
        PlayerTouch,
        OnScreenEnter,
        OnCreate
    };
    enum Behaviour{
        Standing,
        CircleWalking,
        LinearWalking,
        FollowPlayer
    };
protected:
    int m_tooltipTimer;
    int m_toolTipTime;
    int m_timer;
    Direction::directions m_direction;
    b2Vec2 m_basePoint;
    bool m_disabled;
    TooltipObject* m_tooltip;

};

#endif // NPC_H
