#ifndef NEWOBJECTWIDGET_H
#define NEWOBJECTWIDGET_H

#include <QWidget>

namespace Ui {
class newObjectWidget;
}

class newObjectWidget : public QWidget
{
    Q_OBJECT

public:
    explicit newObjectWidget(QWidget *parent = 0);
    ~newObjectWidget();

private:
    Ui::newObjectWidget *ui;
};

#endif // NEWOBJECTWIDGET_H
