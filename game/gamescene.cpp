#include "gamescene.h"

#include "game/gameobjects/basegameobject.h"
#include "game/gameobjects/gamecontactlistener.h"
#include "game/gameobjects/parallaxbackground.h"
#include <game/gameobjects/tooltipobject.h>


#include <QString>
#include <QPointF>
#include <QList>
#include <QJsonDocument>
#include <QJsonArray>
#include <QGLContext>
#include <QKeyEvent>
#include <QDebug>

GameScene::GameScene()
{
    m_player = nullptr;   
    m_finished = false;
    m_loaded = false;  
    m_player = nullptr;
    m_levelProperies = new LevelProperties;
}

GameScene::~GameScene()
{
    destroyWorld();
    clearScene();

}

void GameScene::updateViewport(QSize size)
{
    m_view = m_camera.viewport(size.width(), size.height());

}

void GameScene::createWorld()
{
    b2Vec2 gravity(0.0f, 0.0f);
    p_world = new b2World(gravity);
    p_world->SetAllowSleeping(true);
    m_contactListener = new GameContactListener;
    p_world->SetContactListener(m_contactListener);
    m_objectsCreator.setWorld(p_world);
}

void GameScene::destroyWorld()
{
    delete p_world;
    delete m_contactListener;
}


b2World* GameScene::getWorld()
{
    return p_world;
}



void GameScene::loadLevel(QString file)
{
    if (m_loaded) clearScene();
    createWorld();
    QFile jsonFile(file);
    if ( ! jsonFile.open(QFile::ReadOnly))
    {
        qDebug() << "cant open level file " << file;
        return;
    }


    QJsonDocument document = QJsonDocument::fromJson(jsonFile.readAll());
    QJsonObject obj = document.object();

    m_camera.reset();

    QVariantMap properties = obj.value("properties").toObject().toVariantMap();
    m_levelProperies->setup(&properties);
    createPlayer();
    QJsonArray levelMap = document.object().value("map").toArray();

    QJsonObject props;  
    for (const QJsonValue & value : levelMap)
    {       
        props = value.toObject();
        Object* obj = Object::create(props);

        if (obj == nullptr)
        {
            obj = m_objectsCreator.create(props);
        }
        addObject((BaseGameObject*)obj);
    }

    if (properties.contains("description"))
    {
        QString description = properties.value("description").toString();
        if (!description.isEmpty()) addObject(new TooltipObject(description));
    }
    m_loaded = true;
    m_finished = false;

}

void GameScene::advance(int timeDelta)
{
    deletePendingObjects();  
    updateGameData();
    registerBullets();
    m_lastPlayerPos = m_player->body->GetPosition();
    p_world->Step(timeDelta/1000.0f, 5, 5);    


    for (auto &obj : m_objectsList.backgrounds)
        obj->advance(timeDelta);    

    for (auto &obj : m_objectsList.objects)
        obj->advance(timeDelta);

    for (auto &obj : m_objectsList.lights)
        obj->advance(timeDelta);

    for (auto &obj : m_objectsList.bulletList)
        obj->advance(timeDelta);

    if (m_finished)
    {        
        emit finishLevel(m_finishReason);
        return;
    }    
}

LevelProperties *GameScene::properties()
{
    return m_levelProperies;
}

void GameScene::createPlayer()
{
    if (m_player != nullptr) return;

    QJsonObject playerProps;
    playerProps.insert("x", 0);
    playerProps.insert("y", 0);
    playerProps.insert("width", 126.0);
    playerProps.insert("height", 176.0);
    playerProps.insert("textureStyle", "player");
    playerProps.insert("texture", "borty.png");



    m_player = new Player(playerProps, p_world);
    m_player->setGameData(&m_gameData);

    m_player->setDefaultAmmo(m_levelProperies->defaultAmmo());
    qDebug()<<"defaultAmmo = " << m_levelProperies->defaultAmmo();

    QObject::connect(m_player,
                     &Player::finishLevel,
                     this,
                     &GameScene::onPlayerFinishedLevel
                     ); 
    m_camera.setPlayer(m_player);
    m_objectsList.objects.append(m_player);
}



void GameScene::keyPress(QKeyEvent * event)
{  
    m_player->keyEvent(QKeyEvent::KeyPress, event);
}

void GameScene::keyRelease(QKeyEvent * event)
{
    m_player->keyEvent(QKeyEvent::KeyRelease, event);
}

QRectF GameScene::viewport()
{

    return m_view;
}

QPointF GameScene::playerPos()
{
    return m_player->physicsPos();
}

void GameScene::clearScene()
{
    for (auto &obj : m_objectsList.objects) {
        if (obj != nullptr && obj!= m_player)
            delete obj;
        obj = nullptr;
    }
    m_objectsList.objects.clear();

    for (auto &obj : m_objectsList.bulletList) {
        if (obj != nullptr && obj!= m_player)
            delete obj;
        obj = nullptr;
    }
    m_objectsList.bulletList.clear();

    for (auto &obj : m_objectsList.lights) {
        if (obj != nullptr )
            delete obj;
        obj = nullptr;
    }
    m_objectsList.lights.clear();


    for (auto &obj : m_objectsList.backgrounds) {
        if (obj != nullptr )
            delete obj;
        obj = nullptr;
    }
    m_objectsList.backgrounds.clear();

    delete m_player;
    m_player = nullptr;
    m_finished = false;
    m_loaded = false;
}


void GameScene::deleteObject(BaseGameObject * object)
{
    m_forDeleting.append(object);
}

void GameScene::deleteBullet(BaseGameObject * bullet)
{
    m_forDeleting.append(bullet);
}

void GameScene::deletePendingObjects()
{
    if (m_forDeleting.isEmpty()) return;
    for (auto &obj : m_forDeleting) {
        if (obj != nullptr)
        {
            if (obj->objectType() == type::Bullet)
            {
                m_objectsList.bulletList.removeAll(obj);
            }
            else
            {
                m_objectsList.objects.removeAll(obj);
                m_objectsList.backgrounds.removeAll(obj);
                m_objectsList.lights.removeAll(obj);
            }
            delete obj;
        }
        obj = nullptr;
    }

    m_forDeleting.clear();
}


void GameScene::onPlayerFinishedLevel(Player::finishReason reason)
{  
    m_finished = true;
    m_finishReason = reason;
}

void GameScene::addObject(BaseGameObject *object)
{
    if (object == nullptr)
        return;
    QObject::connect(object,
                     &BaseGameObject::deleteMe,
                     this,
                     &GameScene::deleteObject
                     );
    QObject::connect(object,
                     &BaseGameObject::addNewObject,
                     this,
                     &GameScene::addObject
                     );
    object->setGameData(&m_gameData);
    QString metaType = object->metaType();
    if (metaType == "background") m_objectsList.backgrounds.append(object);
    else if (metaType == "light") m_objectsList.lights.append(object);
    else m_objectsList.objects.append(object);
}

void GameScene::updateGameData()
{
    m_gameData.setPlayerPos(m_player->body->GetPosition());
    m_gameData.setPlayerAngry(true);
    m_gameData.setPlayerFriendly(false);
    m_gameData.setViewport(m_view);

  //  qDebug() << m_gameData.attackCount();

}

void GameScene::registerBullets()
{
    QList<BaseGameObject*> list = m_gameData.newBullets();
    for (auto b : list)
    {
        QObject::connect(b,
                         &BaseGameObject::deleteMe,
                         this,
                         &GameScene::deleteBullet
                         );
        QObject::connect(b,
                         &BaseGameObject::addNewObject,
                         this,
                         &GameScene::addObject
                         );
        b->setGameData(&m_gameData);
        m_objectsList.bulletList.append(b);
    }
    m_gameData.bulletsRegisteredNotify();
}







