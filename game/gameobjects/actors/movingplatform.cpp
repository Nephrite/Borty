#include "movingplatform.h"
#include "common/objecttypes.h"
MovingPlatform::MovingPlatform()
{

}

MovingPlatform::MovingPlatform(QJsonObject &parameters, b2World *world) : PhysicsObject(parameters, world)
{
    setup(parameters);

    m_objectType = type::MovingPlatform;   
    m_forward  = false;
    body->SetUserData(this);
    body->SetType(b2_kinematicBody);
    m_start = body->GetPosition();

}



void MovingPlatform::advance(int timeDelta)
{   
    b2Vec2 direction = (m_targetPosition - body->GetPosition());

    float distanceToTravel = direction.Normalize(); // b2vec2::Normalize() returns length of vector and normalize it

    if ( distanceToTravel < m_velocity*(timeDelta/1000.0) ) // timeDelta is in milliseconds
    {
       linearSpeed = distanceToTravel;
       m_forward = !m_forward;
    }
    else
    {
        linearSpeed = m_velocity;
    }

    body->SetLinearVelocity(linearSpeed * direction);


    if (m_forward)
        m_targetPosition = m_start;
    else
        m_targetPosition = b2Vec2(m_start.x + m_xDistance, m_start.y + m_yDistance);
}




