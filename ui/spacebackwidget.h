#ifndef SPACEBACKWIDGET_H
#define SPACEBACKWIDGET_H

#include <QWidget>
#include <QPaintEvent>
#include <QTimer>
#include <QPen>
class QShowEvent;

struct Star {
    QPointF position = QPointF(0,0);
    int step;
    float size;
    int blinkSpeed = ((rand()%4)+1);
    QPen pen;
    float xSpeed;
    float ySpeed;
    Star() {
        size = ((rand()%4)+1);
        pen.setWidth(size);
        xSpeed = 0.003;
        ySpeed = -float(size/4000.0);
        pen.setColor(0xAAAAAA);
        step = (rand()%360);
    }
};

class SpaceBackWidget : public QWidget
{
    Q_OBJECT
public:
     explicit SpaceBackWidget(QWidget *parent = 0);
     void paintEvent(QPaintEvent*  e);
     void resizeEvent(QResizeEvent* e);
     void showEvent(QShowEvent*);
     void setPause(bool paused){m_paused = paused; advance();}

signals:

public slots:
    void advance();
protected:
    int m_frame;
    QImage back;
    QImage backScaled;
    QVector<Star> stars;
    QTimer timer;
    QImage starColorMap;
    bool m_paused;
};

#endif // SPACEBACKWIDGET_H
