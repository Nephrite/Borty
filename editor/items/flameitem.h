#ifndef FLAMEITEM_H
#define FLAMEITEM_H

#include "editor/items/baseitem.h"
#include <common/objecttypes.h>
#include <QString>
class FlameItem : public BaseItem
{
    Q_OBJECT
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS(QString,  stencilTexture,   setStencilTexture)
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS(QString,  sparcleTexture,   setSparcleTexture)

public:
    FlameItem();
    FlameItem(BaseItem *parent = 0);
    FlameItem(QVariantMap *newProperties);
    ~FlameItem();

    void paintObject(QPainter *painter);
protected:
    QPixmap m_Stencil;
    QPixmap m_Sparcle;

    QImage m_RenderCash;
};

#endif // FLAMEITEM_H
