#include "editorwindow.h"
#include "ui_editorWindow.h"

#include "3rdParty/QPropertyEditor/QPropertyEditorWidget.h"
#include "editorsceneview.h"
#include "editorscene.h"
#include "objecteditor.h"

#include "common/config.h"

#include <QString>
#include <QDebug>
#include <QFile>
#include <QFileDialog>
#include <QJsonDocument>
#include <QJsonArray>
#include <QFontDatabase>
#include <QCloseEvent>

#include <QFont>
#include <QMessageBox>
#include <QJsonValue>
#include <QMetaType>
#include "common/object.h"

EditorWindow::EditorWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::EditorWindow)
{

    ui->setupUi(this);
    ui->extraPropertiesView->setModel(&m_extraPropertiesModel);
    ui->extraPropertiesFrame->setVisible(false);

    createActions();

    m_view = new EditorSceneView;

    ui->tabWidget_2->setContentsMargins(0,0,0,0);
    m_scene = new EditorScene;
    m_scene->setEditorWindow(this);
    m_view->setScene(m_scene);
    m_view->scale(1,1);

    m_view->setEnabled(false);
    ui->menuLayout->setEnabled(false);
    ui->tabWidget_2->setEnabled(false);
    m_appPath = QApplication::applicationDirPath();
    ui->menuLayout->addWidget(m_view);

    QObject::connect(
                ui->itemSelector,
                &ItemSelectWidget::sendNewProperties,
                m_scene,
                &EditorScene::setNewProperties
                );

     QObject::connect(
                 ui->backColorPicker,
                 &ColorPicker::picked,
                 m_scene->levelProperties(),
                 &LevelProperties::setBackColor
                 );
     QObject::connect(
                 ui->ambientLightPicker,
                 &ColorPicker::picked,
                 m_scene->levelProperties(),
                 &LevelProperties::setAmbientLight
                 );
     QObject::connect(
                 ui->defaultAmmoInput,
                 &QLineEdit::textEdited,
                 m_scene->levelProperties(),
                 &LevelProperties::setDefaultAmmo
                 );
}

EditorWindow::~EditorWindow()
{    
    delete ui;
}

void EditorWindow::closeEvent(QCloseEvent *e)
{
    e->accept();
    this->deleteLater();
}

void EditorWindow::setObjectForEditing(BaseItem *obj)
{
    m_currentEditorItem = obj;
    ui->propertyEditor->setObject(obj);
    if (ui->extraPropertiesView->isVisible())
    {
      //  m_extraPropertiesModel.loadJson("{}");
        m_extraPropertiesModel.loadJson(obj->extraProperties());
       // m_extraPropertiesModel.addObject(obj->extraProperties().object());
    }
    ui->tabWidget_2->setCurrentIndex(1);
}

void EditorWindow::removeObjectForEditing()
{
    m_currentEditorItem = nullptr;
    ui->propertyEditor->setObject(nullptr);

}

void EditorWindow::newFile()
{  
    m_scene->newLevel();
    ui->levelTitleInput->setText("new level");
    if (m_scene->loaded())
    {
        m_view->setEnabled(true);
        ui->menuLayout->setEnabled(true);
        ui->tabWidget_2->setEnabled(true);
        updateLevelPropertiesUi();
    }
}

void EditorWindow::on_gridSpin_valueChanged(int arg1)
{
    m_scene->setGridSize(arg1);
}

void EditorWindow::openFile()
{
    QString file = QFileDialog::getOpenFileName(
                this,
                "Open File",
                QString(QCoreApplication::applicationDirPath()+Config::instance().levelsDir()),
                "Level files (*.lvl)"
                );
    if ( ! file.isEmpty())
    {
        m_scene->loadLevelFromFile(file);
        if (m_scene->loaded())
        {
            updateLevelPropertiesUi();           
            m_view->setEnabled(true);
            ui->menuLayout->setEnabled(true);
            ui->tabWidget_2->setEnabled(true);
        }
    }
}



void EditorWindow::on_enableGridCheck_toggled(bool checked)
{
    m_scene->m_behaviour.snapToGrid = checked;
}

void EditorWindow::on_comboBox_currentTextChanged(const QString &arg1)
{
    m_scene->m_behaviour.gridSize = arg1.toInt();
}

void EditorWindow::showCollides(bool checked)
{
    m_scene->m_behaviour.showCollides = checked;
}

void EditorWindow::showPhysics(bool checked)
{
    m_scene->m_behaviour.showPhysicsBodies = checked;
}

void EditorWindow::on_arrowStepInput_currentTextChanged(const QString &arg1)
{
    m_scene->m_behaviour.arrowStep = arg1.toInt();
}

void EditorWindow::saveAs()
{  
    QString fileName = QFileDialog::getSaveFileName(this,
                                                    "Save Level",
                                                    QString(  QCoreApplication::applicationDirPath()
                                                            + Config::instance().levelsDir()
                                                            + ui->levelTitleInput->text()
                                                            + ".lvl"),
                                                    "Level files (*.lvl)"
                                                    );
    m_scene->saveToFile(fileName);
}



void EditorWindow::on_runButon_clicked()
{
    QString file(QCoreApplication::applicationDirPath()+Config::instance().levelsDir()+"test.lvl");
    m_scene->saveToFile(file);
    emit startTestLevel("test.lvl");
}

void EditorWindow::on_modeSelectBox_currentIndexChanged(int index)
{
    m_scene->m_behaviour.setMode(index);

}

void EditorWindow::saveFile()
{
    m_scene->saveToFile();
}


void EditorWindow::on_openObjectEditorButton_clicked()
{
    ObjectEditor* editor = new ObjectEditor;
    editor->show();
}

void EditorWindow::on_levelTitleInput_editingFinished()
{
    m_scene->levelProperties()->setTitle(ui->levelTitleInput->text());
}

void EditorWindow::on_saveDescriptionButton_clicked()
{
    m_scene->levelProperties()->setDescription(ui->levelDescriptionEdit->toPlainText());
}

void EditorWindow::updateLevelPropertiesUi()
{
    ui->ambientLightPicker->setColor(m_scene->levelProperties()->ambientLight());
    ui->backColorPicker->setColor(m_scene->levelProperties()->backColor());
    ui->levelTitleInput->setText(m_scene->levelProperties()->title());
    ui->levelDescriptionEdit->setPlainText(m_scene->levelProperties()->description());
    ui->defaultAmmoInput->setText(m_scene->levelProperties()->defaultAmmo());
    ui->posterFileEdit->setText(m_scene->levelProperties()->posterFile());
}

void EditorWindow::createActions()
{
    m_fileMenu = ui->menuBar->addMenu("Файл");


    m_newFileAct = new QAction("Новый файл");
    m_newFileAct->setParent(m_fileMenu);
    connect(m_newFileAct, &QAction::triggered, this, &EditorWindow::newFile);
    m_fileMenu->addAction(m_newFileAct);


    m_openFileAct = new QAction("Открыть");
    connect(m_openFileAct, &QAction::triggered, this, &EditorWindow::openFile);
    m_fileMenu->addAction(m_openFileAct);

    m_saveFileAct = new QAction("Сохранить");
    connect(m_saveFileAct, &QAction::triggered, this, &EditorWindow::saveFile);
    m_fileMenu->addAction(m_saveFileAct);

    m_saveAsFileAct = new QAction("Сохранить как");
    connect(m_saveAsFileAct, &QAction::triggered, this, &EditorWindow::saveAs);
    m_fileMenu->addAction(m_saveAsFileAct);

    m_viewMenu = ui->menuBar->addMenu("Вид");

    m_showCollidesAct = new QAction("Показывать пересечения");
    m_showCollidesAct->setCheckable(true);
    m_showCollidesAct->setChecked(false);

    connect(m_showCollidesAct, &QAction::toggled, this, &EditorWindow::showCollides);
    m_viewMenu->addAction(m_showCollidesAct);

    m_showCollidesAct = new QAction("Показывать физику");
    m_showCollidesAct->setCheckable(true);
    m_showCollidesAct->setChecked(true);
    connect(m_showCollidesAct, &QAction::toggled, this, &EditorWindow::showPhysics);
    m_viewMenu->addAction(m_showCollidesAct);

}

void EditorWindow::on_showJsonButton_clicked()
{
    on_clearJsonViewButton_clicked();
    ui->textBrowser->append(m_scene->toJson());
}


// make backup and clear text browser
void EditorWindow::on_clearJsonViewButton_clicked()
{
    m_jsonBackup = ui->textBrowser->toPlainText();
    ui->textBrowser->clear();
}

void EditorWindow::on_applyJsonButton_clicked()
{
    QString json = ui->textBrowser->toPlainText();
    if ( ! json.isEmpty())
    {
        m_scene->loadLevelFromJsonString(json);
    }
}

void EditorWindow::on_restoreJsonBackup_clicked()
{
    ui->textBrowser->clear();
    ui->textBrowser->setText(m_jsonBackup);
}

void EditorWindow::on_autoFillButton_clicked()
{
    m_scene->showAutofillDialog();
}

void EditorWindow::on_showExtraPropertiesButton_clicked()
{
    if (ui->extraPropertiesView->isVisible())
        ui->extraPropertiesFrame->setVisible(false);
    else
        ui->extraPropertiesFrame->setVisible(true);

}

void EditorWindow::on_addExtraPropertyObjectButton_clicked()
{
    QString type = ui->newPropertySelecBox->currentText();    
    qDebug() << "type = " << type << "id=" <<  QMetaType::type(type.toLatin1());
    int id = QMetaType::type(type.toLatin1());
    QObject* obj = (QObject*)QMetaType::create(id, nullptr);
    obj->setObjectName(ui->newPropertyNameInput->text());
    if (obj == nullptr)
    {
        qDebug()<< "metaobj is null" ;
        auto msg = new QMessageBox(this);
        msg->setAttribute(Qt::WA_DeleteOnClose, true);
        msg->setText(QString("Не наден тип %1").arg(type));
        msg->setModal(true);
        msg->show();
        return;
    }

    auto js = (Object::toObject(obj)->toJsonObject());
    js.insert("objectType", type);  
    m_extraPropertiesModel.addObject(js);
    ui->extraPropertiesView->update(ui->extraPropertiesView->rootIndex());
    obj->deleteLater();
}

void EditorWindow::on_posterFileEdit_editingFinished()
{
    m_scene->levelProperties()->setPosterFile(ui->posterFileEdit->text());
}

void EditorWindow::on_saveExtraPropertiesButton_clicked()
{
    if (m_currentEditorItem == nullptr)
    {
        qDebug() << Q_FUNC_INFO << " Nothing to save. Object is null";
        return;
    }
    auto json = m_extraPropertiesModel.json();
    qDebug() << Q_FUNC_INFO << " Save: " << json;
    m_currentEditorItem->setExtraProperties(json);
}
