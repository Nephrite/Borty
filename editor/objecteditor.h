#ifndef OBJECTEDITOR_H
#define OBJECTEDITOR_H

#include <QWidget>
#include <QPixmap>
#include <QMetaEnum>
#include <QJsonObject>

#include "editor/items/baseitem.h"
#include "editor/editoritemcreator.h"
#include "common/objecttypes.h"

namespace Ui {
class ObjectEditor;
}

class ObjectEditor : public QWidget
{
    Q_OBJECT

public:

    explicit ObjectEditor(QWidget *parent = 0);
    ~ObjectEditor();

protected:

    BaseItem* m_item;
    QMetaEnum metaEnum;

private slots:

   void on_newObjectButton_clicked();
   void on_loadPixmapButton_clicked();
   void on_editJsonButton_clicked();
   void on_applyJsonButton_clicked();
   void on_saveObjectButton_clicked(); 
   void on_setAutoSizeButton_clicked();

   void on_checkListButton_clicked();

   void on_autoimportButton_clicked();

private:
    void createImportFolders();
    void saveObjectsArray(QJsonArray array, QString file);
    void processType(QJsonArray *objects, type::types t);
    QJsonObject createJson(QString texTureFile, QString style, type::types t);
    void autoFill();
    QString texFile;
    EditorItemCreator itemCreator;
    Ui::ObjectEditor *ui;
    QPixmap m_pixmap;
    QJsonObject jsonTemplate;

};

#endif // OBJECTEDITOR_H
