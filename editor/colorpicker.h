#ifndef COLORPICKER_H
#define COLORPICKER_H

#include <QWidget>
#include "common/qpropertyaccessmacro.h"
#include <QColor>
#include <QRgb>
namespace Ui {
class ColorPicker;
}

class ColorPicker : public QWidget
{
    Q_OBJECT


public:
    explicit ColorPicker(QWidget *parent = 0);
    ~ColorPicker();
    QColor color(){return m_color;}
    void setColor(QColor c);
signals:
    void picked(QColor);
private slots:
    void on_pickButton_clicked();

private:
    QColor m_color;
    Ui::ColorPicker *ui;
};

#endif // COLORPICKER_H
