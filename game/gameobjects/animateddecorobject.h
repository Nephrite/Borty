#ifndef ANIMATEDDECOROBJECT_H
#define ANIMATEDDECOROBJECT_H

#include <QObject>
#include "basegameobject.h"

class AnimatedDecorObject : public BaseGameObject
{
    Q_OBJECT
    PROPERTY(int, rows, setRows)
    PROPERTY(int, columns, setColumns)
    PROPERTY(int, interval, setInterval)
    PROPERTY(int, firstFrame, setFirstFrame)
    PROPERTY(int, frameCount, setFrameCount)

public:
    AnimatedDecorObject();
    AnimatedDecorObject(QJsonObject& parameters);
    void advance(int timeDelta);
protected:
    void setupSprite();
    int m_time;
};

#endif // ANIMATEDDECOROBJECT_H
