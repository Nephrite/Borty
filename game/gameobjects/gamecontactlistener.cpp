#include "gamecontactlistener.h"

#include "basegameobject.h"
#include "game/gameobjects/physicsobject.h"

#include <QDebug>
#include <common/objecttypes.h>
GameContactListener::GameContactListener()
{
}

void GameContactListener::BeginContact(b2Contact *contact)
{
    objA = &(*(PhysicsObject*)contact->GetFixtureA()->GetBody()->GetUserData());
    objB = &(*(PhysicsObject*)contact->GetFixtureB()->GetBody()->GetUserData());
    idA = objA->objectType();
    idB = objB->objectType();
    if (objA->collideWith(idB))
    {
       objA->solveCollision(objB);
    }
    if (objB->collideWith(idA))
    {
       objB->solveCollision(objA);
    }
}


void GameContactListener::EndContact(b2Contact *contact)
{
    objA = &(*(PhysicsObject*)contact->GetFixtureA()->GetBody()->GetUserData());
    objB = &(*(PhysicsObject*)contact->GetFixtureB()->GetBody()->GetUserData());
    idA = objA->objectType();
    idB = objB->objectType();
    if (objA->collideWith(idB))
    {
       objA->endContact(objB);
    }
    if (objB->collideWith(idA))
    {
       objB->endContact(objA);
    }
}

void GameContactListener::PreSolve(b2Contact * contact, const b2Manifold * m)
{
    Q_UNUSED(m);
    fixA = contact->GetFixtureA();
    fixB = contact->GetFixtureB();
    objA = &(*(PhysicsObject*)fixA->GetBody()->GetUserData());
    objB = &(*(PhysicsObject*)fixB->GetBody()->GetUserData());
    idA = objA->objectType();
    idB = objB->objectType();
    if (objA->collideWith(idB))
    {
       objA->preSolve(objB, contact);
    }else
        if (objA->objectType() == type::Player && objB->propertyModifier() != nullptr)
        {
            objA->preSolve(objB, contact);
        }
    if (objB->collideWith(idA))
    {
       objB->preSolve(objA, contact);
    }else
        if (objB->objectType() == type::Player && objA->propertyModifier() != nullptr)
        {
            objB->preSolve(objA, contact);
        }
}

