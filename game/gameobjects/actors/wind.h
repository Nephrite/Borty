#ifndef WIND_H
#define WIND_H

#include "game/gameobjects/physicsobject.h"
#include <QJsonObject>
#include <Box2D/Dynamics/b2World.h>

class Wind : public PhysicsObject
{
public:
    Wind();
    Wind(QJsonObject &parameters, b2World*);
     ~Wind();
};

#endif // WIND_H
