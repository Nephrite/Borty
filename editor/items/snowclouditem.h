#ifndef SNOWCLOUDITEM_H
#define SNOWCLOUDITEM_H

#include "editor/items/baseitem.h"

class SnowCloudItem : public BaseItem
{
    Q_OBJECT
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS(QString,  stencilTexture,   setStencilTexture)

    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS( double, blinkSpeed, setBlinkSpeed)
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS( double, xSpeed, setXSpeed)
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS( double, ySpeed, setYSpeed)

public:
    SnowCloudItem();
    SnowCloudItem(BaseItem *parent = 0);
    SnowCloudItem(QVariantMap *newProperties);
    ~SnowCloudItem();
};

#endif // SNOWCLOUDITEM_H
