#include "editor/autofilldialog.h"
#include "ui_autofilldialog.h"
#include "editor/itembutton.h"

#include <QDebug>
#include <QJsonDocument>

AutofillDialog::AutofillDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AutofillDialog)
{
    ui->setupUi(this);

    QObject::connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(accepted()));
}


QString AutofillDialog::blockJson()
{
    return ui->jsonEditor->toPlainText();
}

AutofillDialog::~AutofillDialog()
{
    delete ui;
}

void AutofillDialog::setJson(QJsonObject* json)
{
    ui->jsonEditor->clear();
    QJsonDocument doc;
    doc.setObject(*json);
    ui->jsonEditor->setPlainText(doc.toJson(QJsonDocument::Indented));
}

int AutofillDialog::startX()
{
    return ui->startXSpin->value();
}

int AutofillDialog::startY()
{
    return ui->startYSpin->value();
}

int AutofillDialog::horizontalStep()
{
    return ui->hStepSpin->value();
}

int AutofillDialog::verticalStep()
{
    return ui->vStepSpin->value();
}

int AutofillDialog::rowCount()
{
    return ui->rowsSpin->value();
}

int AutofillDialog::columnCount()
{
    return ui->colunmsSpin->value();
}

int AutofillDialog::blockCount()
{
    return ui->countSpin->value();
}

void AutofillDialog::accepted()
{
    emit accept();
}

void AutofillDialog::on_rowsSpin_valueChanged(int arg1)
{
    ui->countSpin->setValue(ui->colunmsSpin->value()*arg1);
}

void AutofillDialog::on_colunmsSpin_valueChanged(int arg1)
{
     ui->countSpin->setValue(ui->rowsSpin->value()*arg1);
}
