#ifndef LEVELBUTTON_H
#define LEVELBUTTON_H

#include <QObject>
#include <QWidget>
#include <QPainter>
#include <QLabel>


class LevelButton : public QLabel
{
    Q_OBJECT
public:
    explicit LevelButton(QWidget *parent = 0);
   // void paintEvent(QPaintEvent* event);
    void mouseReleaseEvent(QMouseEvent* event);
    void setLevelFile(QString);
    void setText(QString);
    ~LevelButton();

signals:
    void clicked(QString);
private:
    QString m_file;
    QString m_text;   
    bool m_locked;
    int m_score;
};

#endif // LEVELBUTTON_H
