#include "objecteditor.h"
#include "ui_objecteditor.h"

#include "common/objecttypes.h"

#include <QFileDialog>
#include <QMetaEnum>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonArray>
#include <QMessageBox>
#include <QDir>

ObjectEditor::ObjectEditor(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ObjectEditor)
{
    ui->setupUi(this);
    ui->jsonEditorFrame->setVisible(false);
    ui->loadPixmapButton->setEnabled(false);
    m_item = nullptr;

    metaEnum = QMetaEnum::fromType<type::types>();
    for (int i = 0; i < type::TYPE_COUNT; i++)
    {        
        ui->selectTypeBox->addItem(QString(metaEnum.valueToKey(i)));
    }
    jsonTemplate.insert("objectType", type::PhysicsObject);

    createImportFolders();
}

ObjectEditor::~ObjectEditor()
{
    delete ui;
}


void ObjectEditor::on_newObjectButton_clicked()
{
    if (m_item != nullptr)
    {
        ui->propertyEditor->setObject(nullptr);
        delete m_item;
    }
    jsonTemplate.insert("objectType", QJsonValue(ui->selectTypeBox->currentIndex()));
    m_item = itemCreator.create(jsonTemplate);
    ui->propertyEditor->setObject(m_item);
    ui->loadPixmapButton->setEnabled(true);
}

void ObjectEditor::on_loadPixmapButton_clicked()
{
    texFile = QFileDialog::getOpenFileName(
                this,
                "Select pixmap",
                QCoreApplication::applicationDirPath()+"/res/",
                "Image files (*.png *.jpg *.gif *.bmp)"
                );
    m_pixmap.load(texFile);
    ui->pixmapViewLabel->setPixmap(m_pixmap);    
    autoFill();

}

void ObjectEditor::on_editJsonButton_clicked()
{
    if (ui->jsonEditor->isVisible())
    {        
        ui->propertyEditorFrame->setEnabled(true);
        ui->jsonEditorFrame->setVisible(false);

    }
    else
    {
        if (m_item != nullptr)
        {
            ui->propertyEditorFrame->setEnabled(false);
            ui->jsonEditorFrame->setVisible(true);
            ui->jsonEditor->setPlainText((QJsonDocument::fromVariant(QVariant(m_item->toMap()))).toJson());
        }
    }
}

void ObjectEditor::on_applyJsonButton_clicked()
{
    QJsonDocument doc = QJsonDocument::fromJson(ui->jsonEditor->toPlainText().toUtf8());
    QVariantMap m = doc.object().toVariantMap();  
    m_item->setup(&m);
    ui->propertyEditor->setObject(m_item);
}

void ObjectEditor::on_saveObjectButton_clicked()
{
    QFile jsonFile("./resources/objects.json");

    if (!jsonFile.open(QFile::ReadOnly))
    {
        qDebug()<<jsonFile.errorString();
        return;
    }

    QJsonDocument document = QJsonDocument::fromJson(jsonFile.readAll());
    jsonFile.close();

    QJsonArray objectsArray = document.object().value("objects").toArray();

    QJsonValue newObject = QJsonValue::fromVariant(QVariant(m_item->toMap()));
    objectsArray.append(newObject);
    saveObjectsArray(objectsArray, "./resources/objects.json");

}

void ObjectEditor::saveObjectsArray(QJsonArray objectsArray, QString jsonFileName)
{
    QFile jsonFile(jsonFileName);
    if (!jsonFile.open(QFile::ReadWrite))
    {
        qDebug()<<jsonFile.errorString();
        return;
    }

    QJsonDocument document = QJsonDocument::fromJson(jsonFile.readAll());

    jsonFile.close();
    jsonFile.remove();
    if (!jsonFile.open(QFile::ReadWrite))
    {
        qDebug()<<jsonFile.errorString();
        return;
    }
    QJsonObject object;
    object.insert("objects", objectsArray);

    QJsonDocument newDocument;
    newDocument.setObject(object);

    jsonFile.write(newDocument.toJson());
    jsonFile.close();

    QMessageBox* mess = new QMessageBox(this);
    mess->setText("Saved");
    mess->show();
}


void ObjectEditor::autoFill()
{
    if (!m_item) return;
    m_item->setWidth(m_pixmap.width());
    m_item->setHeight(m_pixmap.height());
    m_item->setTexture(QFileInfo( texFile ).fileName());
    m_item->setTextureStyle(QFileInfo( texFile ).dir().dirName());
}

void ObjectEditor::on_setAutoSizeButton_clicked()
{
    autoFill();
}

void ObjectEditor::on_checkListButton_clicked()
{

}

void ObjectEditor::on_autoimportButton_clicked()
{
    QFile jsonFile("./resources/autoimportobjects.json");
    if ( ! jsonFile.open(QFile::ReadWrite))
    {
        return;
    }
    QJsonDocument document = QJsonDocument::fromJson(jsonFile.readAll());
    jsonFile.close();
    QJsonArray objects = document.object().value("objects").toArray();

    QJsonArray newObjects;
    for (int t = 0; t < type::TYPE_COUNT; t++)
    {
        processType(&newObjects, type::types(t));
    }

    for (auto obj : newObjects)
    {
        objects.append(obj);
    }

    saveObjectsArray(objects, "./resources/autoimportobjects.json");

}

void ObjectEditor::createImportFolders()
{
    QDir typeDir("./resources/autoimport/");
    for (int t = 0; t < type::TYPE_COUNT; t++ )
    {
        typeDir.mkdir(QString(metaEnum.valueToKey(t)));
    }
}

void ObjectEditor::processType(QJsonArray* objects, type::types t)
{
    QString typeName = QString(metaEnum.valueToKey(t));
    QDir typeDir("./resources/autoimport/"+typeName);
    QDir styleDestDir("./resources/textures/");

    QStringList styleList = typeDir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
    for (QString style : styleList)
    {
        styleDestDir.mkdir(style);
        QDir styleDir(typeDir.path() + '/' + style);
        QStringList textures = styleDir.entryList(QStringList("*.png"), QDir::Files | QDir::NoDotAndDotDot);
        for (QString texture : textures)
        {
            objects->append(createJson(texture, style, t));
            QFile::rename("./resources/autoimport/"+typeName+ '/' + style + '/' + texture,
                            "./resources/textures/"   + style + '/' + texture);

        }
    }
}

QJsonObject ObjectEditor::createJson(QString textureFile, QString style, type::types t)
{
    QPixmap pixmap("./resources/autoimport/"+ QString(metaEnum.valueToKey(t))+ '/' + style + '/' + textureFile);

    QJsonObject json;
    json.insert("objectType", QJsonValue(t));
    BaseItem* item = itemCreator.create(json);
    QVariantMap properties = item->toMap();

    properties.insert("width", pixmap.width());
    properties.insert("height", pixmap.height());
    properties.insert("textureStyle", style);    
    properties.insert("texture", textureFile);

    if( t == type::ParallaxBackground)
    {
        properties.insert("metaType", "background");
    }else if (style == "light")
    {
        properties.insert("metaType", "light");
    }else
    {
        properties.insert("metaType", QString(metaEnum.valueToKey(t)));
    }
    if( t == type::BillboardPhysicsObject)
    {
        properties.insert("physicsWidth", properties.value("width").toDouble()*0.8);
        properties.insert("physicsHeight", properties.value("height").toDouble()/6);

    }

    json = QJsonObject::fromVariantMap(properties);
    return json;
}
