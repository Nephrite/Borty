#ifndef CAMERA_H
#define CAMERA_H

#include <Box2D/Box2D.h>
#include <QRectF>

#include "game/gameobjects/player.h"

class Camera
{
public:
    
    Camera();    
    QRectF viewport(float, float);
    void setPlayer(Player*);
    void reset();

protected:
    
    QRectF calculateView(QPointF center, QSizeF size);
    QRectF m_view;
    Player* m_player;
    QPointF m_center;
    QPointF m_lastCenter;
    QPointF m_target;
    b2Vec2 m_standPoint;    
    b2Vec2 m_lastPos;
    QSizeF m_size;
    bool m_freemove;
    float m_speed;
    float m_maxSpeed;
    b2Vec2 interpolate(b2Vec2 start, b2Vec2 end, float value);  

};

#endif // CAMERA_H
