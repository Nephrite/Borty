#include "flameitem.h"
#include <QPainter>
#include <QBitmap>

FlameItem::FlameItem()
{

}

FlameItem::FlameItem(BaseItem *parent): BaseItem(parent)
{

}

FlameItem::FlameItem(QVariantMap *newProperties): BaseItem(newProperties)
{
   setup(newProperties);
   m_Stencil.load("./resources/textures/" + m_textureStyle + '/' + m_stencilTexture);
   m_Sparcle.load("./resources/textures/" + m_textureStyle + '/' + m_sparcleTexture);

   QBitmap mask = (m_Stencil.createMaskFromColor(Qt::black));
   QPainter p;

   m_RenderCash = QImage(QSize(width(), height()), QImage::Format_ARGB32 );
   m_RenderCash.fill(0x0);
   p.begin(&m_RenderCash);
   p.setClipRegion(QRegion(mask));
   p.drawPixmap(0,0, width(), height(), pixmap);
   p.end();
}

FlameItem::~FlameItem()
{

}

void FlameItem::paintObject(QPainter *painter)
{
    painter->drawImage(QRectF(0,0, width(), height()), m_RenderCash);
}
