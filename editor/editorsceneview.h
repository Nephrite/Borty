#ifndef MGRAPHICSVIEW_H
#define MGRAPHICSVIEW_H

#include <QGraphicsView>

class EditorSceneView : public QGraphicsView
{
    Q_OBJECT
public:
    explicit EditorSceneView(QWidget *parent = 0);
    void wheelEvent(QWheelEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent* event);
    void paintEvent(QPaintEvent *event);
public slots:
    void setScale(qreal, qreal);
private:
    QPoint wheelPresPos;
    bool middlePressed;
};


#endif // MGRAPHICSVIEW_H
