#include "directions.h"


const QVector<b2Vec2> Direction::directionVectors{
    b2Vec2(-1.0,  0.0),
    b2Vec2(-0.7, -0.7),
    b2Vec2( 0.0, -0.7),
    b2Vec2( 0.7, -0.7),
    b2Vec2( 1.0,  0.0),
    b2Vec2( 0.7,  0.7),
    b2Vec2( 0.0,  0.7),
    b2Vec2(-0.7,  0.7),
    b2Vec2(0,  0),
};

const  QVector<Direction::directions> Direction::rightWave =
{
    Direction::right,
    Direction::rightUp,
    Direction::right,
    Direction::rightDown
};

const  QVector<Direction::directions> Direction::leftWave =
{
    Direction::left,
    Direction::leftUp,
    Direction::left,
    Direction::leftDown
};

const   QVector<Direction::directions> Direction::upWave =
{
    Direction::up,
    Direction::leftUp,
    Direction::up,
    Direction::rightUp
};

const  QVector<Direction::directions> Direction::downWave =
{
    Direction::down,
    Direction::rightDown,
    Direction::down,
    Direction::leftDown
};

const  QVector<Direction::directions> Direction::nextDir =
{
    Direction::leftUp,
    Direction::up,
    Direction::rightUp,
    Direction::right,
    Direction::rightDown,
    Direction::down,
    Direction::leftDown,
    Direction::stand,
    Direction::left,
};

Direction::Direction()
{

}

Direction::directions Direction::vecToDir(b2Vec2 vec)
{
    Direction::directions dir;
    if (vec.x > 0)
    {
        if (vec.y > 0)
        {
            dir = rightDown;
        }
        else
        {
            dir = rightUp;
        }
    }
    else
    {
        if (vec.y > 0)
        {
            dir = leftDown;
        }
        else
        {
            dir = leftUp;
        }
    }
    return dir;
}
