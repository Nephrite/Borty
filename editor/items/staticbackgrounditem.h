#ifndef STATICBACKGROUNDITEM_H
#define STATICBACKGROUNDITEM_H

#include "editor/items/baseitem.h"

class StaticBackgroundItem : public BaseItem
{
public:
    StaticBackgroundItem();
    StaticBackgroundItem(QVariantMap *newProperties);
};

#endif // STATICBACKGROUNDITEM_H
