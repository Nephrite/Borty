#ifndef HEN_H
#define HEN_H

#include "game/gameobjects/actors/actor.h"

#include <QObject>
#include <game/gameobjects/physicsobject.h>

class Hen : public Actor
{
    Q_OBJECT

public:
    Hen();
    Hen(QJsonObject&, b2World*);
    ~Hen();
    void onScreenEnter();
    void onScreenLeave();
    void preSolve(PhysicsObject* object, b2Contact* contact);
    enum states{idleWalking, Panic};
    void setupSprite();
    void glDraw(GlUtils* glutils);
    void advance(int timedelta);
protected:
    states m_state;
    int m_timer;
    bool m_hungry;

};

#endif // HEN_H
