#include "parallaxbackground.h"

#include "common/objecttypes.h"
#include "Box2D/Box2D.h"

#include <QGLFunctions>
#include <QDebug>

ParallaxBackground::ParallaxBackground(QJsonObject& parameters) : BaseGameObject(parameters)
{

    m_XsizePolicy = expandingSize;
    m_YsizePolicy = fixedSize;
    m_attachToWorld = false;
    m_xOffsetFix = 0;
    setup(parameters);

    m_objectType = type::ParallaxBackground;

    if (m_horizontalMapping == Drawable::TiledMapping ) {
        m_Xtiling *= m_width;
    }
    if (m_verticalMapping == Drawable::TiledMapping )
    {
        m_Ytiling *= m_height;
    }
    m_xAutoScroll/=1000.0;
    m_yAutoScroll/=1000.0;
    m_Xspeed /=m_width;
    m_Yspeed /=m_height;

    m_sprite.currentTexture()->setMagnificationFilter(QOpenGLTexture::LinearMipMapLinear);
    m_sprite.currentTexture()->setMinificationFilter(QOpenGLTexture::LinearMipMapLinear);
    texAspect = double(m_sprite.currentTexture()->width()) / double(m_sprite.currentTexture()->height());

}



ParallaxBackground::ParallaxBackground()
{

}


void ParallaxBackground::glDraw(GlUtils* glutils)
{
    m_sprite.bind();
    glutils->lookAt(m_sprite.m_pos, 0, 0);
    glutils->setShader("parallaxBack");
    if (m_attachToWorld)
    {
        glutils->m_currentShader->setUniformValue("offset",
                                                  QPointF( m_offset.x() + m_xOffsetFix
                                                         , m_offset.y()
                                                         )
                                                  );
    }
    else
    {
        glutils->m_currentShader->setUniformValue("offset", m_offset);
    }
    glutils->m_currentShader->setUniformValue("tiling", m_tiling);
    glutils->glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, 0);
    glutils->releaseShader();
    m_sprite.release();
}

QRectF ParallaxBackground::boundingRect()
{    
    return rect;
}

void ParallaxBackground::setGameData(GameData *data)
{
    m_gameData = data;
}

void ParallaxBackground::advance(int timeDelta)
{
    BaseGameObject::advance(timeDelta);
    updateViewport(m_gameData->viewport());
}

void ParallaxBackground::updateViewport(QRectF viewport)
{
    bool sizeChanged = false;
    m_speed.x  = viewport.x() - rect.x();
    m_speed.y  = viewport.y() - rect.y();
    m_offset.setX(m_offset.x() + (m_speed.x* m_Xspeed) + m_xAutoScroll);
    m_offset.setY(m_offset.y() + (m_speed.y/viewport.height() * m_Yspeed) + m_yAutoScroll);
    b2Vec2 pos(m_x, m_y);

    if (m_attachToWorld)
    {
        m_xOffsetFix = (viewport.x()/(m_width/screenAspect*texAspect));
    }

    if (   fabs(rect.size().width()  - viewport.size().width() ) > 0.001
        || fabs(rect.size().height() - viewport.size().height()) > 0.001
       )
    {       
        sizeChanged = true;
        screenAspect = m_width / m_height;
        m_tiling.setX(m_Xtiling/(texAspect/screenAspect));
        m_tiling.setY(m_Ytiling);
        if (m_XsizePolicy == expandingSize)
        {
            m_width = viewport.width();
        }
        if (m_YsizePolicy == expandingSize)
        {
            m_height = viewport.height();
        }
    }

    if (m_XsizePolicy == expandingSize)
    {        
        pos.x = viewport.center().x();
    }
    if (m_YsizePolicy == expandingSize)
    {       
        pos.y = viewport.center().y();
    }

    if (sizeChanged) m_sprite.setAsBox(m_width, m_height);
    rect = viewport;
    m_sprite.setPos(pos);
}

