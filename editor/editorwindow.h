#ifndef EDITORWINDOW_H
#define EDITORWINDOW_H

#include <QMainWindow>
#include "ui_editorWindow.h"
#include "3rdParty/QJsonModel-master/qjsonmodel.h"

class EditorScene;
class EditorSceneView;

namespace Ui {
class EditorWindow;
}

class EditorWindow : public QMainWindow, Ui::EditorWindow
{
    Q_OBJECT
    
public:
    explicit EditorWindow(QWidget *parent = nullptr);
    void closeEvent(QCloseEvent* e);
    ~EditorWindow();

public slots:    

    // property editor
    void setObjectForEditing(BaseItem *object);
    void removeObjectForEditing();

    // actions
    void newFile();
    void openFile();
    void saveAs();
    void saveFile();

    void showCollides(bool checked);
    void showPhysics(bool checked);

    // item behaviour

    void on_gridSpin_valueChanged(int arg1);    
    void on_enableGridCheck_toggled(bool checked);
    void on_comboBox_currentTextChanged(const QString &arg1);   
    void on_arrowStepInput_currentTextChanged(const QString &arg1);
    void on_modeSelectBox_currentIndexChanged(int index);

    // Saves current level into "./common/levels/test.lvl" and runs it.
    void on_runButon_clicked();

    // level properties
    void on_levelTitleInput_editingFinished();    
    void on_saveDescriptionButton_clicked();    

    // object editor window
    void on_openObjectEditorButton_clicked();

protected:

    void updateLevelPropertiesUi();

    QString m_appPath;
    QString m_jsonBackup;
    EditorScene* m_scene;
    EditorSceneView* m_view;


    Ui::EditorWindow* ui;


    BaseItem* m_currentEditorItem;

    //Menu bar

    void createActions();

    QMenu*   m_fileMenu;
    QAction* m_newFileAct;
    QAction* m_saveFileAct;
    QAction* m_saveAsFileAct;
    QAction* m_openFileAct;
    QMenu*   m_viewMenu;
    QAction* m_showCollidesAct;
    QAction* m_showPhysicsAct;

    QJsonModel m_extraPropertiesModel;

signals:

    void startTestLevel(QString file);

private slots:

    //Json editor
    //TODO: Extract json editor to separate widget;

    void on_showJsonButton_clicked();   
    void on_clearJsonViewButton_clicked();    
    void on_applyJsonButton_clicked();   
    void on_restoreJsonBackup_clicked();

    // autoFillDialog
    void on_autoFillButton_clicked();
    void on_showExtraPropertiesButton_clicked();
    void on_addExtraPropertyObjectButton_clicked();
    void on_posterFileEdit_editingFinished();
    void on_saveExtraPropertiesButton_clicked();
};

#endif // EDITORWINDOW_H
