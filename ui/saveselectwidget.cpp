#include "saveselectwidget.h"
#include "ui_saveselectwidget.h"

#include "common/save.h"
#include <QFile>
#include <QDir>


#include <QDebug>

SaveSelectWidget::SaveSelectWidget(QWidget *parent) :
    PictureBackWidget(parent),
    ui(new Ui::SaveSelectWidget)
{
    ui->setupUi(this);
    setBackground("./resources/saves_back.png");
    loadSaveList();
    ui->newSaveGroupBox->hide();
    ui->newSaveButton->show();
    ui->errorLabel->hide();
}

SaveSelectWidget::~SaveSelectWidget()
{
    delete ui;
}

void SaveSelectWidget::loadSaveList()
{
    qDebug()<< "loadSaveList";
    QFile defaultFile("./resources/saves/defaultslot");
    QString defaultSave;
    if (defaultFile.open(QFile::ReadOnly))
    {
        defaultSave = QString(defaultFile.readAll()).split('\n').first();
        if (defaultSave.isEmpty())
            defaultSave = "Бортия";
        Save::instance()->selectSave(defaultSave);
        qDebug()<< "default " << defaultSave;
    }

    QDir dir("./resources/saves");
    int selectedIndex;
    for (auto& file : dir.entryInfoList(QStringList("*.sav"), QDir::Files | QDir::NoDotAndDotDot) )
    {
        QString saveName = file.baseName();
        ui->savesListWidget->addItem(saveName);       
        if (saveName == defaultSave)
        {
            selectedIndex = ui->savesListWidget->count()-1;
            m_selectedName = saveName;
        }
    }

    if (ui->savesListWidget->count() > 0)
    {
        ui->savesListWidget->
                itemAt(0, selectedIndex)->
                    setSelected(true);
        setSelected(m_selectedName);
    }
}

QString SaveSelectWidget::SaveName()
{
    if (m_selectedName.isEmpty())
    {
        QString name = "Бортия";
        createSave(name);
    }

    writeDefaultName(m_selectedName);

    return m_selectedName;
}

void SaveSelectWidget::writeDefaultName(QString name)
{
    QString slotFile("./resources/saves/defaultslot");
    QFile::remove(slotFile);

    QFile file(slotFile);
    if (!file.open(QFile::ReadWrite))
    {
        return;
    }
    file.write(name.toLocal8Bit());
}

void SaveSelectWidget::on_backButton_clicked()
{
    QString name = SaveName();
    Save::instance()->selectSave(name);
    writeDefaultName(name);
    ui->newSaveGroupBox->hide();
    ui->newSaveButton->show();
    ui->errorLabel->hide();
    emit goBack();
}

void SaveSelectWidget::on_newNameOkButton_clicked()
{
    if ( createSave(ui->saveNameInput->text()))
    {
        ui->newSaveGroupBox->hide();
        ui->newSaveButton->show();
        ui->errorLabel->hide();
    }

}

void SaveSelectWidget::on_newSaveButton_clicked()
{
    ui->newSaveGroupBox->show();
    ui->newSaveButton->hide();
    ui->errorLabel->hide();
}

bool SaveSelectWidget::createSave(QString name)
{
    name.remove('/');
    name.remove(' ');
    name.remove('\\');
    name.remove('.');
    QFile file(QString("./resources/saves/%0.sav").arg(name));

    if (file.exists())
    {
        ui->errorLabel->setText("Уже существует");
        ui->errorLabel->show();
        return false;
    }

    if (!file.open(QFile::ReadWrite))
    {
        ui->errorLabel->setText((file.errorString()));
        ui->errorLabel->show();
        return false;
    }
    file.close();
    ui->savesListWidget->insertItem(0, name);
    ui->savesListWidget->clearSelection();
    ui->savesListWidget->itemAt(0,0)->setSelected(true);
    setSelected(name);
    return true;
}




void SaveSelectWidget::on_savesListWidget_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous)
{
    Q_UNUSED(previous);
    setSelected(current->text());
}

void SaveSelectWidget::setSelected(QString name)
{
    m_selectedName = name;
    ui->selectedLabel->setText(m_selectedName);
}
