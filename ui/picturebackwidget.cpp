#include "picturebackwidget.h"
#include <QPainter>
PictureBackWidget::PictureBackWidget(QWidget *parent) : QWidget(parent)
{

}

void PictureBackWidget::paintEvent(QPaintEvent *e)
{
    if (back.isNull()) return;
    QPainter pa(this);
    pa.drawPixmap(0,
                  0,
                  back.scaled( width(),
                               height(),
                               Qt::KeepAspectRatioByExpanding
                              )
                  );
    QWidget::paintEvent(e);
}

void PictureBackWidget::setBackground(QImage *image)
{
      back = QPixmap::fromImage(*image);
}

void PictureBackWidget::setBackground(QString file)
{
    back.load(file);
}
