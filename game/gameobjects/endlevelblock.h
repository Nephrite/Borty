#ifndef ENDLEVELBLOCK_H
#define ENDLEVELBLOCK_H

#include <QObject>
#include "physicsobject.h"

class EndLevelBlock : public PhysicsObject
{
    Q_OBJECT
public:
    EndLevelBlock();
    EndLevelBlock(QJsonObject& parameters, b2World*);
    bool isAboveOf(BaseGameObject* other);
    virtual ~EndLevelBlock();   
signals:
    void finishLevel();
};

#endif // ENDLEVELBLOCK_H
