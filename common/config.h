#ifndef CONFIG_H
#define CONFIG_H

#include <QString>



class Config
{
    public:
        static Config& instance();
        QString levelsDir(){return QString("/resources/levels/");}
        double scale(){return 64.0;}
        bool useEditorCss(){return false;}
    protected:
        static Config Instance;


    private:
        Config();
        ~Config();
};

#endif // CONFIG_H
