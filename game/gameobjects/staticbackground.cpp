#include "staticbackground.h"
#include "common/objecttypes.h"

StaticBackground::StaticBackground()
{
    m_objectType = type::StaticBackground;
    m_metaType = "background";
}

StaticBackground::StaticBackground(QJsonObject& parameters):
    BaseGameObject(parameters)
{
    m_objectType = type::StaticBackground;
    m_metaType = "background";
}

StaticBackground::~StaticBackground()
{

}
