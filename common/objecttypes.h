#ifndef OBJECTTYPES
#define OBJECTTYPES
#include <QObject>
#include <QMetaEnum>
class type
{
    Q_GADGET
public:
    type(){}    
    enum types{
        Player,
        PhysicsObject,
        frog,
        decorObject,
        EndLevelBlock,
        Wind,
        WaterBlock,
        ParallaxBackground,
        MovingPlatform,
        Coin,
        SpringBug,
        BillboardPhysicsObject,
        StaticBackground,
        Gribbo,
        Bullet,
        Hen,
        ToolTipObject,
        NPC,
        Ammo,
        AnimatedDecorObject,
        SwingingGrass,
        Flame,
        SnowCloud,
        /////////////
        TYPE_COUNT
    };
    Q_ENUM(types)

    enum bullets{
        Note,
        Apple,
        Tomato,
        Egg,
        Carrot,
        Pumpkin,
        Snowball,
        ////////////
        BULLET_TYPE_COUNT
    };
    Q_ENUM(bullets)


    QString toString(type::types t) {
        return QString(QMetaEnum::fromType<type::types>().valueToKey(t));
    }
    QString toString(type::bullets t) {
        return QString(QMetaEnum::fromType<type::bullets>().valueToKey(t));
    }
};

#endif // OBJECTTYPES

