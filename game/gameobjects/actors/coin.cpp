#include "coin.h"
#include "common/objecttypes.h"

Coin::Coin()
{
    m_propertyModifier = new PropertyModifier("coins", 1);
    m_objectType = type::Coin;
}

Coin::Coin(QJsonObject &parameters, b2World *world) : PhysicsObject(parameters, world)
{
    m_propertyModifier = new PropertyModifier("coins", 1);
    m_objectType = type::Coin;
}

bool Coin::collideWith(int type)
{

    return type == type::Player;
}

