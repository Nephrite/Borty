#include "mainwindow.h"
#include "ui_mainwindow.h"


#include <QFileDialog>

#include <ui/worldMap/worldmap.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    gameWidget = ui->stackedWidget;  
    gameController.setGameWidget(gameWidget);
    gameController.setupGameWidget();


}

void MainWindow::startLevel(QString file)
{
    gameController.setLevel(file);
    gameController.start();
}


MainWindow::~MainWindow()
{
    delete ui;
}
