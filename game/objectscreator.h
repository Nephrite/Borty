#ifndef OBJECTSCREATOR_H
#define OBJECTSCREATOR_H
#include "game/gameobjects/basegameobject.h"
#include <QJsonObject>
#include <Box2D/Box2D.h>

class ObjectsCreator
{
public:
    ObjectsCreator();
    BaseGameObject* create(QJsonObject& parameters);
    void setWorld(b2World*);
private:
    b2World* world;

};

#endif // OBJECTSCREATOR_H
