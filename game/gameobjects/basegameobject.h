#ifndef BASEGAMEOBJECT_H
#define BASEGAMEOBJECT_H
//#include "3rdParty/Box2D/Box2D.h"
//#include "3rdParty/Box2D/Dynamics/Contacts/b2Contact.h"

class QPointF;
class QRectF;

class QJsonObject;
class QObject;

#include <QVector>

#include "common/qpropertyaccessmacro.h"
#include "game/glesgameview/glesdrawable.h"
#include "game/glesgameview/glutils.h"
#include "game/gamedata.h"
#include "common/object.h"

class BaseGameObject : public Object
{
    Q_OBJECT
    // from Qobject
    Q_PROPERTY(QString objectName READ objectName WRITE setObjectName DESIGNABLE true USER true)

    Q_PROPERTY_WITH_ACCESSORS(double, width,         setWidth        )
    Q_PROPERTY_WITH_ACCESSORS(double, height,        setHeight       )
    Q_PROPERTY_WITH_ACCESSORS(double, x,             setX            )
    Q_PROPERTY_WITH_ACCESSORS(double, y,             setY            )
    Q_PROPERTY_WITH_ACCESSORS(double, zValue,        setZValue       )
    Q_PROPERTY_WITH_ACCESSORS(double,  yMargin,     setYMargin       )
   // Q_PROPERTY_WITH_ACCESSORS(QString, id,           setId           )
    Q_PROPERTY_WITH_ACCESSORS(QString, texture,      setTexture      )
    Q_PROPERTY_WITH_ACCESSORS(QString, textureStyle, setTextureStyle )
    Q_PROPERTY_WITH_ACCESSORS(QString, metaType,     setMetaType     )
    Q_PROPERTY_WITH_ACCESSORS(int,     objectType,   setObjectType   )
    Q_PROPERTY_WITH_ACCESSORS(double,  rotation,     setRotation     )
    Q_PROPERTY_WITH_ACCESSORS(int,     physicsType,  setPhysicsType  )
    Q_PROPERTY_WITH_ACCESSORS(int,     blendMode,    setblendMode    )


    Q_PROPERTY_WITH_ACCESSORS(int,     verticalMapping,   setVerticalMapping   )
    Q_PROPERTY_WITH_ACCESSORS(int,     horizontalMapping, setHorizontalMapping )
    Q_PROPERTY_WITH_ACCESSORS(bool,    visible,           setVisible )

public:   

    explicit BaseGameObject();
    explicit BaseGameObject(QJsonObject& );

    virtual ~BaseGameObject();

    void setup(QJsonObject &object) override;
    void setupProperties(QJsonObject &object) override;
    virtual void advance(int timeDelta);
    virtual void glDraw(GlUtils* glutils);
    virtual void setGameData(GameData* gameData);
    virtual void onScreenEnter();
    virtual void onScreenLeave();    
    bool isOnScreen();

    virtual QRectF boundingRect();
    virtual QSizeF size();
    virtual double bottomY();
    virtual bool isAboveOf(BaseGameObject* other);
    Drawable* sprite(){return &m_sprite;}

    bool equalZero(float);
    bool equal(b2Vec2, b2Vec2);    

    static QStringList scalableProperties;
    static bool isScalable(QString p);


signals:

    void deleteMe(BaseGameObject*);
    void addNewObject(BaseGameObject*);

protected:
    virtual void setupSprite();
    Drawable m_sprite;

    QSizeF m_size;
    bool m_isOnScreen;
    bool m_active;

};

Q_DECLARE_METATYPE(BaseGameObject)

#endif // BASEGAMEOBJECT_H

