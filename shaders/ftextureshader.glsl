#version 130
#ifdef GL_ES

precision mediump int;
precision mediump float;
#endif

uniform sampler2D texture;

varying vec2 v_texcoord;
uniform vec2 tiling;
uniform vec2 flipping;

void main()
{
    //premultipled alpha
    vec4 color = texture2D(texture, v_texcoord*tiling*flipping);
    color.rgb *= color.aaa;
    gl_FragColor = color;
}
//! [0]
