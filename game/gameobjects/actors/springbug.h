#ifndef SPRINGBUG_H
#define SPRINGBUG_H

#include <QObject>
#include <game/gameobjects/physicsobject.h>

class SpringBug : public PhysicsObject
{
public:
    SpringBug();
    SpringBug (QJsonObject& parameters, b2World*);
    void advance(int timeDelta);
    void solveCollision(PhysicsObject* o);
protected:
    b2Vec2 impulse;
    b2Vec2 basePoint;
    double maxDistance;

};

#endif // SPRINGBUG_H
