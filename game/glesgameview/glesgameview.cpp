#include "glesgameview.h"
#include "glesdrawable.h"
#include "game/gamescene.h"

#include <QOpenGLShaderProgram>
#include <QMatrix4x4>
#include <QOpenGLTexture>
#include <QKeyEvent>
#include <QPainter>
#include <QDateTime>

#define PROGRAM_VERTEX_ATTRIBUTE 0
#define PROGRAM_TEXCOORD_ATTRIBUTE 1

GlesGameView::GlesGameView()
{
    qDebug()<< "GlesGameView::GlesGameView()";
    QSurfaceFormat format;
    format.setDepthBufferSize(8); 
    setFormat(format);    
    startUnfading();
    m_fadeValue = 0;
    m_headup = new HeadUp(this);
}

void GlesGameView::setScene(GameScene * s)
{
    scene = s;   
}

void GlesGameView::enableGL()
{
   // glEnable(GL_DEPTH_TEST);

    glEnable( GL_BLEND );
    glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA); 

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

}

void GlesGameView::initializeGL()
{   
    initializeOpenGLFunctions();
    m_glUtils.setContext(context());
    glClearColor(0, 0, 0, 1);
    enableGL();

    m_glUtils.initializeShaders();

    backGroundBuffer = new QOpenGLFramebufferObject( this->width()
                                                   , this->height()
                                                   , QOpenGLFramebufferObject::NoAttachment
                                                   , GL_TEXTURE_2D
                                                   , GL_RGBA8
                                                   );
    foreGroundBuffer = new QOpenGLFramebufferObject( this->width()
                                                   , this->height()
                                                   , QOpenGLFramebufferObject::NoAttachment
                                                   , GL_TEXTURE_2D
                                                   , GL_RGBA8
                                                   );
    lightBuffer      = new QOpenGLFramebufferObject( this->width()
                                                   , this->height()
                                                   , QOpenGLFramebufferObject::NoAttachment
                                                   , GL_TEXTURE_2D
                                                   , GL_RGBA8
                                                   );
    screenSprite.setAsBox(w,h);   
    screenSprite.setFlipping(1,-1);

    m_headup->setGeometry(1, height()-129, width()-2, 128);

}

void GlesGameView::resizeGL(int w, int h)
{   
    glViewport(0,0,w,h);   

    backGroundBuffer->release();
    delete backGroundBuffer;
    backGroundBuffer = new QOpenGLFramebufferObject( w
                                                   , h
                                                   , QOpenGLFramebufferObject::NoAttachment
                                                   , GL_TEXTURE_2D
                                                   , GL_RGBA8
                                                   );
    foreGroundBuffer->release();
    delete foreGroundBuffer;
    foreGroundBuffer = new QOpenGLFramebufferObject( w
                                                   , h
                                                   , QOpenGLFramebufferObject::NoAttachment
                                                   , GL_TEXTURE_2D
                                                   , GL_RGBA8
                                                   );
    lightBuffer->release();
    delete lightBuffer;
    lightBuffer = new QOpenGLFramebufferObject( w
                                              , h
                                              , QOpenGLFramebufferObject::NoAttachment
                                              , GL_TEXTURE_2D
                                              , GL_RGBA8
                                              );

    m_headup->setGeometry(1, h-129, w-2, 128);

    screenSprite.setTexMappingType(Drawable::VerticalFlipped);
    screenSprite.setAsBox(w,h);

}


void GlesGameView::paintGL()
{
    m_headup->updateData();

 ///////////////////////////////
       //// FADING ////
    switch (m_fadeState) {
    case fade:
        if (m_fadeValue > 0.0) m_fadeValue-=0.02;
        else
        {
            m_fadeValue = 0.0;
            m_fadeState = nofade;
        }
        break;
    case unfade:
        if (m_fadeValue < 1.0) m_fadeValue+=0.02;
        else
        {
            m_fadeValue = 1.0;
            m_fadeState = nofade;
        }
    case nofade:
    default:
        break;
    }

//////////////////////////////
    viewport = scene->viewport();
    m_glUtils.setView(viewport);
    QPainter *p = new QPainter(this);
    p->beginNativePainting();
    enableGL();

    ObjectsLists* list = scene->objectList();

    glActiveTexture(GL_TEXTURE0);
    m_glUtils.m_ambientColor = (scene->properties()->backColor());

    glClearColor( m_glUtils.m_ambientColor.redF()
                , m_glUtils.m_ambientColor.greenF()
                , m_glUtils.m_ambientColor.blueF()
                , 1.0
                );

   m_glUtils.setTime(QDateTime::currentMSecsSinceEpoch());
   // m_glUtils.setShader("default");
    //draw background to buffer

    backGroundBuffer->bind();
    glClear(GL_COLOR_BUFFER_BIT| GL_DEPTH_BUFFER_BIT);
    m_glUtils.lookAt(b2Vec2(0,0), 0);

    for (auto& obj : list->backgrounds)
    {
        obj->glDraw(&m_glUtils);
    }

    m_glUtils.m_backTextureId = backGroundBuffer->texture();
    backGroundBuffer->release();


    // draw foreground to buffer
    glClearColor(0,0,0,0);
    foreGroundBuffer->bind();
    glClear(GL_COLOR_BUFFER_BIT| GL_DEPTH_BUFFER_BIT);  
    m_glUtils.lookAt(b2Vec2(0,0), 0);

/////////////////////////////////////////////////////////////////////////
    /// Collect  visible objects in list ////

    QList<BaseGameObject*> screenList;
    for (auto& obj : list->objects)
    {

        if (obj->boundingRect().intersects(viewport))
        {
            if (!obj->isOnScreen()){
                obj->onScreenEnter();
            }
            screenList.append(obj);
        }
        else
        {
            if (obj->isOnScreen()) {
                obj->onScreenLeave();
            }
        }
    }    
    for (auto& bullet : list->bulletList)
    {
        if ( bullet->boundingRect().intersects(viewport))
        {
            screenList.append(bullet);
        }
        else
        {
            bullet->onScreenLeave();
        }

    }
/////////////////////////////////////////////////////////////////////////
  // Sort list by y

    std::sort(screenList.begin(),
              screenList.end(),
              [](BaseGameObject* a, BaseGameObject* b)
    {
        return b->isAboveOf(a);
    }
    );


 // Draw
    for (auto& obj : screenList)
    {      
        obj->glDraw(&m_glUtils);
    }
    screenList.clear();
    GLuint foretex = foreGroundBuffer->texture();   
    foreGroundBuffer->release();

////////////////////////////////////////////////////////////////////////
    // draw light to buffer    
    lightBuffer->bind();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    m_glUtils.lookAt(b2Vec2(0,0), 0);

    for (auto& obj : list->lights)
    {
        obj->glDraw(&m_glUtils);
    }

    GLuint lighttex = lightBuffer->texture();   
    lightBuffer->release();
    m_glUtils.releaseShader();

    //////////////DRAW_TO_SCREEN/////////////////
    glClearColor(0,0,0,0);
    GLuint t1,t2,t3;
    m_glUtils.lookAt(b2Vec2(0,0),0);
    m_glUtils.setView(QRectF(-width()/2,-height()/2,width(), height()));

    screenShader = m_glUtils.getShaderByName("screen");

    screenShader->bind();

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_glUtils.m_backTextureId);
    screenShader->setUniformValue("backtex", m_glUtils.m_backTextureId);
    t1 = glGetUniformLocation(screenShader->programId(), "backtex");
    glUniform1i(t1, 0);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, foretex);
    screenShader->setUniformValue("objtex", foretex);
    t2 = glGetUniformLocation(screenShader->programId(), "objtex");
    glUniform1i(t2, 1);

    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, lighttex);
    screenShader->setUniformValue("lighttex", lighttex);
    t3 = glGetUniformLocation(screenShader->programId(), "lighttex");
    glUniform1i(t3, 2);

    screenShader->setUniformValue("fade", m_fadeValue);
    screenShader->setUniformValue("ambient", scene->properties()->ambientLight());
    screenSprite.VertexArrayBuf.bind();
    screenSprite.indexBuf.bind();

    screenShader->setUniformValue("mvp_matrix", m_glUtils.m_projection);

    quintptr offset = 0;
    int vertexLocation = screenShader->attributeLocation("a_position");
    screenShader->enableAttributeArray(vertexLocation);
    screenShader->setAttributeBuffer(vertexLocation, GL_FLOAT, offset, 3, sizeof(VertexData));

    offset += sizeof(QVector3D);

    int texcoordLocation = screenShader->attributeLocation("a_texcoord");
    screenShader->enableAttributeArray(texcoordLocation);
    screenShader->setAttributeBuffer(texcoordLocation, GL_FLOAT, offset, 2, sizeof(VertexData));

    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, 0);


    screenSprite.VertexArrayBuf.release();
    screenSprite.indexBuf.release();
    screenShader->release();
    p->end();
    delete p;


}

void GlesGameView::startFading()
{
    m_fadeState = fade;
}

void GlesGameView::startUnfading()
{
    m_fadeState = unfade;

}

void GlesGameView::keyPressEvent(QKeyEvent * event)
{
    switch (event->key()) {
    case Qt::Key_Escape:        
        emit pauseGame();
        break;
    case Qt::Key_Z:
         foreGroundBuffer->toImage().save("fore.png", "png");
         backGroundBuffer->toImage().save("back.png", "png");
         lightBuffer->toImage().save("light.png", "png");
        break;
    default:
        break;
    }

    scene->keyPress(event);
}

void GlesGameView::keyReleaseEvent(QKeyEvent * event)
{
    scene->keyRelease(event);
}

QImage* GlesGameView::toImage()
{
    QPixmap p(size());
    render(&p, QPoint(), QRegion(0,0, width(), height()));
    m_image = p.toImage();
    return &m_image;
}

