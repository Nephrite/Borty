#include "waterblock.h"
#include <QTime>


WaterBlock::WaterBlock()
{    
    m_offset = QVector2D(0.0,0.0);
}


WaterBlock::WaterBlock(QJsonObject &parameters,
                       b2World* world) : decorObject(parameters)
{
    Q_UNUSED(world);
    setup(parameters);  
    rippleTex = m_sprite.addTexture("./resources/textures/ripples.png", QOpenGLTexture::Nearest);
    stencilTex = m_sprite.addTexture("./resources/textures/wave_stencil.png", QOpenGLTexture::Nearest);
    mainTex =  rippleTex -1;

}

void WaterBlock::advance(int timeDelta)
{
    BaseGameObject::advance(timeDelta);
    time += timeDelta;
    m_offset.setX(m_offset.x() + timeDelta/15000.0);
}

void WaterBlock::glDraw(GlUtils* glutils)
{ 
    m_sprite.bind();
    m_sprite.lookHere(glutils);
    glutils->setShader("water");
    m_sprite.selectTexture(mainTex);
    glutils->addTextureToShader("texture0", m_sprite.currentTexture(), GL_TEXTURE0 );

    m_sprite.selectTexture(rippleTex);
    glutils->addTextureToShader("ripples", m_sprite.currentTexture(), GL_TEXTURE1 );

    m_sprite.selectTexture(stencilTex);
    glutils->addTextureToShader("stencil", m_sprite.currentTexture(), GL_TEXTURE2 );

    glutils->shader()->setUniformValue("offset", m_offset );
   // glutils->shader()->setUniformValue("ambient", glutils->m_ambientColor );
    m_sprite.drawBinded(glutils);

    glutils->releaseShader();
    m_sprite.release();    

}

WaterBlock::~WaterBlock()
{

}
