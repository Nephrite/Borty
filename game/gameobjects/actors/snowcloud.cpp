#include "snowcloud.h"

SnowCloud::SnowCloud()
{

}
SnowCloud::SnowCloud(QJsonObject& parameters) : BaseGameObject(parameters)
{
    setup(parameters); 
    setupSprite();
    m_basePos = m_sprite.position();
    m_time = (rand()%10000);
}

void SnowCloud::advance(int timeDelta)
{
    if (!isOnScreen()) return;
    m_time+=timeDelta;
    if (m_attachX || m_attachY)
    {
        b2Vec2 pos = m_sprite.position();
        pos.x = m_attachX ? m_basePos.x + m_gameData->viewport().x() : m_basePos.x ;
        pos.y = m_attachY ? m_basePos.y + m_gameData->viewport().y() : m_basePos.y ;
        m_sprite.setPos(pos);
        m_x = pos.x;
        m_y = pos.y;
    }

    m_sprite.shaderArgs()->time = (m_time);
}

void SnowCloud::setupSprite()
{
    m_sprite.setBindAllTextures(true);
    m_sprite.setShaderName("SnowCloud");
    m_sprite.addTexture("./resources/textures/"+m_textureStyle+'/'+m_stencilTexture, QOpenGLTexture::Nearest);
    m_sprite.shaderArgs()->arg1 = m_blinkSpeed;
    m_sprite.shaderArgs()->arg2 = m_xSpeed;
    m_sprite.shaderArgs()->arg3 = m_ySpeed;
}

void SnowCloud::setGameData(GameData *data)
{
    m_gameData = data;
}

