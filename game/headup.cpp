#include <QKeyEvent>
#include <QDebug>

#include "headup.h"
#include <QFile>
#include <QDebug>
#include <QFontDatabase>

HeadUp::HeadUp(QWidget *parent) : QWidget(parent)
{
    setObjectName("HeadUp");
    m_playerHealth = -1;

    m_font = QFont(QFontDatabase::applicationFontFamilies(QFontDatabase::GeneralFont).at(0));

    m_font.setPixelSize(32);
    m_font.setLetterSpacing(QFont::AbsoluteSpacing, 1.5);
    setFont(m_font);

    m_heart = QRect(0,0,64,32);    
    m_bottleRegion = m_bottleRect;
    m_bottlePix = QPixmap("./resources/ui/bottle.png");
    m_bottleRect = QRect(0,0,(m_bottlePix.width()/6),m_bottlePix.height());
    m_coinPix = QPixmap("./resources/ui/coin.png");

    QFile styleFile("./resources/headup.qss");
    if (styleFile.open(QFile::ReadOnly))
    {
        setStyleSheet(styleFile.readAll());
    }

    m_layout = new QHBoxLayout(this);
    m_layout->setContentsMargins(40,0,0,0);

    m_layout->setSpacing(20);
    m_bottleLabel = new QLabel(this);
    m_layout->addWidget(m_bottleLabel);


    m_coinIconLabel = new QLabel(this);
    m_coinIconLabel->setPixmap(m_coinPix);
    m_layout->addWidget(m_coinIconLabel);

    m_coinCountLabel = new QLabel(this);
    m_layout->addWidget(m_coinCountLabel);
    m_coinCountLabel->setFont(m_font);
    m_ammoLayout = new QHBoxLayout();
    m_layout->addLayout(m_ammoLayout);
    m_layout->addStretch(1);

    setLayout(m_layout);
    timer = 0;
}

HeadUp::~HeadUp()
{

}

void HeadUp::setPlayer(Player* player)
{
    m_player = player;
    m_playerAmmo = player->ammo();
    m_selected = m_player->currentAmmo();
    loadIcons();
    updateData();
    updateSelection();
}

void HeadUp::updateData()
{
    ++timer;
    if (m_playerHealth != m_player->health())
    {
        m_playerHealth = m_player->health();
        m_bottleRegion.setTopLeft(QPoint(m_player->health()*m_bottleRect.width(), 0));
        m_bottleRegion.setSize(m_bottleRect.size());
        m_bottleLabel->setPixmap(m_bottlePix.copy(m_bottleRegion));
    }
    if (timer > 10)
    {
        updateAmmo();
        updateSelection();
        timer = 0;
        m_coinCountLabel->setText("x"+ QString::number(m_player->coins()));
    }
}

void HeadUp::loadIcons()
{
    cleanup();
    type typeData;
    type::bullets bullet;
    for (int t = 0; t < type::BULLET_TYPE_COUNT; ++t)
    {
        bullet = type::bullets(t);
        IconLabel* label = new IconLabel(this);
        label->setFont(m_font);
        label->setPixmap(QPixmap("./resources/ui/"+
                                 typeData.toString(bullet)+".png"));
        if(t != m_player->defaultAmmo())
            label->setText(QString::number(m_playerAmmo->at(bullet)));
        if (m_playerAmmo->at(bullet) < 1) label->setVisible(false);
        label->setDisabled(true);
        m_ammoLayout->addWidget(label);
        m_ammoLabels.append(label);
    }

    m_selected = m_player->currentAmmo();
    m_ammoLabels[m_selected]->setEnabled(true);
}

void HeadUp::cleanup()
{
    for (auto& label : m_ammoLabels)
    {
        m_ammoLayout->removeWidget(label);
        delete label;
    }
    m_ammoLabels.clear();
}

void HeadUp::updateAmmo()
{   
    for (int t = 0; t < type::BULLET_TYPE_COUNT; ++t)
    {
        if (t == m_player->defaultAmmo())
        {
            m_ammoLabels[t]->setVisible(true);
            continue;
        }
        if (m_playerAmmo->at(t) > 0)
        {
            m_ammoLabels[t]->setVisible(true);
            m_ammoLabels[t]->setText(QString::number(m_playerAmmo->at(t)));
        }
        else
        {
            m_ammoLabels[t]->setVisible(false);
        }
    }
}

void HeadUp::updateSelection()
{
    if (m_selected != m_player->currentAmmo())
    {
        m_ammoLabels[m_selected]->setDisabled(true);
        m_selected = m_player->currentAmmo();
        m_ammoLabels[m_selected]->setEnabled(true);
    }
}


IconLabel::IconLabel(QWidget *parent) : QWidget(parent)
{
    layout = new QHBoxLayout(this);
    iconLabel = new QLabel(this);
    layout->addWidget(iconLabel);
    textLabel = new QLabel(this);
    layout->addWidget(textLabel);
}
