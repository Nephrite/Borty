#include "endlevelscreen.h"
#include "ui_endlevelscreen.h"

EndLevelScreen::EndLevelScreen(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EndLevelScreen)
{
    ui->setupUi(this);
    setPoster("./resources/posters/default.png");
}

EndLevelScreen::~EndLevelScreen()
{
    delete ui;
}

void EndLevelScreen::keyPressEvent(QKeyEvent *e)
{
    switch (e->key()) {
    case Qt::Key_Return:

        emit nextLevel();
        break;

    case Qt::Key_Escape :

        emit goToMenu();
        break;
    case Qt::Key_Down:
        focusNextChild();
        break;

    case Qt::Key_Up:
        focusPreviousChild();
        break;
    default:
        break;
    }
}

void EndLevelScreen::updateData(Player *p)
{
    ui->livesLabel->setText("<H1>"+QString::number(p->lives())+"</H1>");
    ui->pointsLabel->setText("<H1>"+QString::number(p->coins())+"</H1>");
}

void EndLevelScreen::setPoster(QString file)
{
    if (file.startsWith("./resources/posters/"))
        ui->posterLabel->setPixmap(QPixmap(file));
    else
        ui->posterLabel->setPixmap(QPixmap("./resources/posters/"+file));
}

void EndLevelScreen::on_okButton_clicked()
{
    emit nextLevel();
}
