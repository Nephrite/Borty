#ifndef ANIMATEDDECORITEM_H
#define ANIMATEDDECORITEM_H

#include <QObject>
#include "editor/items/baseitem.h"
#include "common/qpropertyaccessmacro.h"

class AnimatedDecorItem : public BaseItem
{
    Q_OBJECT
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS(int, rows, setRows)
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS(int, columns, setColumns)
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS(int, interval, setInterval)
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS(int, firstFrame, setFirstFrame)
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS(int, frameCount, setFrameCount)
public:
    AnimatedDecorItem();
    ~AnimatedDecorItem();
    explicit AnimatedDecorItem(QGraphicsItem *parent = 0);
    explicit AnimatedDecorItem(QVariantMap *newProperties);

    void paintObject(QPainter *painter);
    int m_frameW;
    int m_frameH;
};

#endif // ANIMATEDDECORITEM_H
