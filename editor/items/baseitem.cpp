#include "baseitem.h"
#include "common/objecttypes.h"

#include <QMetaProperty>
#include <QMetaObject>
#include <QVariantMap>
#include <QJsonObject>

#include <QPainter>
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>

#include <QDebug>

BaseItem::BaseItem(QGraphicsItem *parent) :
    QGraphicsItem(parent),
    m_texture("midle.png"),
    m_textureStyle("forest"), 
    m_width(10),
    m_height(10),
    m_yMargin(0),
    m_objectType(type::PhysicsObject),    
    m_metaType ("object"),   
    m_fixedRotation(true),
    m_verticalMapping(StretchedMapping),
    m_horizontalMapping(StretchedMapping),
    m_physicsType(staticBody),
    m_visible(true),
    m_fullSize(true),
    inMoving(false)
{    

    pixmap.load("./resources/textures/" + m_textureStyle + '/' + m_texture);
    setFlags( QGraphicsItem::ItemIsMovable
            | QGraphicsItem::ItemIsSelectable
            | ItemSendsGeometryChanges
            );
    setTransformOriginPoint(QPointF(m_width/2.0,m_height/2.0));
}

BaseItem::BaseItem(QVariantMap *properties):
    m_texture("midle.png"),
    m_textureStyle("forest"),   
    m_width(10),
    m_height(10),
    m_yMargin(0),
    m_objectType(type::PhysicsObject),
    m_metaType ("object"),   
    m_fixedRotation(true),
    m_verticalMapping(StretchedMapping),
    m_horizontalMapping(StretchedMapping),
    m_physicsType(staticBody),
    m_visible(true),
    m_fullSize(true),
    inMoving(false)
{   
    setup(properties);
    pixmap.load("./resources/textures/" + m_textureStyle + '/' + m_texture);
    setFlags(QGraphicsItem::ItemIsMovable
            |QGraphicsItem::ItemIsSelectable
            |  ItemSendsGeometryChanges
            );        
    inMoving = false;
    setZValue(properties->value("zValue").toDouble());
}


BaseItem::~BaseItem()
{

}

QVariantMap BaseItem::toMap()
{
    auto map = Serializable::toMap();
    map.insert("childs", m_ExtraProperties.object().toVariantMap());
    return map;
}

void BaseItem::updateProperties()
{   
    update(boundingRect());
}

void BaseItem::setPosition(double x, double y)
{
    setPos(x, y); 
}


void BaseItem::setBehaviour(ItemBehaviour *newBehaviour)
{   
     behaviour = newBehaviour;
}

void BaseItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->save();
    painter->translate(width()/-2, height()/-2);
    paintObject(painter);
    paintHelpers(painter);

    painter->restore();
}


void BaseItem::paintObject(QPainter *painter)
{
     painter->drawPixmap(0, 0, int(width()), int(height()), pixmap);
     painter->drawRect(width()/2, height()/2, 10, 10);
}

void BaseItem::paintHelpers(QPainter *painter)
{
    auto w = int(width());  
    auto h = int(height());
    if (behaviour->showCollides)
    {
        QList<QGraphicsItem*> list = scene()->items(
                    QRectF(
                        pos().x()-width()/2,
                        pos().y()-height()/2,
                        width(),
                        height()
                        )
                    );

        if (list.count() > 1)
        {
            painter->setPen(QPen(Qt::red, 2, Qt::SolidLine));
            painter->drawLine(0, 0, w, h);
            painter->drawLine(0, w, h ,0);
        }
    }
    if (isSelected())
    {
        painter->setPen(QPen( Qt::blue, 4 , Qt::DashLine));
        painter->drawRect(4, 4, w-4, h-4);
    }

}

QRectF BaseItem::boundingRect() const
{
    return QRectF(-width()/2, -height()/2, width(), height());
}

void BaseItem::toGrid()
{
    if (!behaviour) return;
    if ( ! behaviour->snapToGrid || behaviour->mode != ItemBehaviour::move ) return;

    QPointF pos = mapToScene(-m_width/2.0, -m_height/2.0);
    int x = align(int(pos.x()), behaviour->gridSize) ;
    int y = align(int(pos.y()), behaviour->gridSize) ;

    setPosition(x + m_width/2.0, y +m_height/2.0);

    m_isAligned = true;
}

QJsonDocument& BaseItem::ExtraPropertiesJson()
{
    return  m_ExtraProperties;
}

void BaseItem::setExtraProperties(QJsonDocument document)
{
    m_ExtraProperties = document;
}


int BaseItem::align(int value, int gridSize)
{
    int offset = abs(value % gridSize);
    if (value < 0)
    {
        if (offset > (gridSize / 2))
        {
            value -= gridSize - offset;
        }
        else
        {
            value += offset;
        }
        return value;
    }
    else
    {
        if (offset > (gridSize / 2))
        {
            value += gridSize - offset;
        }
        else
        {
            value -= offset;
        }
        return value;
    }   
}

void BaseItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if (behaviour->mode == ItemBehaviour::rotate)
    {
        setFlags(QGraphicsItem::ItemIsSelectable );
    }
    else
    {
        setFlags(QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemIsMovable |  ItemSendsGeometryChanges );
        inMoving = true;
    }    
    QGraphicsItem::mousePressEvent(event);
}

void BaseItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mouseReleaseEvent(event);
    setFlags( QGraphicsItem::ItemIsMovable
            | QGraphicsItem::ItemIsSelectable
            | ItemSendsGeometryChanges
            );
    if (event->button() == Qt::LeftButton && !m_isAligned)
    {
        toGrid();
    }
    inMoving = false;
    bool selected = isSelected();

    setSelected(selected);
}

void BaseItem::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{    
    emit editProperties(this);
    QGraphicsItem::mouseDoubleClickEvent(event);
}

void BaseItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mouseMoveEvent(event);
    if (behaviour->mode == ItemBehaviour::rotate)
    {
        setRotation(rotation()+(event->lastScenePos().y() - event->scenePos().y()));
    }
}

QVariant BaseItem::itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value)
{
    if (change == ItemPositionChange)
    {
        m_isAligned = false;
    }
    return QGraphicsItem::itemChange(change, value);
}

