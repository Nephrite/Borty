#ifndef PAUSEMENU_H
#define PAUSEMENU_H

#include <QWidget>

namespace Ui {
class PauseMenu;
}

class PauseMenu : public QWidget
{
    Q_OBJECT

public:
    explicit PauseMenu(QWidget *parent = 0);
    void paintEvent(QPaintEvent* e);
    ~PauseMenu();
    void setBackground(QImage* );
    void keyPressEvent(QKeyEvent *);
signals:
    void continueGame();
    void showMenu();
private slots:

    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

private:
    QPixmap back;
    Ui::PauseMenu *ui;
};

#endif // PAUSEMENU_H
