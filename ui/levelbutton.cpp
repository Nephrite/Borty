#include "levelbutton.h"
#include <QMouseEvent>

LevelButton::LevelButton(QWidget *parent) :
    QLabel(parent)
  , m_file("./common/levels/test.lvl")
  , m_text("1:1\nTest") 
  , m_locked(true)
  , m_score(0)
{
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    setMaximumHeight(80);
    setMinimumHeight(64);
    m_score = rand()%4;
    m_locked = false;
    setWordWrap(true);
    setStyleSheet("border: 1px solid dark-gray;"
                  "background-color: rgba(200,200,200,100);"
                  "font-size: 20px;"
                  "color: #444;"
                 );
    setTextFormat(Qt::RichText);
    setAlignment(Qt::AlignCenter);
}

LevelButton::~LevelButton()
{

}


void LevelButton::mouseReleaseEvent(QMouseEvent *event)
{
    if (! m_locked) emit clicked(m_file);
    event->accept();
}

void LevelButton::setLevelFile(QString file)
{
    m_file = file;
    QStringList l = m_file.split('/');
    setText(l.last());
}

void LevelButton::setText(QString text)
{      
    m_text = QString("<html><h2>%1</h2><html>").arg(text);
    QLabel::setText(m_text);
}
