#ifndef MAINMENU_H
#define MAINMENU_H

#include "ui/spacebackwidget.h"
#include <QKeyEvent>

namespace Ui {
class MainMenu;
}

class MainMenu : public SpaceBackWidget
{
    Q_OBJECT

public:

    explicit MainMenu(QWidget *parent = 0);   
    ~MainMenu();
    void keyPressEvent(QKeyEvent* e);
    void showEvent(QShowEvent* e);
    void displaySaveName(QString name);

signals:

    void showLevelPackWidget();
    void exitGame();
    void showGameView();
    void runEditor();
    void showSaveSelectWidget();

private:

    Ui::MainMenu *ui;


private slots:

    void on_levelsButton_clicked();
    void on_pushButton_2_clicked();
    void on_runEditorButton_clicked();

    void on_exitButton_clicked();
    void on_saveSelectButton_clicked();
};

#endif // MAINMENU_H
