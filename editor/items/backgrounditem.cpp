#include "backgrounditem.h"
#include "common/objecttypes.h"

#include <QJsonObject>

BackgroundItem::BackgroundItem()
{
    m_objectType = type::ParallaxBackground;
    m_YsizePolicy = fixedSize;
    m_XsizePolicy = expandingSize;
    m_Xtiling = 1;
    m_Ytiling = 1;
    m_Xspeed = 0.0;
    m_Yspeed = 0.0;
}

BackgroundItem::BackgroundItem(QVariantMap *newProperties) : BaseItem(newProperties)
{
    m_YsizePolicy = fixedSize;
    m_XsizePolicy = expandingSize;
    m_Xtiling = 1;
    m_Ytiling = 1;
    m_Xspeed = 0.0;
    m_Yspeed = 0.0;
    setup(newProperties);
    m_objectType = type::ParallaxBackground;
}

BackgroundItem::~BackgroundItem()
{

}
