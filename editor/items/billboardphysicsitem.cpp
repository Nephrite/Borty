#include "billboardphysicsitem.h"
#include <QPainter>

BillBoardPhysicsItem::BillBoardPhysicsItem()
{

}

BillBoardPhysicsItem::BillBoardPhysicsItem(BaseItem *parent) :
     BaseItem(parent),
     m_physicsWidth(64),
     m_physicsHeight(64)
{

}

BillBoardPhysicsItem::BillBoardPhysicsItem(QVariantMap *newProperties):
    BaseItem(newProperties),
    m_physicsWidth(64),
    m_physicsHeight(64)
{
    setup(newProperties);
}

BillBoardPhysicsItem::~BillBoardPhysicsItem()
{

}

void BillBoardPhysicsItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    BaseItem::paint(painter, option, widget);
    if (behaviour->showPhysicsBodies)
    {
        painter->setBrush(Qt::lightGray);
        painter->drawRect(QRectF(
                              -m_physicsWidth /2 ,
                              m_height/2 - m_physicsHeight ,
                              m_physicsWidth,
                              m_physicsHeight
                              ));
    }

}
