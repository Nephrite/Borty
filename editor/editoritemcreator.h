#ifndef OBJECTCREATOR_H
#define OBJECTCREATOR_H

#include "items/baseitem.h"
#include <qgraphicsscene.h>

class EditorItemCreator
{
public:
    EditorItemCreator();
    BaseItem* create(QVariantMap* prop);
    BaseItem* create(QJsonObject prop );
    BaseItem* create(QJsonObject* prop );
};

#endif // OBJECTCREATOR_H
