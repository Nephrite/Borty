#ifndef SERIALIZABLE_H
#define SERIALIZABLE_H

#include <QObject>
#include <QMetaObject>
#include <QMetaProperty>
#include <QJsonArray>


#define Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS(type, name, setter)                         \
                  protected: type m_##name;                                              \
                  public:                                                                \
                  Q_PROPERTY(type name READ name WRITE setter DESIGNABLE true USER true) \
                  type const& name () const { return m_##name; }                         \
                  void setter (type const &v) { m_##name = v; }


#define Q_PROPERTY_DESIGNABLE_NOTIFY_WITH_ACCESSORS(type, name, setter, notify)          \
                  protected: type m_##name;                                              \
                  public:                                                                \
                  Q_PROPERTY(type name READ name WRITE setter NOTIFY notify DESIGNABLE true USER true) \
                  type const& name () const { return m_##name; }                         \
                  void setter (type const &v) { m_##name = v; }


#define Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS_SLOTS(type, name, setter)                   \
                  protected: type m_##name;                                              \
                  public:                                                                \
                  Q_PROPERTY(type name READ name WRITE setter DESIGNABLE true USER true) \
                  type const& name () const { return m_##name; }                         \
                  public slots: void setter (type const &v) { m_##name = v; }                          \



class Serializable : public QObject
{
    Q_OBJECT
public:
    explicit Serializable(QObject *parent = nullptr);
    Serializable(QVariantMap*);
    ~Serializable();
    virtual void setup(QVariantMap*);
    virtual QVariantMap toMap();
    QJsonArray m_ChildsProperties;


};

#endif // SERIALIZABLE_H
