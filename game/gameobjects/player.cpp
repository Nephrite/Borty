#include "player.h"

#include "common/objecttypes.h"
#include "Box2D/Box2D.h"
#include "game/glesgameview/glutils.h"

#include <QOpenGLTexture>
#include <QKeyEvent>
#include <QDebug>

#include <math.h>
#include <game/gameobjects/bullet.h>
#include <game/gameobjects/ammo.h>
#include <game/gameobjects/endlevelblock.h>

#include "common/config.h"
#include "common/save.h"


Player::Player(QJsonObject& parameters,   b2World* world) :
    Actor(parameters, world),   
    m_bullets(10),    
    m_state(normal)
{   
    m_damageInterval = 2000;
    m_lastDamageTimeElapsed = 0;
    m_rotation = 0.0;    
    m_zValue = 0;
    m_direction = Direction::right;
    m_oldDirection = Direction::right;
    m_inJump = false;
    m_falling = false;
    m_damping = 10;
    m_maxJumpHeight = 0;
    m_jumpHeight = 0;
    m_maxVelocity = 5;
    m_lastBulletTime = 0;


    collisionList        
            << type::EndLevelBlock
            << type::Wind
            << type::MovingPlatform
            << type::Coin
            << type::Bullet
            << type::Gribbo
            << type::Ammo
               ;

    b2Filter filter;
    filter.categoryBits = PhysicsLayer::Sensor | PhysicsLayer::Loot | PhysicsLayer::BulletPlayerTarget ;
    filter.maskBits = PhysicsLayer::BulletPlayerTarget | PhysicsLayer::Loot;
    b2Fixture* fix;
    fix = body->GetFixtureList();
    fix->GetNext()->SetFilterData(filter);

    setCoins(0);

    m_objectType = type::Player;


    //////////////////////////////////////////////////////

    setupShadowSprite();
    m_sprite.setShaderName("animatedSprite");
    m_ySpriteOffset+=0.2;


    m_ammo.fill(0, type::BULLET_TYPE_COUNT);

    m_propertyModifierManager.setObjectName("PlayerModifiers");
    m_propertyModifierManager.setTarget(&m_stat);
    setObjectName("player");
}

Player::Player()
{

}


Player::~Player()
{    

}


void Player::solveCollision(PhysicsObject *obj)
{
    switch (obj->objectType())
    {
    case type::Wind:
        m_inJump = true;
        break;

    case type::Coin:        
        obj->deleteMe(obj);
        break;
    default:
        break;
    }
}

void Player::preSolve(PhysicsObject * obj, b2Contact *contact)
{   
    if (obj->objectType() != type::Bullet && obj->propertyModifier() != nullptr && !m_propertyModifierManager.contains(obj->propertyModifier()))
    {
       m_propertyModifierManager.addModifier(obj->propertyModifier());
    }

    switch (obj->objectType())
    {
    case type::EndLevelBlock :        
        contact->SetEnabled(false);       
        if (m_keyData.ePressed)
        {
            emit finishLevel(succes);
        }
        break;

    case type::Wind :
        contact->SetEnabled(false);
        body->ApplyLinearImpulse(
               b2Vec2(0, -0.1),
               b2Vec2(0.5,0.5)
               );
        break;
    case type::Coin :
        contact->SetEnabled(false);
        break;
    case type::Bullet:
    {
        contact->SetEnabled(false);
        Bullet* b = nullptr;
        b = dynamic_cast<Bullet*>(obj);
        if (b != nullptr && (b->target() == Bullet::PlayerTarget || b->target() == Bullet::AllTarget))
        {
            if (b->propertyModifier()->name() == "health") processDamage(obj->propertyModifier());
            b->disable();
        }
    }
        break;

    case type::Ammo:
    {
        contact->SetEnabled(false);
        Ammo* a = nullptr;
        a = obj->as<Ammo*>();
        if (a != nullptr)
        {
            addAmmo(type::bullets(a->bulletType()), a->count());
        }
        a->disable();
    }
    }
}

bool Player::isAboveOf(BaseGameObject *other)
{
    auto door = other->as<EndLevelBlock*>();
    if (door)
    {
        return true; // player is allways on top of the door
    }
    else
    {
        return BaseGameObject::isAboveOf(other);
    }
}

void Player::processLevelFinish()
{
    m_propertyModifierManager.processLevelFinished();
}

void Player::procesLevelFail()
{
    m_propertyModifierManager.processLevelFailed();
}

QVector<int> *Player::ammo()
{
    return &m_ammo;
}

type::bullets Player::currentAmmo()
{
    return m_currentAmmo;
}

type::bullets Player::defaultAmmo()
{
    return m_defaultAmmo;
}

void Player::setDefaultAmmo(QString a)
{
   QMetaEnum e =  QMetaEnum::fromType<type::bullets>();
   m_defaultAmmo = type::bullets(e.keyToValue(a.toLatin1()));
   if (int(m_defaultAmmo) == -1) m_defaultAmmo = type::Apple;
   m_currentAmmo = m_defaultAmmo;
   m_ammo[m_currentAmmo] = 999;

   qDebug()<< "set ammo " << m_defaultAmmo;
}

void Player::gribboCatch()
{
    m_state = catched;
    m_visible = false;
    m_timer = 0;
}

double Player::bottomY()
{
    return body->GetPosition().y;
}



void Player::keyEvent(int eventType, QKeyEvent * event)
{   
    if (eventType == QKeyEvent::KeyPress)
    {
        switch (event->key())
        {
        case Qt::Key_A :
        {
            m_keyData.leftPresed = true;
            break;
        }
        case Qt::Key_D :
        {
            m_keyData.rightPresed = true;
            break;
        }
        case Qt::Key_W :
        {
            m_keyData.upPressed = true;
            break;
        }
        case Qt::Key_E :
        {
            if (!event->isAutoRepeat())
                m_keyData.ePressed = true;
            break;
        }
        case Qt::Key_S :
        {
            m_keyData.downPresed = true;
            break;
        }
        case Qt::Key_Space :
        {
            m_keyData.spacePressed = true;            
            break;
        }
        case Qt::Key_Enter :
        {
            m_keyData.enterPressed = true;
        }

        }
    }
    if (eventType == QKeyEvent::KeyRelease)
    {
        switch (event->key())
        {
        case Qt::Key_A :
        {
            m_keyData.leftPresed = false;
            break;
        }
        case Qt::Key_D :
        {
            m_keyData.rightPresed = false;
            break;
        }
        case Qt::Key_W :
        {
            m_keyData.upPressed = false;
            break;
        }
        case Qt::Key_E :
        {
            m_keyData.ePressed = false;
            break;
        }
        case Qt::Key_S :
        {
            m_keyData.downPresed = false;
            break;
        }
        case Qt::Key_Space :
        {
            m_keyData.spacePressed = false;            
            break;
        }
        case Qt::Key_Enter :
        {
            m_keyData.enterPressed = false;
            break;
        }
        case Qt::Key_Shift :
        {
            selectNextAmmo();
            break;
        }
        }
    }


    event->accept();
}

void Player::updateDirections()
{
    m_oldDirection = m_direction;
    m_direction = Direction::stand;
    if (m_keyData.rightPresed)
    {
        m_rightWatching = true;
        m_sprite.setFlipping(-1,1);
        if (m_keyData.upPressed)
        {
            m_direction = Direction::rightUp;
            return;
        }
        if (m_keyData.downPresed)
        {
            m_direction = Direction::rightDown;
            return;
        }
        m_direction = Direction::right;
        return;
    }
    if (m_keyData.leftPresed)
    {
        m_rightWatching = false;
        m_sprite.setFlipping(1,1);
        if (m_keyData.upPressed)
        {
            m_direction = Direction::leftUp;
            return;
        }
        if (m_keyData.downPresed)
        {
            m_direction = Direction::leftDown;
            return;
        }
        m_direction = Direction::left;
        return;
    }

    if (m_keyData.upPressed)
    {
        m_direction = Direction::up;
        return;
    }
    if (m_keyData.downPresed)
    {
        m_direction = Direction::down;
        return;
    }
}

void Player::addAmmo(type::bullets t, int count)
{
    m_ammo[t] += count;
}

void Player::selectNextAmmo()
{
    m_currentAmmo = type::bullets((m_currentAmmo+1)%type::BULLET_TYPE_COUNT);

    if (m_ammo[m_currentAmmo] < 1)
    {
        int visited = 0;
        while (m_ammo[m_currentAmmo]<1 && visited < type::BULLET_TYPE_COUNT)
        {
            m_currentAmmo = type::bullets((m_currentAmmo+1)%type::BULLET_TYPE_COUNT);
            ++visited;
        }
        if (visited >= type::BULLET_TYPE_COUNT) m_currentAmmo = m_defaultAmmo;
    }
}


void Player::advance(int timeDelta)
{
    m_timer+=timeDelta;
    m_propertyModifierManager.advance(timeDelta);

    if (m_state == catched)
    {
        catchAdvance();
        return;
    }

    if (m_keyData.ePressed)
    {
        emitBullet();
        m_keyData.ePressed = false;
    }

    m_jumpNow = m_keyData.spacePressed;

    m_speed = body->GetPosition() - m_lastPos;
    m_lastPos = body->GetPosition();
    m_lastDamageTimeElapsed += timeDelta;

    updateDirections();
    moveAdvance();
    updateJump();

    body->SetLinearDamping(m_damping);
    m_x = body->GetPosition().x;
    m_y = body->GetPosition().y;
}

void Player::catchAdvance()
{
    m_propertyModifierManager.processLevelFailed();
    emit finishLevel(catchedByGribbo);
    m_state = normal;
}


void Player::glDraw(GlUtils* glUtils)
{
    if (!m_visible) return;

    if (m_lastDamageTimeElapsed < m_damageInterval)    
          sprite()->setOpacity(0.5+(sin(m_timer/32.0))/2.0);
     else sprite()->setOpacity(1.0);

    m_shadowSprite.setPos(body->GetPosition());
    m_shadowSprite.draw(glUtils);

    if (m_inJump || m_falling)
    {
        m_sprite.setOffset(0, -m_ySpriteOffset - m_jumpHeight);
    }else
    {
        m_sprite.setOffset(0, -m_ySpriteOffset);
    }
    m_sprite.setPos(body->GetPosition());
    m_sprite.draw(glUtils);
}



bool Player::processDamage(PropertyModifier* modifier)
{   
    bool result = false;
    if (m_lastDamageTimeElapsed > m_damageInterval)
    {
        result = true;
        m_propertyModifierManager.addModifier(modifier);
        m_lastDamageTimeElapsed = 0;
    }else
    {
        m_visible = true;
    }

    if (m_stat.health() < 1)
    {
        m_stat.setLives(m_stat.lives()-1);
        qDebug()<<"die";    
        emit finishLevel(unsucces);
    }
    return result;
}

void Player::emitBullet()
{
    if (m_timer - m_lastBulletTime < 1000) return;

    if (m_ammo[m_currentAmmo] > 0)
    {
        Bullet* b = new Bullet(m_currentAmmo, body->GetWorld());
        m_ammo[m_currentAmmo] = m_ammo[m_currentAmmo]-1;
        if (m_ammo[m_currentAmmo] < 1)
        {
            if (m_currentAmmo == defaultAmmo())
            {
                m_ammo[m_currentAmmo] = 999;
            }
            else selectNextAmmo();
        }

        switch (m_direction) {
        case Direction::up:
        case Direction::down:
            b->setDirection(m_direction);
            b->body->SetTransform(body->GetPosition()-b2Vec2(0, 0.5), 0);
            break;
        default:
            if (m_rightWatching)
            {
                b->setDirection(Direction::right);                
                b->setPos(body->GetPosition()-b2Vec2(-0.5, 0.1), -m_height/3);

            }
            else
            {
                b->setDirection(Direction::left);
                b->setPos(body->GetPosition()-b2Vec2(0.5,  0.1), -m_height/3);

            }
            break;
        }
        b->setTarget(Bullet::EnemyTarget);
        m_gamedata->addBullet(b);
        m_lastBulletTime = m_timer;
    }
}






