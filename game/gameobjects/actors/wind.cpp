#include "wind.h"
#include <QJsonObject>

Wind::Wind()
{

}


Wind::Wind(QJsonObject &parameters,
           b2World* world) : PhysicsObject(parameters, world)
{
    //m_id = 5;
    m_physicsType = b2_staticBody;
    createPhysics(world);
    visibleObjectRect = QRectF(m_x, m_y, m_width, m_height);
}

Wind::~Wind()
{

}
