#version 130
uniform sampler2D texture0;
uniform sampler2D ripples;
uniform sampler2D stencil;
varying vec2 v_texcoord;
uniform vec2 offset;


void main()
{
    vec4 ripple = texture2D(ripples,  v_texcoord + (offset/2.0));
    vec4 phase  = texture2D(stencil,  v_texcoord +  offset     );
    vec4 color  = texture2D(texture0, v_texcoord);        
    gl_FragColor = color + (ripple * phase)*ripple.a;
    gl_FragColor.a  =  color.a * (phase.a+ripple.a);
}
