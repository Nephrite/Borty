#ifndef MOVINGPLATFORM_H
#define MOVINGPLATFORM_H

#include <game/gameobjects/basegameobject.h>
#include <game/gameobjects/physicsobject.h>




class MovingPlatform : public PhysicsObject
{
    Q_OBJECT

    Q_PROPERTY_WITH_ACCESSORS(double, velocity,  setVelocity )
    Q_PROPERTY_WITH_ACCESSORS(double, xDistance, setXDistance)
    Q_PROPERTY_WITH_ACCESSORS(double, yDistance, setYDistance)

public:

    MovingPlatform();
    MovingPlatform(QJsonObject& parameters, b2World* world);
    void advance(int timeDelta);

private:

    bool m_forward;
    float linearSpeed;
    b2Vec2 m_start;
    b2Vec2 m_targetPosition;
};

#endif // MOVINGPLATFORM_H
