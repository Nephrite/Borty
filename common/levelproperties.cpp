#include "levelproperties.h"
#include <QDebug>

LevelProperties::LevelProperties() :
    m_ambientLight(Qt::white),
    m_backColor(Qt::black),
    m_posterFile("default.png")
{
}

LevelProperties::LevelProperties(QVariantMap *m):
    m_ambientLight(Qt::white),
    m_backColor(Qt::black),
    m_posterFile("default.png")
{
    setup(m);
}

void LevelProperties::setup(QVariantMap *properties)
{
    m_ambientLight =(properties->value("ambientColor")).value<QColor>();
    m_backColor = properties->value("backColor").value<QColor>();
    m_title = properties->value("title").value<QString>();
    m_description = properties->value("description").value<QString>();
    m_defaultAmmo = properties->value("defaultAmmo").value<QString>();
    m_posterFile = properties->value("posterFile").value<QString>();
}

QVariantMap LevelProperties::toMap()
{
    QVariantMap map;
    map.insert("ambientColor", ambientLight());
    map.insert("backColor", backColor());
    map.insert("description", description());
    map.insert("title", title());
    map.insert("defaultAmmo", defaultAmmo());
    map.insert("posterFile", posterFile());
    return map;
}
