#ifndef HEADUP_H
#define HEADUP_H

#include <QFont>

#include "game/gameobjects/player.h"

#include <QPixmap>
#include <QPainter>

#include <QHBoxLayout>
#include <QLabel>

class IconLabel : public QWidget
{
public:

    IconLabel(QWidget* parent);
    void setText(QString& text){textLabel->setText(text);}
    void setText(QString  text){textLabel->setText(text);}
    void setPixmap(QPixmap& pixmap){iconLabel->setPixmap(pixmap);}
    void setPixmap(QPixmap  pixmap){iconLabel->setPixmap(pixmap);}
    void setFont(QFont& font){iconLabel->setFont(font);
                              textLabel->setFont(font);
                              QWidget::setFont(font); }
    //void paintEvent(QPaintEvent* event);

protected:

    QHBoxLayout* layout;
    QLabel* textLabel;
    QLabel* iconLabel;

};



class QGLWidget;

class HeadUp : public QWidget
{
public:
    HeadUp(QWidget* parent);
    ~HeadUp();
    void setPlayer(Player *);
    void updateData();
private:
    int timer;
    void loadIcons();
    void updateAmmo();
    void updateSelection();
    void cleanup();

    Player* m_player;
    QPixmap m_bottlePix;
    QPixmap m_coinPix;
    QRect m_heart;
    QRect m_bottleRect;
    QRect m_bottleRegion;
    QFont m_font;
    QString text;

    int m_playerHealth;
    // gui
    type::bullets m_selected;
    QVector<IconLabel*> m_ammoLabels;
    QVector<int>* m_playerAmmo;
    QHBoxLayout* m_layout;
    QHBoxLayout* m_ammoLayout;
    QLabel* m_bottleLabel;
    QLabel* m_coinIconLabel;
    QLabel* m_coinCountLabel;
};

#endif // HEADUP_H
