#ifndef OBJECT_H
#define OBJECT_H

#include <QObject>

#define PROPERTY(type, name, setter)   \
    protected: type m_##name;                           \
    public: Q_PROPERTY(type name READ name WRITE setter USER true DESIGNABLE true)\
    type const& name () const { return m_##name; }      \
    void setter (type const &v) { m_##name = v; }



#define PROMOTED_PROPERTY(realtype, type, name, setter)   \
    protected: realtype m_##name;                          \
    public: Q_PROPERTY(type name READ name##type WRITE setter##type USER true DESIGNABLE true)\
    realtype const& name() const { return m_##name; }      \
    type name##type() const { return (type)m_##name; }      \
    void setter (realtype const &v) { m_##name = v; }                \
    void setter##type (type const &v) { m_##name = (realtype)v; }


class Object : public QObject
{     
    Q_OBJECT

    PROPERTY(int, test, setTest)
public:

    explicit Object(QObject *parent = nullptr);
    explicit Object(const Object &other);
    static Object* create(QJsonObject& object);
    static Object* toObject(QObject*);
    static const QMetaObject* metaObjectOf(QString& type);   
    static const QMetaObject* metaObjectOf(const char* type);

    virtual void setup(QJsonObject& object);

    virtual void setupProperties(QJsonObject& object);
    virtual QString toString();
    virtual QVariantMap toVariantMap();
    static QVariantMap toVariantMap(const Object *);

    virtual QJsonObject toJsonObject();
    static  QJsonObject toJsonObject(const Object*);

    virtual void setProperty(const char *name, const QVariant &value);
    virtual void setProperty(const QString name, const QVariant &value);

    template<typename T>
    T as() { return qobject_cast<T>(this); }

signals:

public slots:

protected:
    virtual void setupMembers(QJsonObject& object);
    virtual void setupChildMember(QJsonObject& object);


};

Q_DECLARE_METATYPE(Object)
#endif // OBJECT_H
