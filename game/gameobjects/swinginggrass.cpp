#include "swinginggrass.h"

SwingingGrass::SwingingGrass()
{

}

SwingingGrass::SwingingGrass(QJsonObject &parameters): BaseGameObject(parameters)
{
    m_sprite.setShaderName("swingingGrass");
    m_sprite.shaderArgs()->arg1 = (GLfloat)m_height;
    m_sprite.shaderArgs()->arg2 = (GLfloat)(rand()%10);
    m_sprite.shaderArgs()->arg3 = (GLfloat)(rand()%10);
}

SwingingGrass::~SwingingGrass()
{

}

void SwingingGrass::advance(int timedelta)
{
    m_sprite.shaderArgs()->time += timedelta;
}
