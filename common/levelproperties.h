#ifndef LEVELPROPERTIES_H
#define LEVELPROPERTIES_H

#include <QString>
#include <QColor>
#include "common/serializable.h"
class LevelProperties : public QObject
{
    Q_OBJECT

    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS ( QString, description,       setDescription )
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS ( QString, title,             setTitle       )
    Q_PROPERTY(QColor ambientLight READ ambientLight WRITE setAmbientLight DESIGNABLE true USER true)
    Q_PROPERTY(QColor backColor READ backColor WRITE setBackColor DESIGNABLE true USER true)
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS(QString, defaultAmmo,  setDefaultAmmo )
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS(QString, posterFile,  setPosterFile )

public:
    LevelProperties();
    LevelProperties(QVariantMap*);

    void setup(QVariantMap* properties);
    QVariantMap toMap();

    void setAmbientLight(QColor v){m_ambientLight = v;}
    void setBackColor( QColor v){m_backColor = v;}
    QColor ambientLight(){return m_ambientLight;}
    QColor backColor(){return m_backColor;}
protected:
    QColor m_ambientLight;
    QColor m_backColor;
};

#endif // LEVELPROPERTIES_H
