﻿#ifndef GAMESCENE_H
#define GAMESCENE_H

#include <QFile>
#include <QObject>
#include <QList>

#include "objectscreator.h"
#include "game/gameobjects/player.h"
#include "game/gameobjects/gamecontactlistener.h"
#include "common/levelproperties.h"
#include "gamedata.h"
#include "camera.h"

class QKeyEvent;
class HeadUp;
class b2World;
class BaseGameObject;
class QGLContext;

struct ObjectsLists{
    ObjectsLists(){}
    ~ObjectsLists(){}
    QList<BaseGameObject*> objects;
    QList<BaseGameObject*> backgrounds;
    QList<BaseGameObject*> lights;
    QList<BaseGameObject*> bulletList;
};


class GameScene : public QObject
{
    Q_OBJECT
public:
    GameScene();
    ~GameScene();
    void updateViewport(QSize size);
    void loadLevel(QString);
    bool levelLoaded();
    b2World* getWorld();
    void keyPress(QKeyEvent*);
    void keyRelease(QKeyEvent*);
    void advance(int timeDelta);   
    ObjectsLists* objectList(){return &m_objectsList;}
    LevelProperties* properties();
    HeadUp* headUp(){return p_headUp;}
    Player* player(){return m_player;}
    QRectF viewport();
    QPointF playerPos();
    void clearScene();   
    Player* m_player;

public slots:

    void deleteObject(BaseGameObject*);
    void addObject(BaseGameObject* object);
    void deleteBullet(BaseGameObject *bullet);
    void onPlayerFinishedLevel(Player::finishReason reason);

signals:

    void finishLevel(Player::finishReason reason);

protected:


    GameData m_gameData;
    void updateGameData();
    void registerBullets();
    QList<BaseGameObject*> m_forDeleting;
    ObjectsLists m_objectsList;
    void createWorld();
    void destroyWorld();
    ObjectsCreator m_objectsCreator;
    b2World* p_world;
    LevelProperties* m_levelProperies;

    QFile m_levelFile;
    QString m_levelMap;
    QRectF m_view;
    void createPlayer();
    int m_scale;
    bool m_loaded;
    bool m_finished;
    Player::finishReason m_finishReason;
    HeadUp* p_headUp;
    GameContactListener *m_contactListener;
    void deletePendingObjects();
    Camera m_camera;
    b2Vec2 m_lastPlayerPos;
};



#endif // GAMESCENE_H
