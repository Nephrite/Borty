#include "actor.h"

Actor::Actor()
{

}

Actor::Actor(QJsonObject& properties, b2World *world)
    : PhysicsObject(properties, world, false),
      m_gamedata(nullptr),
      m_moveVec(b2Vec2(0,0)),
      m_direction(Direction::stand),
      m_oldDirection(Direction::stand),
      m_maxVelocity(5),
      m_inJump(false),
      m_canJump(true),
      m_falling(false),
      m_damageInterval(2000),
      m_lastDamageTimeElapsed(0)
{   
    setup(properties);
    createPhysics(world);
    m_maxJumpHeight = 2.0;

    m_sprite.setOffset(0, -m_ySpriteOffset);
}

Actor::Actor(QJsonObject &properties, b2World *world, bool autoCreatePhysics)
    : PhysicsObject(properties, world, false),
      m_gamedata(nullptr),
      m_moveVec(b2Vec2(0,0)),
      m_direction(Direction::stand),
      m_oldDirection(Direction::stand),
      m_maxVelocity(5),
      m_inJump(false),
      m_canJump(true),
      m_falling(false),
      m_damageInterval(2000),
      m_lastDamageTimeElapsed(0)
{
    setup(properties);
    if (autoCreatePhysics) createPhysics(world);
}

Actor::~Actor()
{

}


Direction::directions Actor::direction()
{
    return m_direction;
}

void Actor::setGameData(GameData *gameData)
{
    m_gamedata = gameData;
}

void Actor::createPhysics(b2World *world)
{
    ///////////////////////////////////
    //create Body
    bodyDef.type = b2BodyType(b2_dynamicBody);
    bodyDef.position.Set(m_x, m_y);
    body = world->CreateBody(&bodyDef);
    body->SetBullet(false);

    ///////////////////////////////////
    // Main Fixture -- Sensor

    fixtureDef.density =1;
    fixtureDef.friction = 0.1;
    fixtureDef.restitution = 0;
    body->SetUserData(this);
    m_ySpriteOffset = m_height/2-m_width/4;
    shape.SetAsBox(m_width/4, m_height/3,b2Vec2(0, -m_ySpriteOffset),0);
    fixtureDef.shape = &shape;


    fixtureDef.filter.categoryBits = PhysicsLayer::Sensor;
    fixtureDef.filter.maskBits =  PhysicsLayer::BulletEnemyTarget;
    body->CreateFixture(&fixtureDef);

    shadowDef.filter.categoryBits = PhysicsLayer::Collider;
    shadowDef.filter.maskBits = PhysicsLayer::Collider;

    shadowShape.SetAsBox(m_width/3, m_width/6);
    shadowDef.shape = &shadowShape;
    body->CreateFixture(&shadowDef);
    body->SetFixedRotation(true);
}

void Actor::disableCollisions()
{
    b2Filter filter;
    filter.categoryBits = 0;
    filter.maskBits = 0;
    b2Fixture* fix = body->GetFixtureList();
    while(fix)
    {
        fix->SetFilterData(filter);
        fix = fix->GetNext();
    }
    collisionList.clear();
}

void Actor::setupShadowSprite()
{
    m_shadowSprite.setAsBox(m_width, m_width/2);
    m_shadowSprite.addTexture("./resources/playerShadow.png", QOpenGLTexture::Linear);
    m_shadowSprite.setPos(body->GetPosition());   
    m_shadowSprite.setAngle(m_rotation);
    m_shadowSprite.setZvalue(m_zValue);   
}

double Actor::getMaxVelocity() const
{
    return m_maxVelocity;
}

void Actor::setMaxVelocity(double value)
{
    m_maxVelocity = value;
}

void Actor::glDraw(GlUtils *glutils)
{
    if (!visible()) return;
    m_sprite.setPos(body->GetPosition());

    if (m_inJump || m_falling)
    {
        m_shadowSprite.setOpacity(m_maxJumpHeight/m_jumpHeight);
        m_sprite.setOffset(0, -m_ySpriteOffset - m_jumpHeight);
    }else
    {
        m_shadowSprite.setOpacity(1);
        m_sprite.setOffset(0, -m_ySpriteOffset);
    }
    m_shadowSprite.setPos(body->GetPosition());

    m_shadowSprite.draw(glutils);

    if (Direction::isLeft(m_direction))
    {
        m_sprite.setFlipping(1.0, 1.0);
    }
    else if (Direction::isRight(m_direction))
    {
        m_sprite.setFlipping(-1.0, 1.0);
    }

    m_sprite.draw(glutils);
}

double Actor::bottomY()
{
    return body->GetPosition().y-m_width/6;
}

void Actor::moveAdvance()
{
    if (body->GetLinearVelocity().Length() < m_maxVelocity && m_direction != Direction::stand)
    {
        m_moveVec = Direction::dirToVector(m_direction);
        m_moveVec *= m_maxVelocity;
        applyImpulse(m_moveVec);
    }
}


void Actor::updateJump()
{
    if (!m_inJump && !m_falling && m_jumpNow)
    {
        m_jumpNow = false;
        m_falling = false;
        m_inJump = true;
        m_jumpHeight = 0;
        m_maxJumpHeight = 2.0;

    }

    if (!m_falling && m_inJump && m_jumpHeight < m_maxJumpHeight )
    {
        m_jumpHeight += 0.06;
    }
    if (m_jumpHeight > 2.0)
    {
        m_falling = true;
    }
    if (m_falling)
    {
        m_jumpHeight -= 0.06;
    }

    if (m_jumpHeight < 0.01 )
    {
        m_jumpHeight = 0;
        m_inJump = false;
        m_falling = false;
    }
}

void Actor::applyImpulse(b2Vec2 v)
{
     body->ApplyLinearImpulse(v, body->GetLocalCenter());
}

void Actor::applyImpulse(double x, double y)
{
    body->ApplyLinearImpulse(b2Vec2(x, y), body->GetLocalCenter());
}
