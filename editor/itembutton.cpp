#include "itembutton.h"
#include "QMouseEvent"
#include "QDebug"

#include <QLayout>
#include <QAbstractButton>
#include <common/objecttypes.h>
ItemButton::ItemButton()
{

}

ItemButton::ItemButton(QJsonObject object)
{

    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    setContentsMargins(10,10,10,10);
    setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    _properties = object;
    QMetaEnum metaEnum = QMetaEnum::fromType<type::types>();
    int objtype = object.value("objectType").toInt();
    QString text = QString("%1 x %2 \n%3 \n%4/%5")
            .arg(object.value("width").toInt())
            .arg(object.value("height").toInt())
            .arg(metaEnum.valueToKey(objtype))
            .arg(object.value("textureStyle").toString())
            .arg(object.value("texture").toString())
                 ;
    setText(text);
    m_objectsStyle = object.value("textureStyle").toString();
    setIconSize(QSize(64,64));
  //  setStyleSheet ("QWidget{border:none; text-align: left; background: #adf; color: black; padding:10px; margin-bottom:10px;}"
  //                 "QWidget:hover{background: #bef;} "
  //                 "QWidget:pressed{background: #cff;}");


}



ItemButton::~ItemButton()
{

}

QString ItemButton::objectStyle()
{
    return m_objectsStyle;
}

void ItemButton::setIconFile(QString file)
{
    icon.load(file);
    setIcon(icon);
}


QJsonObject ItemButton::properties()
{
    return _properties;
}

void ItemButton::mouseReleaseEvent(QMouseEvent *e)
{
    if (e->button() == Qt::LeftButton)
    {
        emit sendProperies(&_properties);
    }
    QToolButton::mouseReleaseEvent(e);
}
