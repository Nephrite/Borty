#ifndef NPCITEM_H
#define NPCITEM_H

#include <QObject>
#include "common/serializable.h"
#include "editor/items/baseitem.h"

class NPCItem : public BaseItem
{
    Q_OBJECT

public:
    NPCItem(QGraphicsItem *parent = 0);
    NPCItem(QVariantMap *newProperties);
    ~NPCItem();

    enum ActivateOnEvent{
        PlayerTouch,
        OnScreenEnter,
        OnCreate
    };
    Q_ENUMS(ActivateOnEvent)

    enum Behaviour{
        Standing,
        CircleWalking,
        LinearWalking,
        FollowPlayer
    };
    Q_ENUMS(Behaviour)
    enum directions{left,
                    leftUp,
                    up,
                    rightUp,
                    right,
                    rightDown,
                    down,
                    leftDown,
                    stand
                   };
    Q_ENUMS(directions)

    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS(ActivateOnEvent, activateEvent, setActivateEvent)
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS(Behaviour,       behaviour,     setBehaviour)
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS(int,             distance,      setDistance)
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS(directions,      initDirection, setInitDirection)
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS(QString,         text,          setText)
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS(QString,         propertyName,  setPropertyName)
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS(int,             value,         setValue)
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS(bool,            singleAction,  setSingleAction)

};

#endif // NPCITEM_H
