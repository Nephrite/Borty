#ifndef DECOROBJECT_H
#define DECOROBJECT_H

#include "game/gameobjects/basegameobject.h"
#include <QJsonObject>

class decorObject : public BaseGameObject
{
    Q_OBJECT
public:
    decorObject(QJsonObject& parameters);
    decorObject();
    ~decorObject();
    QPointF visiblePos();    
};

#endif // DECOROBJECT_H
