#ifndef GLESGAMEVIEW_H
#define GLESGAMEVIEW_H

#include "glesdrawable.h"
//#include "3rdParty/Box2D/Box2D.h"
#include "game/gamescene.h"
#include "game/headup.h"

#include <QObject>
#include <QOpenGLFunctions>
#include <QOpenGLWidget>
#include <QOpenGLShader>
#include <QOpenGLFramebufferObject>
#include <QKeyEvent>

class GlesGameView :  public QOpenGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT
public:

    enum fadeStates{fade, unfade, nofade};

    GlesGameView();
    void setScene(GameScene*);
    HeadUp* headUp(){return m_headup;}

    void resizeGL(int w, int h);
    void paintGL();  

    void startFading();
    void startUnfading();

    void keyPressEvent(QKeyEvent*);
    void keyReleaseEvent(QKeyEvent*);   
    QImage* toImage();

signals:

    void pauseGame();

protected:
    void initializeGL();
    void enableGL();
    HeadUp* m_headup;
    QPixmap m_headupPixmap;

    Drawable screenSprite;
    GlUtils m_glUtils;
    bool firsttime=false;

    fadeStates m_fadeState;
    GLfloat m_fadeValue;

    QOpenGLShaderProgram* screenShader;
    QOpenGLFramebufferObject* backGroundBuffer;
    QOpenGLFramebufferObject* foreGroundBuffer;
    QOpenGLFramebufferObject* lightBuffer;

    b2World* world;
    GameScene* scene;
    QRectF viewport;

    float m_scale;
    void draw();
    GLfloat h;
    GLfloat w;
    bool wait;
    QImage m_image;


};

#endif // GLESGAMEVIEW_H
