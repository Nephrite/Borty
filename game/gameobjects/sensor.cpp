#include "sensor.h"
#include "player.h"
#include "common/objecttypes.h"
#include "Box2D/Dynamics/Joints/b2WeldJoint.h"

#include <QDebug>

Sensor::Sensor(b2World* world, int x, int y)
{
    m_physicsType = b2_dynamicBody;
    m_width = 0.5;
    m_height = 1;
    m_x=x;
    m_y=y;

    bodyDef.type = b2BodyType(m_physicsType);
    bodyDef.position.Set(m_x, m_y);
    body = world->CreateBody(&bodyDef);
    shape.SetAsBox(m_width/2.0, m_height/2.0);
    fixtureDef.density = 1.0;
    fixtureDef.friction = 0.1;
    fixtureDef.restitution = 0;

    body->SetUserData(this);
    fixtureDef.shape = &shape;
    body->CreateFixture(&fixtureDef);
    m_contactCount = 0;
}

bool Sensor::onGround()
{    
    return m_contactCount > 0;
}

void Sensor::preSolve(PhysicsObject* o, b2Contact * contact)
{
    Q_UNUSED(o);    
    contact->SetEnabled(false);
}
void Sensor::solveCollision(PhysicsObject* o)
{   
    Q_UNUSED(o);
    if (o->objectType() != type::Player) ++m_contactCount;
}

void Sensor::endContact(PhysicsObject* o)
{   
    Q_UNUSED(o);
    if (o->objectType() != type::Player) --m_contactCount;
}
