#include "worldmap.h"
#include "ui_worldmap.h"
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QKeyEvent>
#include <QDebug>
#include <QFile>
#include <QPainter>
#include <QFontDatabase>

#include <common/save.h>

WorldMap::WorldMap(QWidget *parent) :
    QWidget(parent),    
    m_currentLevelIdx(0),
    m_dialogReplyIdx(-1),
    m_state(MapSelect),
    m_confirm(false),
    ui(new Ui::WorldMap)
{
    ui->setupUi(this);

    int id = QFontDatabase::GeneralFont;
    QString family = QFontDatabase::applicationFontFamilies(id).at(0);
    QFont font(family);
    font.setLetterSpacing(QFont::AbsoluteSpacing, 1.5);
    setFont(font);
    font.setPixelSize(24);
    ui->textLabel->setFont(font);
    ui->textLabel->setWordWrap(true);

    ui->BortyLabel->setPixmap(QPixmap("./resources/ui/borty.png"));
    ui->companionLabel->setPixmap(QPixmap("./resources/ui/enen.png"));

    m_background.load("./resources/ui/mapBack.png");
    QFile stylesheet("./resources/mapstyle.qss");
    if (stylesheet.open(QFile::ReadOnly))
    {
        setStyleSheet(stylesheet.readAll());
    }

    loadLevels();
}

WorldMap::~WorldMap()
{
    delete ui;
}

void WorldMap::paintEvent(QPaintEvent *e)
{

    QPainter paint(this);
    paint.drawPixmap(0,
                     0,
                     m_background.scaled(
                         width(),
                         height(),
                         Qt::KeepAspectRatioByExpanding
                         )
                     );
    QWidget::paintEvent(e);
}

void WorldMap::keyPressEvent(QKeyEvent *e)
{    
    switch (m_state)
    {
    case MapSelect:
    {
        switch (e->key())
        {
        case Qt::Key_S:
        case Qt::Key_A:

            selectPrevious();
            break;

        case Qt::Key_W:
        case Qt::Key_D:

            selectNext();
            break;

        case Qt::Key_Return:
        case Qt::Key_Enter:

            m_state = Dialog;
            m_dialogReplyIdx = -1;
            showNextDialogReply();
            break;

        case Qt::Key_Escape:
            emit goBack();
        default: break;
        }
        break;
    }
    case Dialog :
    {
        switch (e->key())
        {
        case Qt::Key_W:
        case Qt::Key_D:
        case Qt::Key_Return:
        case Qt::Key_Enter:
        case Qt::Key_Space:

            showNextDialogReply();
            break;

        case Qt::Key_Escape:
        case Qt::Key_S:
        case Qt::Key_A:

            m_state = MapSelect;
            m_dialogReplyIdx = -1;
            updateUi();
            break;

        default: break;
        }
        break;
    }
    case Confirm:
    {
        switch (e->key())
        {
        case Qt::Key_W:
        case Qt::Key_D:
        case Qt::Key_S:
        case Qt::Key_A:
        case Qt::Key_Space:
            m_confirm = !m_confirm;
            showConfirm();
            break;
        case Qt::Key_Enter:
        case Qt::Key_Return:
            if (m_confirm)
            {
                QString file = m_levels[m_currentLevelIdx]
                        .toObject().value("file").toString();
                if (file.isEmpty()) break;
                emit selectLevel(file);
            }
            else
            {
                m_state = MapSelect;
                updateUi();
            }
            break;
        case Qt::Key_Escape:
            m_state = MapSelect;
            updateUi();
            break;
        default: break;
        } // switch key
        break;
    }    // case confirm
    default: break;
    }   // switch state
}

void WorldMap::reset()
{
    m_currentLevelIdx = 0;
    m_dialogReplyIdx = 0;
    m_confirm = false;
}

void WorldMap::showEvent(QShowEvent *e)
{
    ui->mapwidget->clear();
    reset();
    loadLevels();
    QWidget::showEvent(e);
}


void WorldMap::showNextDialogReply()
{
    ++m_dialogReplyIdx;
    if (m_dialogReplyIdx > 0 || m_dialogReplyIdx < m_dialog.count())
    {
        QString text("<p style='color:%3'><b>%0:</b> %1<p> ");
        if (m_dialogReplyIdx % 2 == 0) //реплика компаньона
        {
            ui->textLabel->setText(text.arg(m_companion.value("name").toString())
                                   .arg(m_dialog.at(m_dialogReplyIdx).toString())
                                   .arg(m_companion.value("textcolor").toString())

                                   );
        }
        else // реплика Бортии
        {
            ui->textLabel->setText(text.arg("Бортия").
                                   arg(m_dialog.at(m_dialogReplyIdx).toString())
                                   .arg("#3d972d")
                                   );
        }
    }
    if (m_dialogReplyIdx == m_dialog.count())
    {
        m_dialogReplyIdx = -1;
        if (!m_currentLevel.value("file").toString().isEmpty())
        {
            m_state = Confirm;
            showConfirm();
        }
        else
        {
            m_state = MapSelect;
            selectNext();
        }
    }
}

void WorldMap::showConfirm()
{
    QString text("<h4>Выбираем: %0</h4>"
                 "%1   %2");
    if (m_confirm)
    {
        ui->textLabel->setText(text.arg(m_currentLevel.value("title").toString())                              
                               .arg("<a style='color:#444444'>Нет</a>")
                               .arg("<a style='color:#119911'>*Да*</a>")
                               );
    }
    else
    {
        ui->textLabel->setText(text.arg(m_currentLevel.value("title").toString())                              
                               .arg("<a style='color:#991111'>*Нет*</a>")
                               .arg("<a style='color:#444444'> Да </a>")
                               );
    }
}

void WorldMap::selectNext()
{
    if (++m_currentLevelIdx == m_levels.count())
    {
        m_currentLevelIdx = 0;
    }
    m_currentLevel = m_levels[m_currentLevelIdx].toObject();
    ui->mapwidget->selectNext();
    updateUi();
}

void WorldMap::selectPrevious()
{
    if (--m_currentLevelIdx == -1)
    {
        m_currentLevelIdx = m_levels.count()-1;
    }
    m_currentLevel = m_levels[m_currentLevelIdx].toObject();
    ui->mapwidget->selectPrevious();
    updateUi();
}

void WorldMap::loadLevels()
{
    QFile file("./resources/levels/levels.json");
    if (!file.open(QFile::ReadOnly))
    {
        qDebug()<< "WorldMap: can't open ./resources/levels/levels.json";
        return;
    }
    QJsonDocument doc = QJsonDocument::fromJson(file.readAll());
    m_levels = doc.object().value("levels").toArray();
    for (const QJsonValue & level : m_levels)
    {
        QPoint p;
        p.setX(level.toObject().value("x").toInt());
        p.setY(level.toObject().value("y").toInt());
        QString file = level.toObject().value("file").toString();
        ui->mapwidget->addPoint(p, Save::instance()->levelIsFinished(file));
    }
    m_currentLevel = m_levels.first().toObject();
    updateUi();
    m_currentLevelIdx = 0;
}

void WorldMap::updateUi()
{
    QString text("<h4> %0 </h4>"
                 " <p style='color:%3'><b>%1</b>: %2</p>"
                 );
    m_companion = m_currentLevel.value("companion").toObject();
    ui->textLabel->setText(text.arg(m_currentLevel.value("title").toString())
                               .arg(m_companion.value("name").toString())
                               .arg(m_currentLevel.value("description").toString())
                               .arg(m_companion.value("textcolor").toString())
                           );
    ui->companionLabel->setPixmap(QPixmap("./resources/ui/"+m_companion.value("photo").toString()));
    m_dialog = m_currentLevel.value("dialog").toArray();
}
