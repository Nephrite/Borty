#include "billboardphysicsobject.h"
#include "game/gameobjects/physicsobject.h"



BillboardPhysicsObject::BillboardPhysicsObject(QJsonObject &parameters, b2World *world) : PhysicsObject(parameters, world, false)
{
    setup(parameters);
    m_size = QSizeF(m_width, m_height);
    m_visible = true;
    m_propertyModifier = nullptr;

    m_yOffset = (m_height/2 -m_physicsHeight/2) ;

    createPhysics(world);

    setupSprite();
    m_sprite.setOffset(0, -(m_yOffset));
}

BillboardPhysicsObject::~BillboardPhysicsObject()
{

}

double BillboardPhysicsObject::bottomY()
{
    return body->GetPosition().y + m_physicsHeight/2;
}

void BillboardPhysicsObject::createPhysics(b2World* world)
{
    bodyDef.type = b2BodyType(physicsType());
    bodyDef.position.Set(m_x, m_y+m_yOffset);

    body = world->CreateBody(&bodyDef);
    body->SetBullet(false);

    shape.SetAsBox(m_physicsWidth/2.0, m_physicsHeight/2.0);
    b2Filter filter;
    filter.categoryBits = PhysicsLayer::Collider;
    filter.maskBits = PhysicsLayer::Collider;
    fixtureDef.filter = filter;
    fixtureDef.density =1;
    fixtureDef.friction = 0.1;
    fixtureDef.restitution = 0;
    body->SetUserData(this);
    fixtureDef.shape = &shape;
    body->CreateFixture(&fixtureDef);
    body->SetFixedRotation(m_fixedRotation);
    body->SetTransform(body->GetPosition(), m_rotation*(180.0/M_PI));
}
