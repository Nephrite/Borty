#ifndef GRIBBOITEM_H
#define GRIBBOITEM_H

#include "editor/items/baseitem.h"

#include <QObject>

class GribboItem : public BaseItem
{
    Q_OBJECT

public:

    enum GribboType
    {
        simple, withBag, withFlute
    };
    Q_ENUMS(GribboType)

    Q_PROPERTY( GribboType gribboType
                READ gribboType
                WRITE setGribboType
                DESIGNABLE true
                USER true)


    explicit GribboItem(QGraphicsItem *parent = 0);
    explicit GribboItem(QVariantMap *newProperties);
     ~GribboItem();
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);


    GribboType gribboType(){return m_gribboType;}
    void setGribboType(GribboType t){m_gribboType = t; updateGribboType();}

protected slots:
    void updateGribboType();

protected:
    void paintObject(QPainter *painter);
    GribboType m_gribboType;
};

#endif // GRIBBOITEM_H
