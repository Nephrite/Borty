#ifndef DIRECTIONS_H
#define DIRECTIONS_H

#include "Box2D/Box2D.h"
#include <QVector>

class Direction
{
public:
    Direction();
    enum directions{left,
                    leftUp,
                    up,
                    rightUp,
                    right,
                    rightDown,
                    down,
                    leftDown,
                    stand
                   };
     static Direction::directions vecToDir(b2Vec2 vec);
     static  Direction::directions random(){
         return directions(rand()%stand);
     }
     static  b2Vec2 dirToVector(Direction::directions dir){
         return directionVectors[int(dir)];
     }
     static  Direction::directions next(Direction::directions dir){
         return nextDir[int(dir)];
     }
     static Direction::directions invert(Direction::directions dir)
     {
         return(Direction::directions((dir+4)%8));
     }
     static bool isLeft(Direction::directions dir){
         return dir == left || dir == leftDown || dir == leftUp;
     }
     static bool isRight(Direction::directions dir){
         return dir == right || dir == rightDown || dir == rightUp;
     }
     static const QVector<b2Vec2> directionVectors;
     static const  QVector<Direction::directions> nextDir;
     static const  QVector<Direction::directions> rightWave;
     static const  QVector<Direction::directions> leftWave;
     static const  QVector<Direction::directions> upWave;
     static const  QVector<Direction::directions> downWave;

};

#endif // DIRECTIONS_H
