#include "animateddecoritem.h"
#include "QPainter"

AnimatedDecorItem::AnimatedDecorItem()
{

}

AnimatedDecorItem::~AnimatedDecorItem()
{

}

AnimatedDecorItem::AnimatedDecorItem(QGraphicsItem *parent)
{

}

AnimatedDecorItem::AnimatedDecorItem(QVariantMap *newProperties)
{
    setup(newProperties);
    m_frameW = m_width / m_columns;
    m_frameH = m_height / m_rows;
}

void AnimatedDecorItem::paintObject(QPainter *painter)
{
    painter->drawTiledPixmap(0, 0, m_frameW, m_frameH, pixmap,0,0);
}

