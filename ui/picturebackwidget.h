#ifndef PICTUREBACKWIDGET_H
#define PICTUREBACKWIDGET_H

#include <QWidget>

class PictureBackWidget : public QWidget
{
    Q_OBJECT
public:
    explicit PictureBackWidget(QWidget *parent = nullptr);
    void paintEvent(QPaintEvent* e);
    void setBackground(QImage* image );
    void setBackground(QString file );

protected:
     QPixmap back;
};

#endif // PICTUREBACKWIDGET_H
