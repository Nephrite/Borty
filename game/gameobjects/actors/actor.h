#ifndef ENEMY_H
#define ENEMY_H

#include "game/gameobjects/physicsobject.h"
#include "game/gameobjects/directions.h"
#include <QObject>

#include "game/gameobjects/bullet.h"


class Actor : public PhysicsObject
{
    Q_OBJECT

    Q_PROPERTY_WITH_ACCESSORS(int, health, setHealth)

public:
    Actor();
    Actor(QJsonObject&, b2World*);
    Actor(QJsonObject&, b2World*, bool autoCreatePhysics);
    virtual ~Actor();
    Direction::directions direction();
    void setGameData(GameData *gameData);
    void glDraw(GlUtils* glutils);
    double bottomY();

    void applyImpulse(double, double);
    void applyImpulse(b2Vec2);

protected:   

    void createPhysics(b2World*world);
    void disableCollisions();
    void virtual setupShadowSprite();
    GameData* m_gamedata;
    Drawable m_shadowSprite;
    b2FixtureDef shadowDef;
    b2PolygonShape shadowShape;

    // moving
    b2Vec2 m_moveVec;
    Direction::directions  m_direction;
    Direction::directions  m_oldDirection;

    virtual void moveAdvance();
    double m_maxVelocity;
    double getMaxVelocity() const;
    void setMaxVelocity(double value);


    // jumping
    virtual void updateJump();
    double m_maxJumpHeight;
    double m_jumpHeight;
    bool m_inJump;
    bool m_canJump;
    bool m_falling;
    bool m_jumpNow;
    //damage

    int m_damageInterval;
    int m_lastDamageTimeElapsed;

};

#endif // ENEMY_H
