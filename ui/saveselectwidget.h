#ifndef SAVESELECTWIDGET_H
#define SAVESELECTWIDGET_H

#include <QWidget>
#include <QListWidget>
#include "ui/picturebackwidget.h"

namespace Ui {
class SaveSelectWidget;
}

class SaveSelectWidget : public PictureBackWidget
{
    Q_OBJECT

public:
    explicit SaveSelectWidget(QWidget *parent = 0);
    ~SaveSelectWidget();
    QString SaveName();
signals:
    void goBack();

private slots:


    void on_newNameOkButton_clicked();

    void on_newSaveButton_clicked();

    void on_backButton_clicked();

    void on_savesListWidget_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous);

protected:
    void setSelected(QString name);

    QString m_selectedName;
    bool createSave(QString name);
    void loadSaveList();
    void writeDefaultName(QString name);
private:
    Ui::SaveSelectWidget *ui;
};

#endif // SAVESELECTWIDGET_H
