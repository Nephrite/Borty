#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMoveEvent>
#include <QResizeEvent>
//#include <Box2D.h>
#include "game/gamecontroller.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void startLevel(QString file);


private:
    Ui::MainWindow *ui; 
    GameController gameController;
    QStackedWidget* gameWidget;
};

#endif // MAINWINDOW_H
