#-------------------------------------------------
#
# Project created by QtCreator 2013-09-14T17:59:16
#
#-------------------------------------------------

QT       += core opengl

QMAKE_CXXFLAGS += -std=gnu++11



greaterThan(QT_MAJOR_VERSION, 4) {
  QT += widgets
  DEFINES += HAVE_QT5
}

TARGET = borty
TEMPLATE = app

win32:CONFIG(release, debug|release):    LIBS += -L$$shell_path($$PWD/3rdParty/Box2D_v2.2.1/Box2D/win) -lBox2D
else:win32:CONFIG(debug, debug|release): LIBS += -L$$shell_path($$PWD/3rdParty/Box2D_v2.2.1/Box2D/win) -lBox2D
else:unix: LIBS += -L$$PWD/3rdParty/Box2D_v2.2.1/Box2D/linux -lBox2D

INCLUDEPATH += $$PWD/3rdParty/Box2D_v2.2.1

DEPENDPATH += $$PWD/3rdParty/Box2D_v2.2.1


DESTDIR = build

MOC_DIR = tmp

OBJECTS_DIR = tmp

RCC_DIR = tmp

UI_DIR = tmp


#win32 {
#    COPY_RES_FROM_PATH=\"$$shell_path($$PWD\shaders)\"
#    COPY_RES_TO_PATH=  \"$$shell_path($$OUT_PWD\\$$DESTDIR\shaders)\"
#}
#else {
#    COPY_RES_FROM_PATH=\"$$PWD/shaders\"
#    COPY_RES_TO_PATH=  \"$$OUT_PWD/$$DESTDIR/shaders\"
#}

#copydata.commands =  $(COPY_DIR) $$COPY_RES_FROM_PATH $$COPY_RES_TO_PATH

## dll's for windows
win32 {
   copydata.commands += \
     && $(COPY_DIR) \"$$shell_path($$PWD)\platforms\" \"$$shell_path($$OUT_PWD)\\$$DESTDIR\platforms\" \
     && $(COPY_DIR) \"$$shell_path($$PWD)\bin\"       \"$$shell_path($$OUT_PWD)\\$$DESTDIR \"
}


first.depends = $(first) copydata

export(first.depends)
export(copydata.commands)

QMAKE_EXTRA_TARGETS += first copydata



SOURCES += main.cpp\
        mainwindow.cpp \
    game/gamescene.cpp \
    game/objectscreator.cpp \
    game/gameobjects/actors/frog.cpp \
    game/gameobjects/actors/wind.cpp \
    game/gameobjects/decorobject.cpp \
    game/gameobjects/gamecontactlistener.cpp \
    game/gameobjects/parallaxbackground.cpp \
    ui/mainmenu.cpp \
    game/gameobjects/actors/waterblock.cpp \
    game/gameobjects/physicsobject.cpp \
    game/gamecontroller.cpp \
    ui/pausemenu.cpp \
    ui/levelbutton.cpp \
    game/settings.cpp \
    common/save.cpp \
    game/gameobjects/endlevelblock.cpp \
    game/gameobjects/actors/movingplatform.cpp \
    game/headup.cpp \
    editor/items/baseitem.cpp \
    editor/items/itembehaviour.cpp \
    editor/editorscene.cpp \
    editor/itemselectwidget.cpp \
    editor/newobjectwidget.cpp \
    editor/items/endlevelblockitem.cpp \
    editor/items/movingplatformitem.cpp \
    3rdParty/QPropertyEditor/ColorCombo.cpp \
    3rdParty/QPropertyEditor/EnumProperty.cpp \
    3rdParty/QPropertyEditor/Property.cpp \
    3rdParty/QPropertyEditor/QPropertyEditorWidget.cpp \
    3rdParty/QPropertyEditor/QPropertyModel.cpp \
    3rdParty/QPropertyEditor/QVariantDelegate.cpp \
    editor/items/backgrounditem.cpp \
    game/gameobjects/actors/coin.cpp \
    game/gameobjects/basegameobject.cpp \
    game/gameobjects/player.cpp \
    editor/editoritemcreator.cpp \    
    ui/spacebackwidget.cpp \
    game/camera.cpp \
    game/gameobjects/sensor.cpp \
    editor/objecteditor.cpp \
    editor/thumbnailer.cpp \
    game/glesgameview/glesgameview.cpp \
    game/glesgameview/glesdrawable.cpp \
    game/glesgameview/glutils.cpp \
    editor/colorpicker.cpp \
    common/levelproperties.cpp \
    common/serializable.cpp \
    game/gameobjects/actors/springbug.cpp \
    editor/editorwindow.cpp \
    editor/editorsceneview.cpp \
    common/config.cpp \
    editor/itembutton.cpp \
    editor/autofilldialog.cpp \
    game/gameobjects/billboardphysicsobject.cpp \
    editor/items/billboardphysicsitem.cpp \
    editor/items/staticbackgrounditem.cpp \
    game/gameobjects/staticbackground.cpp \
    game/gamedata.cpp \
    game/gameobjects/actors/actor.cpp \
    game/gameobjects/actors/gribbo.cpp \
    game/gameobjects/directions.cpp \
    game/gameobjects/bullet.cpp \
    editor/items/gribboitem.cpp \
    ui/worldMap/worldmap.cpp \
    ui/endlevelscreen.cpp \
    ui/worldMap/mapwidget.cpp \
    game/gameobjects/actors/hen.cpp \
    ui/saveselectwidget.cpp \
    game/gameobjects/tooltipobject.cpp \
    editor/items/tooltipitem.cpp \
    game/gameobjects/actors/npc.cpp \
    editor/items/npcitem.cpp \
    game/gameobjects/ammo.cpp \
    editor/items/ammoitem.cpp \
    common/object.cpp \
    game/gameobjects/playerstatistics.cpp \
    game/gameobjects/propertyeffectmanager.cpp \
    game/gameobjects/propertymodifier.cpp \
    game/gameobjects/animateddecorobject.cpp \
    editor/items/animateddecoritem.cpp \
    game/gameobjects/swinginggrass.cpp \
    game/gameobjects/actors/flame.cpp \
    editor/items/flameitem.cpp \
    3rdParty/QJsonModel-master/qjsonmodel.cpp \
    game/gameobjects/actors/snowcloud.cpp \
    editor/items/snowclouditem.cpp \
    ui/picturebackwidget.cpp


HEADERS  += mainwindow.h \
    game/gamescene.h \
    game/objectscreator.h \
    game/gameobjects/actors/frog.h \
    game/gameobjects/actors/wind.h \
    game/gameobjects/decorobject.h \
    game/gameobjects/gamecontactlistener.h \
    game/gameobjects/parallaxbackground.h \
    game/gameobjects/physicsobject.h \
    game/gameobjects/player.h \
    ui/mainmenu.h \
    game/gameobjects/actors/waterblock.h \
    game/gamecontroller.h \
    ui/pausemenu.h \
    ui/levelbutton.h \
    game/settings.h \   
    common/objecttypes.h \
    game/gameobjects/endlevelblock.h \
    game/gameobjects/actors/movingplatform.h \
    game/headup.h \
    editor/items/baseitem.h \
    editor/items/itembehaviour.h \
    editor/editorscene.h \
    editor/itemselectwidget.h \
    editor/newobjectwidget.h \
    editor/items/endlevelblockitem.h \
    editor/items/movingplatformitem.h \
    3rdParty/QPropertyEditor/ColorCombo.h \
    3rdParty/QPropertyEditor/EnumProperty.h \
    3rdParty/QPropertyEditor/Property.h \
    3rdParty/QPropertyEditor/QPropertyEditorWidget.h \
    3rdParty/QPropertyEditor/QPropertyModel.h \
    3rdParty/QPropertyEditor/QVariantDelegate.h \
    editor/items/backgrounditem.h \
    game/gameobjects/actors/coin.h \
    game/gameobjects/basegameobject.h \
    editor/editoritemcreator.h \
    ui/endlevelscreen.h \
    ui/spacebackwidget.h \
    common/qpropertyaccessmacro.h \
    game/camera.h \
    game/gameobjects/sensor.h \
    editor/objecteditor.h \
    editor/thumbnailer.h \
    game/glesgameview/glesgameview.h \
    game/glesgameview/glesdrawable.h \
    game/glesgameview/glutils.h \
    editor/colorpicker.h \
    common/levelproperties.h \
    common/serializable.h \
    game/gameobjects/actors/springbug.h \
    editor/editorwindow.h \
    editor/editorsceneview.h \
    common/config.h \
    editor/itembutton.h \
    editor/autofilldialog.h \
    3rdParty/Box2D_v2.2.1/Box2D/Collision/Shapes/b2ChainShape.h \
    3rdParty/Box2D_v2.2.1/Box2D/Collision/Shapes/b2CircleShape.h \
    3rdParty/Box2D_v2.2.1/Box2D/Collision/Shapes/b2EdgeShape.h \
    3rdParty/Box2D_v2.2.1/Box2D/Collision/Shapes/b2PolygonShape.h \
    3rdParty/Box2D_v2.2.1/Box2D/Collision/Shapes/b2Shape.h \
    3rdParty/Box2D_v2.2.1/Box2D/Collision/b2BroadPhase.h \
    3rdParty/Box2D_v2.2.1/Box2D/Collision/b2Collision.h \
    3rdParty/Box2D_v2.2.1/Box2D/Collision/b2Distance.h \
    3rdParty/Box2D_v2.2.1/Box2D/Collision/b2DynamicTree.h \
    3rdParty/Box2D_v2.2.1/Box2D/Collision/b2TimeOfImpact.h \
    3rdParty/Box2D_v2.2.1/Box2D/Common/b2BlockAllocator.h \
    3rdParty/Box2D_v2.2.1/Box2D/Common/b2Draw.h \
    3rdParty/Box2D_v2.2.1/Box2D/Common/b2GrowableStack.h \
    3rdParty/Box2D_v2.2.1/Box2D/Common/b2Math.h \
    3rdParty/Box2D_v2.2.1/Box2D/Common/b2Settings.h \
    3rdParty/Box2D_v2.2.1/Box2D/Common/b2StackAllocator.h \
    3rdParty/Box2D_v2.2.1/Box2D/Common/b2Timer.h \
    3rdParty/Box2D_v2.2.1/Box2D/Dynamics/Contacts/b2ChainAndCircleContact.h \
    3rdParty/Box2D_v2.2.1/Box2D/Dynamics/Contacts/b2ChainAndPolygonContact.h \
    3rdParty/Box2D_v2.2.1/Box2D/Dynamics/Contacts/b2CircleContact.h \
    3rdParty/Box2D_v2.2.1/Box2D/Dynamics/Contacts/b2Contact.h \
    3rdParty/Box2D_v2.2.1/Box2D/Dynamics/Contacts/b2ContactSolver.h \
    3rdParty/Box2D_v2.2.1/Box2D/Dynamics/Contacts/b2EdgeAndCircleContact.h \
    3rdParty/Box2D_v2.2.1/Box2D/Dynamics/Contacts/b2EdgeAndPolygonContact.h \
    3rdParty/Box2D_v2.2.1/Box2D/Dynamics/Contacts/b2PolygonAndCircleContact.h \
    3rdParty/Box2D_v2.2.1/Box2D/Dynamics/Contacts/b2PolygonContact.h \
    3rdParty/Box2D_v2.2.1/Box2D/Dynamics/Joints/b2DistanceJoint.h \
    3rdParty/Box2D_v2.2.1/Box2D/Dynamics/Joints/b2FrictionJoint.h \
    3rdParty/Box2D_v2.2.1/Box2D/Dynamics/Joints/b2GearJoint.h \
    3rdParty/Box2D_v2.2.1/Box2D/Dynamics/Joints/b2Joint.h \
    3rdParty/Box2D_v2.2.1/Box2D/Dynamics/Joints/b2MouseJoint.h \
    3rdParty/Box2D_v2.2.1/Box2D/Dynamics/Joints/b2PrismaticJoint.h \
    3rdParty/Box2D_v2.2.1/Box2D/Dynamics/Joints/b2PulleyJoint.h \
    3rdParty/Box2D_v2.2.1/Box2D/Dynamics/Joints/b2RevoluteJoint.h \
    3rdParty/Box2D_v2.2.1/Box2D/Dynamics/Joints/b2RopeJoint.h \
    3rdParty/Box2D_v2.2.1/Box2D/Dynamics/Joints/b2WeldJoint.h \
    3rdParty/Box2D_v2.2.1/Box2D/Dynamics/Joints/b2WheelJoint.h \
    3rdParty/Box2D_v2.2.1/Box2D/Dynamics/b2Body.h \
    3rdParty/Box2D_v2.2.1/Box2D/Dynamics/b2ContactManager.h \
    3rdParty/Box2D_v2.2.1/Box2D/Dynamics/b2Fixture.h \
    3rdParty/Box2D_v2.2.1/Box2D/Dynamics/b2Island.h \
    3rdParty/Box2D_v2.2.1/Box2D/Dynamics/b2TimeStep.h \
    3rdParty/Box2D_v2.2.1/Box2D/Dynamics/b2World.h \
    3rdParty/Box2D_v2.2.1/Box2D/Dynamics/b2WorldCallbacks.h \
    3rdParty/Box2D_v2.2.1/Box2D/Rope/b2Rope.h \
    game/gameobjects/billboardphysicsobject.h \
    editor/items/billboardphysicsitem.h \
    editor/items/staticbackgrounditem.h \
    game/gameobjects/staticbackground.h \
    game/gamedata.h \
    game/gameobjects/actors/actor.h \
    game/gameobjects/actors/gribbo.h \
    game/gameobjects/directions.h \
    game/gameobjects/bullet.h \
    editor/items/gribboitem.h \
    ui/worldMap/worldmap.h \
    ui/worldMap/mapwidget.h \
    game/gameobjects/actors/hen.h \
    ui/saveselectwidget.h \
    game/gameobjects/tooltipobject.h \
    editor/items/tooltipitem.h \
    game/gameobjects/actors/npc.h \
    editor/items/npcitem.h \
    game/gameobjects/ammo.h \
    editor/items/ammoitem.h \
    common/object.h \
    game/gameobjects/playerstatistics.h \
    game/gameobjects/propertymodifier.h \
    game/gameobjects/propertymodifiermanager.h \
    game/gameobjects/animateddecorobject.h \
    editor/items/animateddecoritem.h \
    game/gameobjects/swinginggrass.h \
    game/gameobjects/actors/flame.h \
    editor/items/flameitem.h \
    3rdParty/QJsonModel-master/qjsonmodel.h \
    game/gameobjects/actors/snowcloud.h \
    editor/items/snowclouditem.h \
    ui/picturebackwidget.h


FORMS    += mainwindow.ui \
    ui/mainmenu.ui \
    ui/pausemenu.ui \
    editor/editorWindow.ui \
    editor/itemselectwidget.ui \
    editor/newobjectwidget.ui \
    editor/objectpropertieseditor.ui \
    ui/endlevelscreen.ui \
    editor/objecteditor.ui \
    editor/colorpicker.ui \
    editor/autofilldialog.ui \
    ui/worldMap/worldmap.ui \
    ui/saveselectwidget.ui


SUBDIRS += \
    3rdParty/QPropertyEditor/QPropertyEditor.pro

DISTFILES += \   
    ui/worldMap/style.css \
    resources/shaders/shader.json \
    shaders/fanimsprite.glsl \
    shaders/fglowshader.glsl \
    shaders/fscreenshader.glsl \
    shaders/ftextureshader.glsl \
    shaders/fwater.glsl \
    shaders/parallaxbackshader.glsl \
    shaders/vanimsprite.glsl \
    shaders/vpointshader.glsl \
    shaders/vshader.glsl \
    shaders/shaders.json \
    shaders/vswingingGrass.glsl \
    shaders/fflame.glsl \
    shaders/fsnowcloud.glsl



