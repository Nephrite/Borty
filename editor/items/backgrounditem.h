#ifndef BACKGROUNDITEM_H
#define BACKGROUNDITEM_H

#include <QObject>
#include "editor/items/baseitem.h"
#include <common/serializable.h>
class BackgroundItem : public BaseItem
{
    Q_OBJECT

    Q_PROPERTY(double xAutoScroll READ xAutoScroll WRITE setXAutoScroll DESIGNABLE true USER true)
    Q_PROPERTY(double yAutoScroll READ yAutoScroll WRITE setYAutoScroll DESIGNABLE true USER true)
    Q_PROPERTY(double Xspeed      READ Xspeed      WRITE setXspeed      DESIGNABLE true USER true)
    Q_PROPERTY(double Yspeed      READ Yspeed      WRITE setYspeed      DESIGNABLE true USER true)
    Q_PROPERTY(double Xtiling     READ Xtiling     WRITE setXtiling     DESIGNABLE true USER true)
    Q_PROPERTY(double Ytiling     READ Ytiling     WRITE setYtiling     DESIGNABLE true USER true)

public:
    enum sizeType{expandingSize, fixedSize};
    Q_ENUM(sizeType)
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS(sizeType, XsizePolicy,  setXsizePolicy)
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS(sizeType, YsizePolicy,  setYsizePolicy)
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS(bool, attachToWorld,  setAttachToWorld)

    explicit BackgroundItem();
    explicit BackgroundItem(QVariantMap *newProperties);
    ~BackgroundItem();


    double xAutoScroll() const
    {
        return m_xAutoScroll;
    }

    double yAutoScroll() const
    {
        return m_yAutoScroll;
    }

    double Xspeed() const
    {
        return m_Xspeed;
    }

    double Yspeed() const
    {
        return m_Yspeed;
    }

    double Xtiling() const
    {
        return m_Xtiling;
    }

    double Ytiling() const
    {
        return m_Ytiling;
    }

public slots:

    void setXAutoScroll(double xAutoScroll)
    {
        m_xAutoScroll = xAutoScroll;
    }
    void setYAutoScroll(double yAutoScroll)
    {
        m_yAutoScroll = yAutoScroll;
    }
    void setXspeed(double Xspeed)
    {
        m_Xspeed = Xspeed;
    }
    void setYspeed(double Yspeed)
    {
        m_Yspeed = Yspeed;
    }
    void setXtiling(double Xtiling)
    {
        m_Xtiling = Xtiling;
    }
    void setYtiling(double Ytiling)
    {
        m_Ytiling = Ytiling;
    }

private:

    double m_xAutoScroll;
    double m_yAutoScroll;
    double m_Xspeed;
    double m_Yspeed;
    double m_Xtiling;
    double m_Ytiling;
};

#endif // BACKGROUNDITEM_H
