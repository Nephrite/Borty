#version 130
#ifdef GL_ES
// Set default precision to medium
precision mediump int;
precision mediump float;
#endif

uniform mat4 mvp_matrix;
uniform float time;
uniform float arg1;
uniform float arg2;
uniform float arg3;
attribute vec4 a_position;
attribute vec2 a_texcoord;
varying vec2 v_texcoord;
//! [0]
void main()
{
    v_texcoord = a_texcoord;
    gl_Position = mvp_matrix * a_position;
    gl_Position.x += ((1.0-a_texcoord.y)*sin((arg3+gl_Position.x+gl_Position.y)+time/arg2))*(arg1/100.0);
}
//! [0]
