#include "mainmenu.h"
#include "ui_mainmenu.h"


#include <QVector>
#include <QTimer>
#include <QDebug>
#include <QShowEvent>

#include <QFile>
#include "common/save.h"

MainMenu::MainMenu(QWidget *parent) :
    SpaceBackWidget(parent),
    ui(new Ui::MainMenu)
{
    ui->setupUi(this);
    ui->levelsButton->setFocus(Qt::TabFocusReason);
}

MainMenu::~MainMenu()
{
    delete ui;
}

void MainMenu::keyPressEvent(QKeyEvent *e)
{
    switch (e->key()) {
    case Qt::Key_S:
        focusNextChild();
        break;

    case Qt::Key_W:
        focusPreviousChild();
        break;
    }
    QWidget::keyPressEvent(e);
}

void MainMenu::showEvent(QShowEvent *e)
{   
    SpaceBackWidget::showEvent(e);
}

void MainMenu::displaySaveName(QString name)
{
    ui->saveSelectButton->setText("Сохранение: "+ name);
}

void MainMenu::on_levelsButton_clicked()
{
    Save::instance()->load();
    setPause(true);

    emit showLevelPackWidget();
}

void MainMenu::on_pushButton_2_clicked()
{
    setPause(true);
    emit showGameView();
}

void MainMenu::on_runEditorButton_clicked()
{
    setPause(true);
    emit runEditor();
}


void MainMenu::on_exitButton_clicked()
{
    QApplication::exit(0);
}

void MainMenu::on_saveSelectButton_clicked()
{
    setPause(true);
    emit showSaveSelectWidget();
}
