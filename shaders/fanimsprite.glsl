#version 130
#ifdef GL_ES

precision mediump int;
precision mediump float;
#endif

uniform sampler2D texture;

varying vec2 v_texcoord;

uniform vec2 flipping;
uniform vec2 tiling;

uniform float frameW;
uniform int column;

uniform float frameH;
uniform int row;

uniform float opacity;

void main()
{
    vec2 coord;

    // selecting frame
    coord.y = v_texcoord.y*frameH+(frameH*row);
    coord.x = v_texcoord.x*frameW+(frameW*column);

    // tiling and flipping
    coord *= tiling * flipping;

    // texture
    vec4 color = texture2D(texture, coord);

    //premultipled alpha
    color.a *= opacity;
    color.rgb *= color.aaa;
    //result

    gl_FragColor = color;
}
//! [0]
