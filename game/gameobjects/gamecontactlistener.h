#ifndef GAMECONTACTLISTENER_H
#define GAMECONTACTLISTENER_H
#include <Box2D/Box2D.h>
class PhysicsObject;
class GameContactListener : public b2ContactListener
{
public:
    GameContactListener();
    void BeginContact(b2Contact*);   
    void PreSolve (b2Contact*, const b2Manifold*);
    void EndContact(b2Contact*);
private:
    PhysicsObject* objA;
    PhysicsObject* objB;
    b2Fixture* fixA;
    b2Fixture* fixB;
    b2Filter filterA;
    b2Filter filterB;
    int idA;
    int idB;
    bool collide;


};

#endif // GAMECONTACTLISTENER_H
