#ifndef BULLET_H
#define BULLET_H

#include "common/objecttypes.h"

#include "game/gameobjects/physicsobject.h"
#include "game/gameobjects/directions.h"

#include <QObject>

class Bullet : public PhysicsObject
{
    Q_OBJECT

    Q_PROPERTY_WITH_ACCESSORS(QString, affectProperty, setAffectProperty )
    Q_PROPERTY_WITH_ACCESSORS(int, value, setValue )
    Q_PROPERTY_WITH_ACCESSORS(bool, isTasty, setTasty)
    Q_PROPERTY_WITH_ACCESSORS(double, maxVelocity, setMaxVelocity)

public:

    enum BulletTarget{
        PlayerTarget,
        EnemyTarget,
        AllTarget
    };
    Q_ENUMS(BulletTarget)



    Bullet(QJsonObject &parameter, b2World* world);
    Bullet(type::bullets t, b2World* world);
    Bullet(b2World* world);
    Bullet(){}
    bool collideWith(int type);
    void preSolve(PhysicsObject* object, b2Contact* contact);
    enum state{fly, fading};
    void glDraw(GlUtils* glutils);
    void setPos(b2Vec2 pos, double offset);
    double bottomY();
    virtual ~Bullet();
    void createPhysics(b2World* world);
    void disable();

    void advance(int timeDelta);
    void onScreenLeave();
    void setDirection(Direction::directions dir);
    void setTarget(BulletTarget t);
    BulletTarget target(){return m_target;}

protected:
    type::bullets m_bulletType;
    BulletTarget m_target;
    state m_state;
    Direction::directions m_direction;
    b2Vec2 moveVec;
    int m_timer;
    Drawable m_shadowSprite;

    b2CircleShape circle;

};

#endif // BULLET_H
