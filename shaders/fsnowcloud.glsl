uniform sampler2D texture0; // main texture
uniform sampler2D texture1; // stencil

varying vec2 v_texcoord;
uniform float time;
uniform float arg1; // blink speed
uniform float arg2; // xspeed
uniform float arg3; // yspeed

void main(void)
{   
    vec4 stencil = texture2D(texture1, v_texcoord);
    float t = time;
    vec2 coord = v_texcoord;
    coord.x += (arg2*t);
    coord.y += fract(arg3*t);

    vec4 value = texture2D(texture0, coord);
    value.a *= stencil.r*sin(time/arg1);
    value.rgb *= value.aaa;

    gl_FragColor = value;
}
