#ifndef SWINGINGGRASS_H
#define SWINGINGGRASS_H

#include <QObject>
#include "game/gameobjects/basegameobject.h"
class SwingingGrass : public BaseGameObject
{
    Q_OBJECT
public:
    SwingingGrass();
    SwingingGrass(QJsonObject& parameters);
    ~SwingingGrass();
    void advance(int timedelta);

};

#endif // SWINGINGGRASS_H
