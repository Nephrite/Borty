#include "staticbackgrounditem.h"
#include "common/objecttypes.h"

StaticBackgroundItem::StaticBackgroundItem()
{
    m_objectType =  type::StaticBackground;
    m_metaType = "background";
    setZValue(-5);

}

StaticBackgroundItem::StaticBackgroundItem(QVariantMap *newProperties) :
    BaseItem(newProperties)
{
    m_objectType =  type::StaticBackground;
    m_metaType = "background";
    setZValue(-5);
}
