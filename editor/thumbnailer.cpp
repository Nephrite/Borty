#include "thumbnailer.h"

#include <QDir>
#include <QPixmap>
#include <QDebug>

Thumbnailer::Thumbnailer()
{
    checkFolder();
}

bool Thumbnailer::fileExists(QString fileName)
{
    m_fileInfo.setFile(fileName);
    return (m_fileInfo.exists() && m_fileInfo.isFile());
}

QString Thumbnailer::thumb(QString fileName)
{
    if (!fileExists(fileName)) return QString("/res/ui/heart.png");
    QString thumbName;
    thumbName = "./resources/thumbs/thumb_"+m_fileInfo.dir().dirName()+'_'+m_fileInfo.baseName()+".png";
    if (!fileExists(thumbName))
    {
        createThumb(fileName, thumbName);
    }
    return thumbName;
}

void Thumbnailer::createThumb(QString sourceFile, QString thumbName)
{
    qDebug()<<"create thumbnail: ||" << thumbName << " || for || " << sourceFile;
    QPixmap pix;
    pix.load(sourceFile);
    QPixmap thumb = pix.scaled(64,64, Qt::KeepAspectRatioByExpanding);
    thumb.save(thumbName, "PNG");
}

void Thumbnailer::forceRegenerate()
{

}

void Thumbnailer::checkFolder()
{
    QDir resDir("./resources/");
    resDir.mkdir("thumbs");
}

