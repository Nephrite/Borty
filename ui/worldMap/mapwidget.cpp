#include "mapwidget.h"
#include <QPainter>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonValue>
#include <QDebug>

#include <math.h>

MapWidget::MapWidget(QWidget *parent) :
    QWidget(parent),
    m_levelIdx(0)

{
    m_mapPixmap.load("./resources/ui/worldmap.png");
    m_cursor.load("./resources/ui/cursor.png");
    m_mapPoint.load("./resources/ui/mappoint.png");
    m_mapPointFinished.load("./resources/ui/mappointfinished.png");
    m_animation = new QPropertyAnimation(this, "cursorPos");
}

MapWidget::~MapWidget()
{
    m_animation->stop();
}

void MapWidget::paintEvent(QPaintEvent *e)
{
    if (m_mapPoints.count() == 0) return;

    QPainter pa(this);
    pa.translate( (width()-m_mapPixmap.width())/2,
                  (height()-m_mapPixmap.height())/2
                );
    pa.drawPixmap(0,
                  0,
                  m_mapPixmap.width(),
                  m_mapPixmap.height(),
                  m_mapPixmap);

    for (int i = 0; i < m_mapPoints.count(); i++)
    {
        QPoint point = m_mapPoints[i].point;
        if (!m_mapPoints[i].finished)
        {
            pa.drawPixmap(point.x()   -(m_mapPoint.width()/2),
                      point.y()-(m_mapPoint.height()/2),
                      m_mapPoint);
        }
        else
        {
            pa.drawPixmap(point.x()   -(m_mapPointFinished.width()/2),
                          point.y()-(m_mapPointFinished.height()/2),
                          m_mapPointFinished);
        }
    }

    pa.drawPixmap(m_cursorPos.x() - m_cursor.width()/2,
                  m_cursorPos.y() - m_cursor.height()/2,
                  m_cursor);

    QWidget::paintEvent(e);
}

void MapWidget::selectNext()
{
    if (++m_levelIdx >= m_mapPoints.count())
    {
        m_levelIdx = 0;
    }
    m_cursorTarget = m_mapPoints[m_levelIdx].point;
    startAnimation();
}

void MapWidget::selectPrevious()
{
    if (--m_levelIdx <= -1 )
    {
        m_levelIdx = m_mapPoints.count()-1;
    }
    m_cursorTarget = m_mapPoints[m_levelIdx].point;
    startAnimation();
}

void MapWidget::addPoint(QPoint point, bool finished)
{
    PointData data;
    data.point = point;
    data.finished = finished;
    m_mapPoints.append(data);
    m_cursorPos = m_mapPoints.first().point;
}

QPoint MapWidget::cursorPos() const
{
    return m_cursorPos;
}

void MapWidget::setCursorPos(const QPoint &cursorPos)
{
    m_cursorPos = cursorPos;
}

void MapWidget::clear()
{
    m_mapPoints.clear();
}

void MapWidget::startAnimation()
{
    m_animation->stop();
    m_animation->setStartValue(m_cursorPos);
    m_animation->setEndValue(m_cursorTarget);
    m_animation->setEasingCurve(QEasingCurve::InOutCubic);
    QPoint dist = (m_cursorTarget - m_cursorPos);
    m_animation->setDuration((sqrt(dist.x()*dist.x()+dist.y()*dist.y()))*2);
    m_animation->start();
    cursorPosChanged();
}


void MapWidget::cursorPosChanged()
{
    update();
    if (m_cursorPos == m_cursorTarget)
    {
        m_animation->stop();
    }
    else
    {
        m_timer.singleShot(16, this, &MapWidget::cursorPosChanged);
    }
}


