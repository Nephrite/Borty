#ifndef OBJECTPROPERTIES_H
#define OBJECTPROPERTIES_H

#include <QObject>
#include <QGraphicsItem>
#include "itembehaviour.h"
#include <Box2D/Box2D.h>

#include "common/serializable.h"
#include "QJsonDocument"


class BaseItem : public Serializable , public QGraphicsItem
{
    Q_OBJECT

    Q_INTERFACES(QGraphicsItem)
public:
    enum  PhysicsType
    {
        staticBody    = b2_staticBody  ,
        kinematicBody = b2_kinematicBody,
        dynamicBody   = b2_dynamicBody  ,
    };
    Q_ENUM(PhysicsType)
    enum  TextureMappingType
    {
        StretchedMapping,
        TiledMapping
    };
    Q_ENUM(TextureMappingType)

    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS ( QString, texture,       setTexture       )
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS ( QString, textureStyle,  setTextureStyle  )
     Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS ( double,   width,         setWidth         )
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS ( double,   height,        setHeight        )
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS ( double,  yMargin,       setYMargin       )
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS ( int,     objectType,    setObjectType    )
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS ( QString, metaType,      setMetaType      )
    //Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS ( QString, id,            setId            )
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS ( bool,    fixedRotation, setFixedRotation )
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS (TextureMappingType, verticalMapping,   setVerticalMapping )
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS (TextureMappingType, horizontalMapping, setHorizontalMapping )
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS (PhysicsType, physicsType, setPhysicsType )
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS (bool,     visible,          setVisible )
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS(bool,  fullSize,   setFullSize)
    Q_PROPERTY(double   x READ x WRITE setXpos  DESIGNABLE true USER true)
    Q_PROPERTY(double   y READ y WRITE setYpos  DESIGNABLE true USER true)
    Q_PROPERTY(qreal zValue   READ zValue   WRITE setZValue   DESIGNABLE true USER true) // inherited from QGraphicsItem
    Q_PROPERTY(qreal rotation READ rotation WRITE setRotation DESIGNABLE true USER true) // inherited from QGraphicsItem
    Q_PROPERTY(QString objectName READ objectName WRITE setObjectName DESIGNABLE true USER true)


    explicit BaseItem(QGraphicsItem *parent = nullptr);
    explicit BaseItem(QVariantMap *newProperties);  
    ~BaseItem();

    QVariantMap toMap();

    int type() const{return QGraphicsItem::UserType;}

    virtual void updateProperties();
    virtual bool hasHandles(){return false;}
    virtual void setupHandles(){}

    void setBehaviour(ItemBehaviour* newBehaviour);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    QJsonDocument& extraProperties(){return m_ExtraProperties;}
    void setExtraProperties(QJsonDocument document);
    QRectF boundingRect() const;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent* event);  
    QVariant itemChange(GraphicsItemChange change, const QVariant &value);
    void setPosition(double x, double y);
    void toGrid();

    QJsonDocument &ExtraPropertiesJson();

    double x(){return pos().x();}
    double y(){return pos().y();}
    void setXpos(double x){setPos(x, pos().y());}
    void setYpos(double y){setPos(pos().x(), y);}

signals:
    void editProperties(BaseItem* properties);

protected:
    virtual void paintObject(QPainter* painter);
    virtual void paintHelpers(QPainter* painter);
    QPixmap pixmap;
    ItemBehaviour* behaviour;
    bool inMoving;
    int align(int value, int gridSize);
    bool m_isAligned = false;
    QJsonDocument m_ExtraProperties;


};


#endif // OBJECTPROPERTIES_H
