#ifndef TOOTIPOBJECT_H
#define TOOTIPOBJECT_H

#include "./game/gameobjects/basegameobject.h"

#include <QObject>
#include <QString>

class TooltipObject : public BaseGameObject
{
    Q_OBJECT
    Q_PROPERTY_WITH_ACCESSORS(QString, text,  setText)
    Q_PROPERTY_WITH_ACCESSORS(int, lifetime,  setLifeTime)
    Q_PROPERTY_WITH_ACCESSORS(int, delay,  setDelay)
    Q_PROPERTY_WITH_ACCESSORS(bool, attachToCenter,  setAttachToCenter)
    Q_PROPERTY_WITH_ACCESSORS(QColor, textColor,  setTextColor)
public:
    enum fadeStates{wait, fade, unfade, nofade};

    TooltipObject();
    TooltipObject(QString text);
    TooltipObject(QString text, b2Vec2 pos);
    TooltipObject(QJsonObject& parameters);
    ~TooltipObject();
    void advance(int timedelta);
    void glDraw(GlUtils* glutils);
    void onScreenLeave();
    void setPosition(b2Vec2 pos);
    double bottomY();
    void setGameData(GameData* data);

protected:
    void startFading();
    void startUnfading();

    void setupSprite();
    void selectPaneForTextLength(int length);
    int m_timer;    
    GameData* m_gameData;
    GLfloat m_fadeValue;
    fadeStates m_fadeState;

};

#endif // TOOTIPOBJECT_H
