#ifndef WORLDMAP_H
#define WORLDMAP_H

#include <QWidget>
#include <QJsonArray>
#include <QJsonObject>
#include <QPaintEvent>
#include <QKeyEvent>

namespace Ui {
class WorldMap;
}

class WorldMap : public QWidget
{
    Q_OBJECT

public:
    explicit WorldMap(QWidget *parent = 0);
    ~WorldMap();
    void paintEvent(QPaintEvent* e);
    void keyPressEvent(QKeyEvent* e);
    void reset();
    void showEvent(QShowEvent* e);
    enum states {
        MapSelect,
        Dialog,
        Confirm
    };

signals:

    void selectLevel(QString file);
    void goBack();

protected:

    states m_state;
    int m_dialogReplyIdx;
    bool m_confirm;

    void showNextDialogReply();
    void showConfirm();
    int m_currentLevelIdx;
    void selectNext();
    void selectPrevious();
    void loadLevels();
    void updateUi();
    QJsonArray m_levels;
    QJsonObject m_currentLevel;
    QJsonObject m_companion;
    QJsonArray m_dialog;
    QPixmap m_background;

private:
    Ui::WorldMap *ui;

};

#endif // WORLDMAP_H
