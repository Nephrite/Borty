#include "gamecontroller.h"
#include "editor/editorwindow.h"

#include <QHBoxLayout>
#include <QPushButton>

#include <common/save.h>


GameController::GameController()
{
    p_gameWidget = nullptr;
    wait = true;

    p_timer = new QTimer;
    m_timeDelta = 20;


}

GameController::~GameController()
{

}

void GameController::setGameWidget(QStackedWidget *widget)
{
    p_gameWidget = widget;
}

void GameController::setupGameWidget()
{
    Q_ASSERT(p_gameWidget != nullptr);

    p_gameView = new GlesGameView();
    p_headUp = p_gameView->headUp();

    p_gameScene = new GameScene();
    p_gameWidget->addWidget(p_gameView);

    p_gameView->setScene(p_gameScene);


    QObject::connect(  p_gameView,
                     & GlesGameView::pauseGame,
                       this,
                     & GameController::pause
                    );
    QObject::connect(  p_timer,
                     & QTimer::timeout,
                       this,
                     & GameController::advance);

    setupLevelSelectWidget();
    setupPauseMenu();
    setupMainMenu();
    setupEndLevelScreen();
    setupSaveSelectWidget();
    showMenu();
}

void GameController::advance()
{
   // m_time.restart();

    switch (m_gameState)
    {
    case game:
        if (m_paused) return;
        p_gameScene->updateViewport(p_gameView->size());
        p_gameScene->advance(m_timeDelta);
        p_gameView->update();

        p_timer->singleShot(m_timeDelta, this, SLOT(advance()));
        break;

    case finishwaiting:
        p_gameView->update();
        p_timer->singleShot(m_timeDelta, this, SLOT(advance()));
        if (m_elapsedTime > 2000) m_gameState = finish;
        break;

    case finish:
       finishLevel();
    default:
        break;
    }
  //  m_advanceTime = m_time.elapsed();
    m_elapsedTime += m_timeDelta;
}

void GameController::setupMainMenu()
{
    p_mainMenu = new MainMenu;
    p_gameWidget->addWidget(p_mainMenu);
    QObject::connect(  p_mainMenu,
                     & MainMenu::showLevelPackWidget,
                       this,
                     & GameController::showLevelPackWidget
                    );
    QObject::connect(  p_mainMenu,
                     & MainMenu::showGameView,
                       this,
                     & GameController::showGameView
                    );
    QObject::connect( p_mainMenu,
                     & MainMenu::runEditor,
                       this,
                     & GameController::onRunEditor);
    QObject::connect( p_mainMenu,
                     & MainMenu::showSaveSelectWidget,
                       this,
                     & GameController::showSaveSelectWidget);


}

void GameController::setupPauseMenu()
{
     p_pauseMenu = new PauseMenu;
     p_gameWidget->addWidget(p_pauseMenu);
     QObject::connect(  p_pauseMenu,
                      & PauseMenu::continueGame,
                        this,
                      & GameController::continueGame
                     );
     QObject::connect(  p_pauseMenu,
                      & PauseMenu::showMenu,
                        this,
                      & GameController::stopGame
                     );

}

void GameController::setupLevelSelectWidget()
{
    p_levelSelectWidget = new WorldMap;
    p_gameWidget->addWidget(p_levelSelectWidget);

    QObject::connect(  p_levelSelectWidget,
                     & WorldMap::goBack,
                       this,
                     & GameController::showMenu
                    );
    QObject::connect(  p_levelSelectWidget,
                     & WorldMap::selectLevel,
                       this,
                     & GameController::setLevel
                    );

}

void GameController::setupEndLevelScreen()
{
    p_endLevelScreen = new EndLevelScreen;
    p_gameWidget->addWidget(p_endLevelScreen);
    QObject::connect(  p_endLevelScreen,
                     & EndLevelScreen::goToMenu,
                       this,
                     & GameController::showMenu
                    );
    QObject::connect(  p_endLevelScreen,
                     & EndLevelScreen::nextLevel,
                       this,
                     & GameController::showLevelPackWidget
                       );

}

void GameController::setupSaveSelectWidget()
{
    m_saveSelectWidget = new SaveSelectWidget;
    p_gameWidget->addWidget(m_saveSelectWidget);
    QObject::connect(  m_saveSelectWidget,
                     & SaveSelectWidget::goBack,
                       this,
                     & GameController::showMenu
                    );
}


void GameController::showMenu()
{
    p_gameWidget->setCurrentWidget(p_mainMenu);   
    p_mainMenu->displaySaveName(m_saveSelectWidget->SaveName());
}

void GameController::showLevelPackWidget()
{

    p_levelSelectWidget->reset();
    p_gameWidget->setCurrentWidget(p_levelSelectWidget);
}

void GameController::exitGame()
{

}


void GameController::setLevel(QString file)
{
    //Save::instance().load();
    m_gameState = game;
    p_gameView->startUnfading();
    if (file.startsWith("./resources/levels/"))
    {
        m_levelFile = file;
    }
    else
    {
        m_levelFile = "./resources/levels/"+file;
    }
    p_gameWidget->setCurrentWidget(p_gameView);
    p_gameScene->clearScene();
    p_gameScene->loadLevel(m_levelFile);
    p_endLevelScreen->setPoster(p_gameScene->properties()->posterFile());
    p_playerptr = p_gameScene->m_player;
    p_headUp->setPlayer(p_playerptr);

    QObject::connect(  p_gameScene,
                     & GameScene::finishLevel,
                       this,
                     & GameController::levelFinished
                    );
    showGameView();    
}

void GameController::showGameView()
{
   p_gameWidget->setCurrentWidget(p_gameView);
   start();
}

void GameController::showEndLevelScreen()
{
    p_timer->stop();
    p_endLevelScreen->updateData(p_playerptr);
    p_gameWidget->setCurrentWidget(p_endLevelScreen);
}

void GameController::showSaveSelectWidget()
{
    p_gameWidget->setCurrentWidget(m_saveSelectWidget);
}


void GameController::pause()
{
    p_timer->stop();
    p_pauseMenu->setBackground(p_gameView->toImage());
    p_gameWidget->setCurrentWidget(p_pauseMenu);
    m_paused = true;
}

void GameController::stopGame()
{
   p_timer->stop(); 
   p_gameWidget->setCurrentWidget(p_mainMenu);
   m_paused = false;

}

void GameController::start()
{
    p_timer->stop();  
    if (wait)
    {
         Save::instance()->load();
         wait = false;
         p_timer->singleShot(500, this, SLOT(start()));
    }
    else
    {
         m_paused = false;
         p_timer->singleShot(m_timeDelta, this, SLOT(advance()));
    }
}

void GameController::startNextLevel()
{
    if (isLevelFinished)
    {
        showLevelPackWidget();
    }    
    start();
}

void GameController::continueGame()
{ 
    start();
    p_gameWidget->setCurrentWidget(p_gameView);
}


void GameController::levelFinished(Player::finishReason reason)
{
    m_gameState = finishwaiting;
    m_elapsedTime = 0;
    m_finishReason = reason;
    p_gameView->startFading();
}

void GameController::finishLevel()
{
    m_paused = true;
    p_timer->stop();   
    switch (m_finishReason)
    {
    case Player::succes :
    {
        p_playerptr->processLevelFinish();
        Save::instance()->fixState();
        showEndLevelScreen();
        QJsonObject finishdata;
        finishdata.insert("fileName", m_levelFile);
        finishdata.insert("coins", p_playerptr->coins());
        Save::instance()->addToFinished(finishdata);
        Save::instance()->saveCurrentGame();
        Save::instance()->clearSavables();
        p_gameScene->clearScene();
        isLevelFinished = true;

        break;
    }
    case Player::unsucces :
        p_playerptr->procesLevelFail();
        p_gameScene->clearScene();
        Save::instance()->clearSavables();
        isLevelFinished = false;
        if (p_playerptr->lives() > 0)
        {
            wait = true;
            setLevel(m_levelFile);
        }
        else
        {
            p_gameWidget->setCurrentWidget(p_mainMenu);
        }
        break;

    case Player::catchedByGribbo:
        p_playerptr->procesLevelFail();
        Save::instance()->clearSavables();
        p_gameScene->clearScene();
        isLevelFinished = false;
        m_levelFile = "dungeon.lvl";
        wait = true;
        setLevel(m_levelFile);
    break;
    }

}


void GameController::onRunEditor()
{
    EditorWindow* w = new EditorWindow;
    QObject::connect(w,
                     &EditorWindow::startTestLevel,
                     this,
                     &GameController::setLevel);
    w->show();
}
