#include "propertymodifiermanager.h"
#include "common/save.h"

PropertyModifierManager::PropertyModifierManager(Object *target)
{
    Save::instance()->registerSavable(this);
    Save::instance()->restoreState(this);
    m_target = target;

}

PropertyModifierManager::PropertyModifierManager()
{
    Save::instance()->registerSavable(this);
    Save::instance()->restoreState(this);
}

PropertyModifierManager::~PropertyModifierManager()
{
    Save::instance()->unregisterSavable(this);
}


void PropertyModifierManager::setup(QJsonObject &object)
{
    Object::setup(object);

    for (auto modifierProps : object.value("modifiers").toArray()) {
        QJsonObject props = modifierProps.toObject();
        PropertyModifier* modifier = (PropertyModifier*)Object::create(props);
        m_modifiers.append(modifier);
    }
}

void PropertyModifierManager::setTarget(Object *target)
{
    if (target == nullptr)
    {
        qDebug() << "PropertyModifierManager::SetTarget " << objectName() << " targer is null!";
        return;
    }
    m_target = target;
    Save::instance()->restoreState(this);
    applyAll();
}

void PropertyModifierManager::clearModifiers()
{
    foreach (auto* modifier, m_modifiers)
    {
        delete modifier;
    }
    m_modifiers.clear();
}

void PropertyModifierManager::addModifier(PropertyModifier *modifier)
{   
    if (modifier->isApplied()) return;
    if (modifier->timeMode() != PropertyModifier::Once)
        m_modifiers.append(modifier);
    applyEffect(modifier);

}

void PropertyModifierManager::advance(int timedelta)
{
    PropertyModifier* toRemove = nullptr;
    m_time += timedelta;
    for (auto& modifier : m_modifiers)
    {
        if (modifier->timeMode() == PropertyModifier::Timed)
        {
            modifier->addWorkTime(timedelta);
            if (modifier->workTime() > modifier->effectTime())
            {
                toRemove = modifier;
            }
        }
    }

    if (toRemove != nullptr)
    {
        disableEffect(toRemove);
        m_modifiers.removeAll(toRemove);
    }
}

void PropertyModifierManager::processLevelFailed()
{
    QList<PropertyModifier*> toRemoveList;
    for (auto& modifier : m_modifiers)
    {
        if (modifier->timeMode() == PropertyModifier::Timed || modifier->timeMode() == PropertyModifier::UntilFail)
        {
            toRemoveList.append(modifier);
        }
    }

    for(auto& modifier : m_modifiers)
    {
        disableEffect(modifier);
        m_modifiers.removeAll(modifier);
    }
}

void PropertyModifierManager::processLevelFinished()
{
    QList<PropertyModifier*> toRemoveList;
    for (auto& modifier : m_modifiers)
    {
        if (modifier->timeMode() != PropertyModifier::Infinite)
        {
            toRemoveList.append(modifier);
        }
    }

    for(auto& modifier : m_modifiers)
    {
        disableEffect(modifier);
        m_modifiers.removeAll(modifier);
    }
}

bool PropertyModifierManager::contains(PropertyModifier *modifier)
{
    if (modifier == nullptr || m_modifiers.isEmpty()) return false;
    return m_modifiers.contains(modifier);
}

void PropertyModifierManager::applyAll()
{
    if (m_target == nullptr)
    {
        qDebug() << "PropertyModifierManager::Apply " << objectName() << " targer is null!";
        return;
    }
    if (m_modifiers.isEmpty())
    {
        qDebug() << "PropertyModifierManager::Apply " << objectName() << " no modifiers";
        return;
    }

    for(auto& modifier : m_modifiers)
    {
        applyEffect(modifier);
    }

}

void PropertyModifierManager::disableAll()
{
    if (m_target == nullptr)
    {
        qDebug() << "PropertyModifierManager::Apply " << objectName() << " targer is null!";
        return;
    }
    if (m_modifiers.isEmpty())
    {
        qDebug() << "PropertyModifierManager::Apply " << objectName() << " no modifiers";
        return;
    }

    for (auto iter = m_modifiers.end(); iter >= m_modifiers.begin(); iter--)
    {
        disableEffect(*iter);
    }
}

void PropertyModifierManager::applyEffect(PropertyModifier *modifier)
{
    qDebug() << "PropertyModifierManager::applyEffect " << objectName() << modifier->name();
    if (m_target == nullptr)
    {
        qDebug() << "PropertyModifierManager::applyEffect " << objectName() << " targer is null!";
        return;
    }
    modifier->setApplied(true);
    modifier->setWorkTime(0);
    QByteArray propName = modifier->name().toLatin1();
    QVariant value = m_target->property(propName);

    double newVal = value.toDouble();

    switch (modifier->operation())
    {
    case PropertyModifier::Add:
        newVal += modifier->value();
        break;

    case PropertyModifier::Substract:
        newVal -= modifier->value();
        break;

    case PropertyModifier::Multiply:
        newVal *= modifier->value();
        break;

    case PropertyModifier::Divide:
        newVal /= modifier->value();
        break;

    default:
        break;
    }

    if (m_target->property(propName).type() == QVariant::Int)
    {
        m_target->setProperty(modifier->name(), QVariant(int(newVal)));
    }
    else
    {
        m_target->setProperty(modifier->name(), QVariant(newVal));
    }

}

void PropertyModifierManager::disableEffect(PropertyModifier *modifier)
{
    if (m_target == nullptr)
    {
        qDebug() << "PropertyModifierManager::disableEffect " << objectName() << " targer is null!";
        return;
    }

    QByteArray propName = modifier->name().toLatin1();
    QVariant value = m_target->property(propName);

    double newVal = value.toDouble();

    switch (modifier->operation())
    {
    case PropertyModifier::Add:
        newVal -= modifier->value();
        break;

    case PropertyModifier::Substract:
        newVal += modifier->value();
        break;

    case PropertyModifier::Multiply:
        newVal /= modifier->value();
        break;

    case PropertyModifier::Divide:
        newVal *= modifier->value();
        break;

    default:
        break;
    }

    if (m_target->property(propName).type() == QVariant::Int)
    {
        m_target->setProperty(modifier->name(), QVariant(int(newVal)));
    }
    else
    {
        m_target->setProperty(modifier->name(), QVariant(newVal));
    }
}
