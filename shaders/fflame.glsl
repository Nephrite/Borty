#version 130
#ifdef GL_ES

precision mediump int;
precision mediump float;
#endif

uniform sampler2D texture0;
uniform sampler2D texture1;
uniform sampler2D texture2;
varying vec2 v_texcoord;


uniform float time;
uniform float arg1;
void main()
{

   // color.rgb *= color.aaa;
    vec2 coord = v_texcoord;

    coord.y += time;
    vec4 value = texture2D(texture2, coord);

    vec2 st = v_texcoord;
    st.x += sin(st.y*16+time*16)/16;
    coord.y += (time/2);
    vec4 color = texture2D(texture0, coord);

    vec4 phase = texture2D(texture1, st);
    color *= phase*value;
    color.a *= (value.r*phase.r);
    gl_FragColor =color ;
}
