#include "propertymodifier.h"

PropertyModifier::PropertyModifier()
{

}

PropertyModifier::PropertyModifier(QString &name, int value)
{
    m_isApplied = false;
    m_name = name;
    m_value =  value;
    m_timeMode = Once;
    m_operation = Add;
}

void PropertyModifier::addWorkTime(int time)
{
    m_workTime += time;
}
PropertyModifier::PropertyModifier(const char* name, int value)
{
    m_isApplied = false;
    m_name = QString(name);
    m_value =  value;
    m_timeMode = Once;
    m_operation = Add;
}
