#ifndef MOVINGPLATFORM_H
#define MOVINGPLATFORM_H

#include "editor/items/baseitem.h"

#include <QMouseEvent>

class QPainter;
class PlatformHandleItem :  public QGraphicsObject
{
    Q_OBJECT

public:

    explicit PlatformHandleItem(QGraphicsItem* parent =0);
    explicit PlatformHandleItem(float x, float y, int width, int height, QPixmap *pix);
    ~PlatformHandleItem();
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void setSize(float nw, float nh);
    void setPixmap(QPixmap* pix){pixmap = pix;}
    enum { Type = UserType + 1 };
    int type() const {return Type;}
    QRectF boundingRect() const;
    QVariant itemChange(GraphicsItemChange change, const QVariant &value);
    QPixmap *pixmap;
    float w;
    float h;
signals:
    void itemMoved(float x, float y);
    void deleted();


};



class MovingPlatformItem : public BaseItem
{
    Q_OBJECT

    Q_PROPERTY(double velocity  READ velocity   WRITE setVelocity DESIGNABLE true USER true)
    Q_PROPERTY(double xDistance READ xDistance  WRITE xDistance DESIGNABLE true USER true)
    Q_PROPERTY(double yDistance READ yDistance  WRITE yDistance DESIGNABLE true USER true)

public:
    explicit MovingPlatformItem();
    explicit MovingPlatformItem(QVariantMap* properties);
    ~MovingPlatformItem();  
    bool hasHandles(){return true;}
    void setupHandles();
    void updateProperties();

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void setBehaviour(ItemBehaviour* b);
    QRectF boundingRect();

    double velocity() const
    {
        return m_velocity;
    }  
    double xDistance() const
    {
        return m_xDistance;
    }

    double yDistance() const
    {
        return m_yDistance;
    }

public slots:

    void onHandleDeleted();
    void setXYDistance(int x, int y);

    void setVelocity(double velocity)
    {
        m_velocity = velocity;
    }   

    void xDistance(double xDistance)
    {
        m_xDistance = xDistance;
    }

    void yDistance(double yDistance)
    {
        m_yDistance = yDistance;
    }

private:
    double m_velocity;
    double m_xDistance;
    double m_yDistance;
    PlatformHandleItem* platformHandle;
};

#endif // MOVINGPLATFORM_H
