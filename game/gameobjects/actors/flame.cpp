#include "flame.h"
#include <QDateTime>

Flame::Flame()
{

}

Flame::Flame(QJsonObject &parameters) : BaseGameObject(parameters)
{
    setup(parameters);
    setupSprite();
}

Flame::~Flame()
{

}

void Flame::setupSprite()
{
    m_sprite.setBindAllTextures(true);
    m_sprite.setShaderName("flame");
    m_sprite.shaderArgs()->arg1 = (GLfloat)(rand()%100/100.0);
    m_sprite.addTexture("./resources/textures/"+m_textureStyle+'/'+m_stencilTexture, QOpenGLTexture::Nearest);
    m_sprite.currentTexture()->setWrapMode(QOpenGLTexture::ClampToEdge);
    m_sprite.addTexture("./resources/textures/"+m_textureStyle+'/'+m_sparcleTexture, QOpenGLTexture::Nearest);
}



void Flame::advance(int timedelta)
{
   if (!isOnScreen()) return;
   m_sprite.shaderArgs()->time += timedelta;
}
