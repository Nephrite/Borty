#ifndef TOOLTIPITEM_H
#define TOOLTIPITEM_H

#include "editor/items/baseitem.h"
#include <QColor>

class TooltipItem : public BaseItem
{
    Q_OBJECT

    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS(int, lifetime,  setLifeTime)
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS(int, delay,  setDelay)
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS(bool, attachToCenter,  setAttachToCenter)
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS(QColor, textColor,  setTextColor)
    Q_PROPERTY(QString text READ text WRITE setText  DESIGNABLE true USER true)

public:

    TooltipItem();
    TooltipItem(QVariantMap *newProperties);
    ~TooltipItem();
    QString text() const;
    void setText(const QString &text);

protected:
    void generate();
    void paintObject(QPainter* painter);
    QString m_text;

};

#endif // TOOLTIPITEM_H
