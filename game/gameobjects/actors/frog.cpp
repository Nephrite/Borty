#include "frog.h"
#include "common/objecttypes.h"
#include "../player.h"
frog::frog(QJsonObject& parameters, b2World* world) : PhysicsObject(parameters, world)
{

    m_physicsType = b2_dynamicBody;   
    time = qrand()%4000;
    m_objectType = type::frog;

    if ((qrand()%2) == 1)
        impulse.Set(0.4,0);
    else
        impulse.Set(-0.4,0);
    body->SetLinearVelocity(impulse);
    body->SetType(b2_dynamicBody);
    m_jumpForce =  1.5; // jump impulse
    collisionList.append(type::Player);
}


void frog::advance(int timeDelta)
{
    BaseGameObject::advance(timeDelta);
    time+=timeDelta;

    if (equalZero(body->GetLinearVelocity().x)) // if stiked change direction
    {
        impulse.x = -(impulse.x);
    }
    if (time > 4000) // time to jump
    {
        body->ApplyLinearImpulse(b2Vec2(impulse.x, -m_jumpForce), body->GetLocalCenter());
        time = 0;
    } else if (equalZero(body->GetLinearVelocity().y - lastY) )
    {
        body->SetLinearVelocity(impulse);
    }
    lastY = body->GetLinearVelocity().y ;
}

void frog::solveCollision(PhysicsObject *o)
{

}


frog::~frog()
{  
}
