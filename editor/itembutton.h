#ifndef EDITOROBJECTS_H
#define EDITOROBJECTS_H

#include <QToolButton>
#include <QJsonObject>
#include "items/baseitem.h"


class ItemButton : public QToolButton
{
    Q_OBJECT

public:

    ItemButton();
    ItemButton(QJsonObject object);
    ~ItemButton();
    QJsonObject properties();     
    void mouseReleaseEvent(QMouseEvent *e);
    QPixmap icon;
    QSize sizeHint(){return QSize(128,70);}   
    void setIconFile(QString);
    QString objectStyle();

private:

    QJsonObject _properties;
    QString m_objectsStyle;

signals:

    void sendProperies(QJsonObject* properties);
};

#endif // EDITOROBJECTS_H
