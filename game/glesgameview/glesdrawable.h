#ifndef DRAWABLE_H
#define DRAWABLE_H
#include <QOpenGLTexture>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include <QVector2D>
#include <QVector3D>
#include "glutils.h"
#include "common/object.h"

struct ShaderArguments
{
    ShaderArguments()
        : time(0),
          arg1(0),
          arg2(0),
          arg3(0)
    {}
    GLfloat time;
    GLfloat arg1;
    GLfloat arg2;
    GLfloat arg3;
};

class Drawable : public Object
{
    Q_OBJECT
    PROMOTED_PROPERTY(GLfloat, double, opacity, setOpacity);

    PROPERTY(QString, shaderName, setShaderName);
    PROPERTY(bool, bindAllTextures,  setBindAllTextures);

    Q_PROPERTY(int frameCount READ frameCount WRITE setFrameCount USER true DESIGNABLE true)
    Q_PROPERTY(int rowCount READ rowCount WRITE setRowCount USER true DESIGNABLE true)
    Q_PROPERTY(int columnCount READ columnCount WRITE setColumnCount USER true DESIGNABLE true)
    // from QObject
    Q_PROPERTY(QString objectName READ objectName WRITE setObjectName USER true DESIGNABLE true)

public:
    Drawable();
   // explicit Drawable(QObject* parent = nullptr);
    virtual  ~Drawable();
    void setAsBox(b2Vec2 size);
    void setAsBox(double, double);
    void initGeometry();
    void draw(GlUtils *);
    void drawBinded(GlUtils *);
    void selectTexture(int id);
    void setOffset(double x, double y);

    QOpenGLTexture* currentTexture(){return m_textures[m_currentTexture];}
    double bottomY();
    enum Mapping // TODO: remove, use flipping
    {
        Normal,
        VerticalFlipped
    };
    enum TextureMappingType
    {
        StretchedMapping,
        TiledMapping
    };
    enum FrameOrder
    {
        Direct, // 1 2 3
        Reverse,// 3 2 1
        Loop    // 1 2 3 2 1
    };
    TextureMappingType m_horizontalMapping;
    TextureMappingType m_verticalMapping;
    int addTexture(QString file, QOpenGLTexture::Filter filter);
    int addTexture(QString file);
    int addTexture(QPixmap &image);
    void setTexMappingType(Mapping m);
    void setPos(b2Vec2 p){m_pos = p;}
    void setPos(float x, float y){m_pos = b2Vec2(x,y);}
    void setPos(QPointF p){m_pos = b2Vec2(p.x(),p.y());}
    void setAngle(GLfloat angle){m_angle = angle;}
    void setTililingType(TextureMappingType tx,TextureMappingType ty);
    void setTililingType(int tx,int ty);
    void setTiling(QPointF t){m_tiling = t;}
    void setTiling(double x, double y){m_tiling = QPointF(x,y);}
    void setFlipping(double x, double y);
    void setVerticalFlipped(bool flipped);
    void setHorizontalFlipped(bool flipped);
    void setZvalue(float z){m_zValue = z;}
    void setFrameCount(int c);
    void setRowCount(int c);
    void setColumnCount(int c);
    void setCurrentFrame(int f);
    void setNextFrame(FrameOrder order);
    void turn(double angle);  
    void addOpacity(double o){ m_opacity += o;}

    int frameCount(){return m_frameCount;}
    int rowCount(){return m_rowCount;}
    int columnCount(){return m_columnCount;}
    int currentFrame(){return m_currentFrame;}

    double opacity(){return m_opacity;}

    float zValue(){return m_zValue;}
    GLfloat angle(){return m_angle;}
    b2Vec2& position(){return m_pos;}

    ShaderArguments* shaderArgs(){return &m_shaderArguments;}
    Mapping m_mapping;
    b2Vec2 m_size;
    int m_elCount;
    b2Vec2 m_pos;
    VertexData m_vertices[4];
    GLushort indices[6];

    ShaderArguments m_shaderArguments;
    QOpenGLBuffer VertexArrayBuf;
    QOpenGLBuffer indexBuf;
    GLfloat m_angle;
    QPointF m_tiling;
    QPointF m_flipping;
    b2Vec2 m_offset;

    void bind(GlUtils *glutils);
    void bind();
    void lookHere(GlUtils* glutils);
    void release();

protected:
    int m_frameCount;
    int m_columnCount;
    int m_rowCount;
    int m_currentFrame;
    int m_currentRow;
    int m_currentColumn;
    void updateFrameCoord();


    float m_zValue;
    void updateTiling();

    int m_currentTexture=0;
    QVector<QOpenGLTexture*> m_textures;
    bool m_reverseFrameOrder;
};

static int drawableId = qRegisterMetaType<Drawable>("Drawable");

//Q_DECLARE_METATYPE(Drawable)
#endif // DRAWABLE_H
