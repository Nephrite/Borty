#ifndef AMMOITEM_H
#define AMMOITEM_H

#include <QObject>
#include "editor/items/baseitem.h"
#include "common/objecttypes.h"

class AmmoItem : public BaseItem
{
    Q_OBJECT
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS (type::bullets, bulletType, setBulletType)
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS(int, count, setCount)
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS(bool, staticView, setStaticView)

public:    
    AmmoItem(QVariantMap* proprties);
    ~AmmoItem();
};

#endif // AMMOITEM_H
