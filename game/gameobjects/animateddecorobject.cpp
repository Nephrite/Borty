#include "animateddecorobject.h"

AnimatedDecorObject::AnimatedDecorObject() :
    m_rows(1),
    m_columns(1),
    m_interval(1000),
    m_firstFrame(0),
    m_frameCount(1),
    m_time(0)
{

}

AnimatedDecorObject::AnimatedDecorObject(QJsonObject &parameters) :
    BaseGameObject(parameters),
    m_rows(1),
    m_columns(1),
    m_interval(1000),
    m_firstFrame(0),
    m_frameCount(1),
    m_time(0)
{
    setup(parameters);
    if (m_firstFrame == -1)
    {
        m_firstFrame = rand()%m_frameCount;
        m_time = rand()%m_interval;
    }

    setupSprite();

    //qDebug()<< "Animated object " << m_rows << m_columns << m_frameCount;
}

void AnimatedDecorObject::advance(int timeDelta)
{
    BaseGameObject::advance(timeDelta);
    m_time += timeDelta;
    if (m_time > m_interval)
    {
        m_sprite.setNextFrame(Drawable::Loop);
        m_time = 0;
    }
}

void AnimatedDecorObject::setupSprite()
{
    BaseGameObject::setupSprite();
    m_sprite.setTiling(1.0,1.0);
     m_sprite.setFrameCount(m_frameCount);
    m_sprite.setRowCount(m_rows);
    m_sprite.setColumnCount(m_columns);

    m_sprite.setCurrentFrame(m_firstFrame);
    m_sprite.setShaderName("animatedSprite");
}
