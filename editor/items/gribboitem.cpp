#include "gribboitem.h"

#include <QPainter>
#include <QDebug>
GribboItem::GribboItem(QGraphicsItem *parent) :
    BaseItem(parent),
    m_gribboType(simple)
{

}

GribboItem::GribboItem(QVariantMap *newProperties) :
    BaseItem(newProperties),
    m_gribboType(simple)
{
    setup(newProperties);
    m_width = pixmap.width()/5;
    m_height = pixmap.height()/2;
}

GribboItem::~GribboItem()
{

}

void GribboItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->save();
    painter->translate(-width()/2.0, -height()/2.0);

    paintObject(painter);
    paintHelpers(painter);

    painter->restore();
}

void GribboItem::paintObject(QPainter *painter)
{
    painter->drawTiledPixmap(0, 0, width(), height(), pixmap,0,0);
}

void GribboItem::updateGribboType()
{
    switch (m_gribboType) {
    case simple:
        m_texture = "gribbo_sheet_default.png";
        break;

    case withBag:
        m_texture = "gribbo_sheet_bag.png";
        break;
    case withFlute:
        m_texture = "gribbo_sheet_flute.png";
        break;
    default:
        break;
    }
    pixmap = QPixmap("./resources/textures/enemy/"+m_texture);
    update(boundingRect());
}
