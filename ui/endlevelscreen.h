#ifndef ENDLEVELSCREEN_H
#define ENDLEVELSCREEN_H

#include <QWidget>
#include <QKeyEvent>
#include <game/gameobjects/player.h>

namespace Ui {
class EndLevelScreen;
}

class EndLevelScreen : public QWidget
{
    Q_OBJECT

public:
    explicit EndLevelScreen(QWidget *parent = 0);
    void keyPressEvent(QKeyEvent* e);
    void updateData(Player* p);
    void setPoster(QString file);
    ~EndLevelScreen();
signals:
    void goToMenu();
    void nextLevel();

private slots:
    void on_okButton_clicked();

private:
    Ui::EndLevelScreen *ui;
};

#endif // ENDLEVELSCREEN_H
