#ifndef PLAYER_H
#define PLAYER_H
#include "actors/actor.h"
#include "sensor.h"
#include "common/objecttypes.h"

#include "playerstatistics.h"

#include "propertymodifier.h"
#include "propertymodifiermanager.h"

class QKeyEvent;


struct KeyData
{
    void keyData() {}
    bool upPressed    = false;
    bool rightPresed  = false;
    bool downPresed   = false;
    bool leftPresed   = false;
    bool spacePressed = false;
    bool enterPressed  = false;
    bool ePressed   = false;
};

class Player : public Actor
{
    Q_OBJECT

    Q_PROPERTY(int health READ health WRITE setHealth )
    Q_PROPERTY(int lives READ lives WRITE setLives )
    Q_PROPERTY(int bullets READ bullets WRITE setBullets )
    Q_PROPERTY(int coins READ coins WRITE setCoins )

    enum states{normal, catched};

public:

    void glDraw(GlUtils *glutils);
    Player(QJsonObject &properties, b2World*);
    Player();
     ~Player();

    void keyEvent(int type, QKeyEvent*);
    void advance(int timeDelta);    
    void solveCollision(PhysicsObject *obj);   
    void preSolve(PhysicsObject* obj, b2Contact* contact);  

    bool isAboveOf(BaseGameObject* other);
    void processLevelFinish();
    void procesLevelFail();

    QVector<int>* ammo();
    type::bullets currentAmmo();
    type::bullets defaultAmmo();
    void setDefaultAmmo(type::bullets a){m_defaultAmmo = a;}
     void setDefaultAmmo(QString a);
    void gribboCatch();

    double bottomY();

    enum finishReason{
        succes,
        unsucces,
        catchedByGribbo
    };
    b2Vec2 speed(){return m_speed;}   
    bool processDamage(PropertyModifier *modifier);

    int health()  const{ return m_stat.health(); }
    int lives()   const{ return m_stat.lives();  }
    int bullets() const{ return m_bullets;}
    int coins()   const{ return m_stat.coins();  }

    PlayerStatistics* statistics(){return &m_stat;}



public slots:

    void setHealth(int health)   { m_stat.setHealth(health); }
    void setLives(int lives)     { m_stat.setLives(lives);   }
    void setBullets(int bullets) { m_bullets = bullets; }
    void setCoins(int coins)     { m_stat.setCoins(coins); }

protected:
    void emitBullet();
    void catchAdvance();

    KeyData m_keyData;

    states m_state;

    void updateDirections();
    float startJumpY;

    b2Vec2 transform;
    b2Vec2 m_lastPos;
    b2Vec2 m_speed;

    int m_bullets;   

    int m_lastBulletTime;
    bool m_rightWatching;
    int m_timer;

    QVector<int> m_ammo;
    type::bullets m_currentAmmo;
    type::bullets m_defaultAmmo;
    void addAmmo(type::bullets t, int count);
    void selectNextAmmo();
    PlayerStatistics m_stat;
    PropertyModifierManager m_propertyModifierManager;
signals:
    void finishLevel(finishReason);
};


#endif // PLAYER_H
