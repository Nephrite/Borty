
#include <QDebug>
#include <QFile>

#include <QStringList>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>

#include "save.h"
#include "object.h"

Save Save::Instance;

Save* Save::instance()
{
    return &Instance;
}

void Save::registerSavable(Object *obj)
{
    if (!m_savables.contains(obj))
    {
        qDebug() << "registerSavable" << obj->objectName();
        m_savables.append(obj);
    }
}

void Save::unregisterSavable(Object *obj)
{
    if (m_savables.contains(obj))
    {
        qDebug() << "unregisterSavable" << obj->objectName();
        m_savables.removeAll(obj);
    }
}

void Save::restoreState(Object *obj)
{
    QJsonObject&& props = m_mainObject.value(obj->objectName()).toObject();
    obj->setup(props);
}

void Save::clearSavables()
{
    qDebug() << "clearSavables";
    m_savables.clear();
}

Save::Save() : m_saved(false), m_loaded(false)
{

}

Save::~Save()
{

}


void Save::selectSave(const QString &saveName)
{
    m_saveName = saveName;
    m_saveFileName = QString("./resources/saves/" + m_saveName + ".sav");
}

bool Save::openSaveFile()
{
    qDebug() << "Open save" << m_saveFileName;
    if (m_saveFile.isOpen())
        m_saveFile.close();

    m_saveFile.setFileName(m_saveFileName);
    if (m_saveFile.size() < 2)
    {
        initSaveDocument();
        saveCurrentGame();
    }

    return (m_saveFile.open(QFile::ReadOnly));
}

void Save::initSaveDocument()
{
    qDebug() << "Init save";
    QJsonObject main;

    QJsonArray levels;   
    main.insert("finishedLevels", levels);

    m_saveDocument.setObject(main);
}

void Save::fixState()
{
     qDebug()<<"fixState";
    for (Object* obj : m_savables)
    {
        qDebug()<<"fixed "<< obj->objectName();
        m_mainObject.insert(obj->objectName(), Object::toJsonObject(obj));
    }
}

void Save::load()
{
    if (!m_saved)
        qDebug()<<"Game not saved!";

    if (openSaveFile())
    {        
        m_saveDocument = QJsonDocument::fromJson(m_saveFile.readAll());
        m_mainObject = m_saveDocument.object();
        loadFinishedList();
    }
    else qDebug()<<"save file not opened! " << m_saveFile.fileName();

    if (m_finishedLevels.isEmpty())
        qDebug()<<"Finished level list is empty!";
}

void Save::saveCurrentGame()
{
    fixState();
    m_saveDocument.setObject(m_mainObject);
    if (m_saveFile.isOpen())
        m_saveFile.close();
    m_saveFile.setFileName(m_saveFileName);
    qDebug() << "file " << m_saveFileName;
    QByteArray backup;

    if (m_saveFile.open(QFile::ReadOnly))
    {
        backup = m_saveFile.readAll();
        m_saveFile.close();
        m_saveFile.remove(m_saveFileName);
    }
    else
    {
        qDebug()<<"Can't open save file " << m_saveFile.fileName();
        return;
    }

    m_saveFile.setFileName(m_saveFileName);
    if ( !m_saveFile.open(QFile::ReadWrite))
    {
        qDebug()<<"Can't open save file " << m_saveFile.fileName();
        return;
    }
    if (m_saveFile.write(m_saveDocument.toJson()) == -1)
    {
        QFile backupFile("./resources/saves/" + m_saveName + "_backup.sav");
        if (backupFile.open(QFile::WriteOnly))
            backupFile.write(backup);
        backupFile.close();
    }
    else
    {
        m_saved = true;
        qDebug()<< "saved";
    }
    m_saveFile.flush();
    m_saveFile.close();
}

QStringList& Save::saves()
{
    return m_savedGames;

}

void Save::loadFinishedList()
{
    QJsonArray levelsData = m_mainObject.value("finishedLevels").toArray();
   //   qDebug()<<"loadFinishedList: " << levelsData;
    m_finishedLevels.clear();

    for (auto value : levelsData)
    {
        //qDebug()<<"finished: " << value;
        m_finishedLevels.append(value.toObject().value("fileName").toString());
    }
}

bool Save::levelIsFinished(const QString& fileName)
{  
    return m_finishedLevels.contains("./resources/levels/"+fileName, Qt::CaseInsensitive);
}

void Save::addToFinished(const QJsonObject& data)
{
    qDebug() << "added finished" << data.value("fileName").toString();


    m_finishedLevels.append(data.value("fileName").toString());

    QJsonArray levelsData(m_mainObject.value("finishedLevels").toArray());
    levelsData.append(data);
    //qDebug() << "Levels data" << levelsData;

    m_mainObject.remove("finishedLevels");
    m_mainObject.insert("finishedLevels", levelsData);

    m_saveDocument.setObject(m_mainObject);
    m_saved = false;
}

QJsonObject Save::value(QString name)
{
    return m_mainObject.value(name).toObject();
}

void Save::setValue(QString name, QJsonObject value)
{
    m_mainObject.insert(name, value);
}
