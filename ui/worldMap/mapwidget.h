#ifndef MAPWIDGET_H
#define MAPWIDGET_H

#include <QWidget>
#include <QJsonArray>
#include <QJsonObject>
#include <QPropertyAnimation>
#include <QTimer>

struct PointData
{
    QPoint  point;
    bool finished;
};

class MapWidget : public QWidget
{
    Q_OBJECT
    Q_PROPERTY(QPoint cursorPos READ cursorPos WRITE setCursorPos)

public:

    explicit MapWidget(QWidget *parent = nullptr);
    ~MapWidget();
    void paintEvent(QPaintEvent* e);
    void selectNext();
    void selectPrevious();
    void addPoint(QPoint point, bool finished);
    QPoint cursorPos() const;
    void setCursorPos(const QPoint &cursorPos);
    void clear();
public slots:

    void cursorPosChanged();

protected:
    void startAnimation();
    void setupMapPoints();
    int m_levelIdx;
    QList<PointData> m_mapPoints;

    QPoint m_cursorPos;
    QPoint m_cursorTarget;
    QPointF m_scale;
    QPixmap m_mapPixmap;
    QPixmap m_cursor;
    QPixmap m_mapPoint;
    QPixmap m_mapPointFinished;
    QTimer m_timer;
    QPropertyAnimation* m_animation;

};

#endif // MAPWIDGET_H
