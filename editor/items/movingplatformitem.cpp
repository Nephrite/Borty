#include "movingplatformitem.h"
#include "common/objecttypes.h"

#include <QPainter>
#include <QGraphicsScene>
#include <QDebug>
#include <QJsonObject>


PlatformHandleItem::PlatformHandleItem(QGraphicsItem *parent) : QGraphicsObject(parent)
{
    setParentItem(parent);
    setFlags(QGraphicsItem::ItemIsMovable
                    |QGraphicsItem::ItemIsSelectable
                        | QGraphicsItem::ItemSendsScenePositionChanges);
}
PlatformHandleItem::PlatformHandleItem(float x, float y, int width, int height, QPixmap* pix)
{
    setPos(x, y);
    w = width;
    h = height;
    pixmap = pix;
    setFlags(QGraphicsItem::ItemIsMovable
                    |QGraphicsItem::ItemIsSelectable
                        | QGraphicsItem::ItemSendsScenePositionChanges);


}
void PlatformHandleItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);
    painter->drawPixmap(0,0, w, h, *pixmap);
}

void PlatformHandleItem::setSize(float nw, float nh)
{
    w = nw;
    h = nh;
}

PlatformHandleItem::~PlatformHandleItem()
{  
   emit deleted();
}

QRectF PlatformHandleItem::boundingRect() const
{
    return QRectF(0, 0, w, h);
}

QVariant PlatformHandleItem::itemChange(GraphicsItemChange change, const QVariant &value)
{   
    if (change == ItemPositionChange)
        emit itemMoved( value.toPointF().x(), value.toPointF().y());
    return QGraphicsItem::itemChange(change, value);
}

//////////////////////////////////////////////////

MovingPlatformItem::MovingPlatformItem()
{

}

MovingPlatformItem::~MovingPlatformItem()
{    
    if(platformHandle != NULL) platformHandle->deleteLater();
}


void MovingPlatformItem::setupHandles()
{
    platformHandle = new PlatformHandleItem(this);
    platformHandle->setPos(m_xDistance, m_yDistance);
    platformHandle->setSize(width(), height());
    platformHandle->setPixmap(&pixmap);
    setFlags(QGraphicsItem::ItemIsMovable
                    |QGraphicsItem::ItemIsSelectable
                         | QGraphicsItem::ItemSendsScenePositionChanges);
    QObject::connect(
                platformHandle,
                &PlatformHandleItem::deleted,
                this,
                &MovingPlatformItem::onHandleDeleted
                );
    QObject::connect(
                platformHandle,
                &PlatformHandleItem::itemMoved,
                this,
                &MovingPlatformItem::setXYDistance
                    );

}

MovingPlatformItem::MovingPlatformItem(QVariantMap *properties) : BaseItem(properties)
{
    setup(properties);
    m_objectType = type::MovingPlatform;
    m_velocity = 1;
    pixmap.load("./resources/textures/" + m_textureStyle + '/' + m_texture);
}

void MovingPlatformItem::onHandleDeleted()
{
    deleteLater();
    platformHandle = NULL;
}

void MovingPlatformItem::setXYDistance(int x, int y)
{
    m_xDistance = x;
    m_yDistance = y;
    update();
}


void MovingPlatformItem::updateProperties()
{
    platformHandle->w = m_width;
    platformHandle->h = m_height;
    platformHandle->setPos(mapFromItem(this, xDistance(), yDistance()));
}

void MovingPlatformItem::setBehaviour(ItemBehaviour *b)
{
    BaseItem::setBehaviour(b);   
}


QRectF MovingPlatformItem::boundingRect()
{
    QRectF rect(0,0,width(), height());
    return rect;
}

void MovingPlatformItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);
    if (platformHandle == NULL) return;    

    painter->drawPixmap(0,0, width(), height(),pixmap);  
    painter->setPen(QPen(Qt::blue, 3));
    painter->drawRect(boundingRect());
    painter->setPen(QPen(Qt::red, 1));
    painter->drawLine( 0
                      ,0
                      , platformHandle->pos().x()
                      , platformHandle->pos().y()
                      );  
    painter->drawLine(
                  width()
                , height()
                , platformHandle->pos().x()+width()
                , platformHandle->pos().y()+height()
                );  
}
