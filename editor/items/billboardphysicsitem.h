#ifndef BILLBOARDPHYSICSITEM_H
#define BILLBOARDPHYSICSITEM_H

#include "editor/items/baseitem.h"
#include <common/objecttypes.h>

class BillBoardPhysicsItem : public BaseItem
{
    Q_OBJECT
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS(double,  physicsWidth,   setPhysicsWidth)
    Q_PROPERTY_DESIGNABLE_WITH_ACCESSORS(double,  physicsHeight,   setPhysicsHeight)

public:
    BillBoardPhysicsItem();
    BillBoardPhysicsItem(BaseItem *parent = 0);
    BillBoardPhysicsItem(QVariantMap *newProperties);
    ~BillBoardPhysicsItem();

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

};

#endif // BILLBOARDPHYSICSITEM_H
