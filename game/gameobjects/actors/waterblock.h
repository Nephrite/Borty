#ifndef WATERBLOCK_H
#define WATERBLOCK_H



#include <QJsonObject>
#include <QVector>
#include <Box2D/Dynamics/b2World.h>


#include "game/gameobjects/decorobject.h"
#include <QRectF>


class WaterBlock : public decorObject
{
    Q_OBJECT

public:

    WaterBlock();
    WaterBlock(QJsonObject& parameters, b2World*);
    ~WaterBlock();  
    void advance(int timeDelta);
protected:
    void glDraw(GlUtils *glutils);
    GLuint mainTex;
    GLuint rippleTex;
    GLuint stencilTex;
    GLfloat time;
    QVector2D m_offset;
private:

};

#endif // WATERBLOCK_H
