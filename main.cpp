#include <QApplication>
#include "mainwindow.h"
#include <QTextCodec>
#include <QFontDatabase>
#include <QMessageBox>
#include <QTime>

int main(int argc, char *argv[])
{
    srand(QTime(0,0,0).secsTo(QTime::currentTime()));
    QTextCodec *codec = QTextCodec::codecForName("utf-8");  
    QTextCodec::setCodecForLocale(codec);
    
    QApplication a(argc, argv);

    // increase font size on windows
 #ifdef Q_OS_WIN32

    QFont globalfont = QApplication::font();
    globalfont.setPointSize(globalfont.pointSize()+4);
    QApplication::setFont(globalfont);

#endif

    // set font for game window
    int id = QFontDatabase::addApplicationFont("./resources/FSEX300.ttf");
    QString family = QFontDatabase::applicationFontFamilies(id).at(0);
    QFont font(family);
    font.setStyleStrategy(QFont::NoAntialias);
    MainWindow w;
    w.setFont(font);

    QFile style;
    style.setFileName("./resources/style.qss");
    if (style.open(QFile::ReadOnly))
    {
        w.setStyleSheet(style.readAll());
    }

    if (a.arguments().count() > 2) w.startLevel(a.arguments().at(2));
    w.show();

    return a.exec();
}
