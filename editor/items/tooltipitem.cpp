#include "tooltipitem.h"
#include <QPainter>

#include <QDebug>

TooltipItem::TooltipItem() :
    BaseItem(),
    m_lifetime(100),
    m_delay(0),
    m_attachToCenter(true),
    m_textColor(Qt::black),
    m_text("no text")
{
    generate();
}

TooltipItem::TooltipItem(QVariantMap* properties) :
    BaseItem(properties),
    m_lifetime(100),
    m_delay(0),
    m_attachToCenter(true),
    m_textColor(Qt::black),
    m_text("no text")

{
    setup(properties);

    generate();

}


TooltipItem::~TooltipItem()
{

}

void TooltipItem::generate()
{
    pixmap.load("./resources/textures/"+m_textureStyle+'/'+m_texture);
    m_height = pixmap.height();
    m_width = pixmap.width();
    QTextOption format;
    format.setAlignment(Qt::AlignCenter);
    format.setWrapMode(QTextOption::WrapAtWordBoundaryOrAnywhere);
    QPainter pa(&pixmap);
    pa.setPen(m_textColor);

    pa.drawText(QRectF(20,
                       20,
                       pixmap.width()-20,
                       pixmap.height()-20),
                m_text,
                format
                );
}

void TooltipItem::paintObject(QPainter *painter)
{

    painter->drawPixmap(0, 0, m_width, m_height, pixmap);
}

void TooltipItem::setText(const QString &text)
{
    if (text != m_text)
    {
        m_text = text;
        generate();
    }
}

QString TooltipItem::text() const
{
    return m_text;
}
