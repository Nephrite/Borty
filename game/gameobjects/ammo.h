#ifndef AMMO_H
#define AMMO_H

#include <QObject>
#include "common/objecttypes.h"
#include "game/gameobjects/physicsobject.h"


class Ammo : public PhysicsObject
{
    Q_OBJECT
    Q_PROPERTY_WITH_ACCESSORS(int, bulletType, setBulletType)
    Q_PROPERTY_WITH_ACCESSORS(int, count, setCount)
    Q_PROPERTY_WITH_ACCESSORS(bool, staticView, setStaticView)

public:
    Ammo();
    Ammo(QJsonObject& parameters, b2World* world);
    ~Ammo();
    double bottomY();
    void disable();
    void glDraw(GlUtils* glutils);    
protected:
    int m_frame;
    Drawable m_shadowSprite;
};

#endif // AMMO_H
