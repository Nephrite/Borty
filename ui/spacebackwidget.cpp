#include "spacebackwidget.h"
#include <QPainter>
#include <QDebug>
#include <math.h>

SpaceBackWidget::SpaceBackWidget(QWidget *parent) : QWidget(parent), m_frame(0)
{

    back.load("./resources/menu_image.png");
    stars.resize(100);
    starColorMap.load("./resources/colorTemp.png");
    for (auto &star : stars)
    {
        star.position.setX(float(rand())/float(RAND_MAX));
        star.position.setY(float(rand())/float(RAND_MAX));
    }
}


void SpaceBackWidget::paintEvent(QPaintEvent *e)
{
    m_frame++;
    QPainter paint(this);

    paint.fillRect(geometry(), Qt::black);

    QPointF size(width(), height());
    for (Star &star : stars)
    {
        paint.setPen(star.pen);
        paint.drawPoint(star.position.x()*size.x(),
                        star.position.y()*size.y()
                        );
    }
    paint.drawImage(width()/2+(cos(m_frame/120.0)*40.0),
                    50+(sin(m_frame/48.0)*20.0),
                    back
                    );

    QWidget::paintEvent(e);
}

void SpaceBackWidget::showEvent(QShowEvent *e)
{
    if (m_paused)
    {
        m_paused = false;
        advance();
    }
    QWidget::showEvent(e);
}

void SpaceBackWidget::advance()
{
    if ( m_paused )
    {
        if (timer.isActive()) timer.stop();
        return;
    }
    if (!isHidden() && !m_paused )
    {
        QPoint p;
        for (Star &star : stars)
        {
            p = QPoint(int(star.position.x()*width()-1),
                       int(star.position.y()*height()-1));

            star.position.setY(star.position.y()+ star.ySpeed);
            star.position.setX(star.position.x()+ star.xSpeed);


            if (star.position.y() < 0)
            {
                star.position.setY(1.0);
            }
            else if (star.position.y() > 1.0)
            {
                star.position.setY(0);
            }
            if (star.position.x() < 0)
            {
                star.position.setX(1.0);
            }
            else if (star.position.x() > 1.0)
            {
                star.position.setX(0);
            }

            star.step += star.blinkSpeed;

            if (star.step > starColorMap.width() - 1)
            {
                star.step = 1;
            }
            star.pen.setColor(starColorMap.pixel(star.step, 0));
        }
        timer.singleShot(30, this, SLOT(advance()));
        update();
    }
}

void SpaceBackWidget::resizeEvent(QResizeEvent* e)
{
    QWidget::resizeEvent(e);

    backScaled = back.scaled(
                width(),
                height(),
                Qt::KeepAspectRatioByExpanding
                );
    for (auto &star : stars)
    {
        star.position.setX(float(rand())/float(RAND_MAX));
        star.position.setY(float(rand())/float(RAND_MAX));
    }
}
