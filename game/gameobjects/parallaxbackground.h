#ifndef PARALLAXBACKGROUND_H
#define PARALLAXBACKGROUND_H

#include "game/gameobjects/basegameobject.h"
#include <QJsonObject>
#include <common/serializable.h>
#include "player.h"

class ParallaxBackground : public BaseGameObject
{
    Q_OBJECT

    Q_PROPERTY_WITH_ACCESSORS(double, Xspeed,      setXspeed       )
    Q_PROPERTY_WITH_ACCESSORS(double, Yspeed,      setYspeed       )
    Q_PROPERTY_WITH_ACCESSORS(double, xAutoScroll, setXAutoScroll  )
    Q_PROPERTY_WITH_ACCESSORS(double, yAutoScroll, setYAutoScroll  )
    Q_PROPERTY_WITH_ACCESSORS(double, Xtiling,     setXtiling      )
    Q_PROPERTY_WITH_ACCESSORS(double, Ytiling,     setYtiling      )

    enum sizePolicy{expandingSize, fixedSize};
    Q_PROPERTY_WITH_ACCESSORS(int, XsizePolicy,  setXsizePolicy)
    Q_PROPERTY_WITH_ACCESSORS(int, YsizePolicy,  setYsizePolicy)
    Q_PROPERTY_WITH_ACCESSORS(bool, attachToWorld,  setAttachToWorld)


public:
    ParallaxBackground(QJsonObject& parameters);
    ParallaxBackground();   
    void glDraw(GlUtils *glutils);
    void updateViewport(QRectF);
    QRectF boundingRect();
    void setGameData(GameData* data);
    void advance(int timeDelta);

protected:
    GameData* m_gameData;
    b2Vec2 m_speed;
    double m_xOffsetFix;
    QRectF rect;
    QPointF m_offset;
    QPointF m_tiling;
    GLuint texW;
    GLuint texH;
    double texAspect;
    double screenAspect;   
};

#endif // PARALLAXBACKGROUND_H
