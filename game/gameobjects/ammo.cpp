#include "ammo.h"

Ammo::Ammo()
    : PhysicsObject(),
      m_bulletType(type::Tomato),
      m_count(0),
      m_staticView(false),
      m_frame(0)
{
    b2Filter filter;
    filter.categoryBits = PhysicsLayer::Loot;
    filter.maskBits = PhysicsLayer::BulletPlayerTarget;
    b2Fixture* fix = body->GetFixtureList();
    while(fix)
    {
        fix->SetFilterData(filter);
        fix = fix->GetNext();
    }
}

Ammo::Ammo(QJsonObject &parameters, b2World *world)
    : PhysicsObject(parameters, world),
      m_bulletType(type::Tomato),
      m_count(0),
      m_frame(0)
{    
    setup(parameters);

    b2Filter filter;
    filter.categoryBits = PhysicsLayer::Loot;
    filter.maskBits = PhysicsLayer::BulletPlayerTarget;
    b2Fixture* fix = body->GetFixtureList();
    while(fix)
    {
        fix->SetFilterData(filter);
        fix = fix->GetNext();
    }
    if (!m_staticView)
    {
        m_shadowSprite.addTexture("./resources/textures/shadow_small.png");
        m_shadowSprite.setAsBox(m_width, m_height/2);
        m_shadowSprite.setPos(body->GetPosition());
    }
}

Ammo::~Ammo()
{

}

double Ammo::bottomY()
{
    return body->GetPosition().y+m_height/2;
}

void Ammo::disable()
{
    emit deleteMe(this);
}

void Ammo::glDraw(GlUtils *glutils)
{   
    if (!m_staticView)
    {
        ++m_frame;
        m_shadowSprite.draw(glutils);
        m_sprite.setOffset(0, -0.5-sin(m_frame/16.0)/8.0);
    }
    m_sprite.draw(glutils);
}
