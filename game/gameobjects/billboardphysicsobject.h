#ifndef BILLBOARDPHYSICSOBJECT_H
#define BILLBOARDPHYSICSOBJECT_H

#include "game/gameobjects/physicsobject.h"

class BillboardPhysicsObject : public PhysicsObject
{
    Q_OBJECT

    Q_PROPERTY_WITH_ACCESSORS(double,  physicsWidth,   setPhysicsWidth)
    Q_PROPERTY_WITH_ACCESSORS(double,  physicsHeight,   setPhysicsHeight)

public:
    BillboardPhysicsObject(){}
    BillboardPhysicsObject(QJsonObject& parameters, b2World* world);
    ~BillboardPhysicsObject();
    double bottomY();

protected:
    double m_yOffset;

    void createPhysics(b2World *world);
};

#endif // BILBOARDPHYSICSOBJECT_H
